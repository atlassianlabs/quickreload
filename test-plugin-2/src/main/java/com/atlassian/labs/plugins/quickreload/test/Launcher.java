package com.atlassian.labs.plugins.quickreload.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * The starting point for the code
 */
@Service
public class Launcher implements InitializingBean, DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(Launcher.class);

    @Override
    public void afterPropertiesSet() throws Exception
    {
        log.warn("\n\n"
                + "Quick Reload Test Plugin 2 is coming into existence!!"
                + "\n\n");
    }

    @Override
    public void destroy() throws Exception
    {
        log.warn("\n\n"
                + "Quick Reload Test Plugin 2 is going out of existence!!"
                + "\n\n");
    }
}
