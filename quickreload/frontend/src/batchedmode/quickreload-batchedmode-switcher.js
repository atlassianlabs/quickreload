import auiFlag from 'aui/flag';
import throttle from 'underscore/modules/throttle.js';
import whenIType from 'aui/when-i-type';
import wrmContextPath from 'wrm/context-path';

let flag = undefined;

const quickReloadBatchModeSwitcherInit = async () => {
    const getCurrentBatchModeStateUrl = function (contextPath) {
        return contextPath + '/rest/qr/batching';
    };

    const getUpdateBatchModeUrl = (contextPath, enableBatching) => {
        return contextPath + '/rest/qr/batching/setState?enabled=' + Boolean(enableBatching);
    };

    const toastInfo = (title, messageHtml) => {
        toastMessage(title, messageHtml, 'info');
    };

    const toastError = (title, messageHtml) => {
        toastMessage(title, messageHtml, 'error');
    };

    const toastMessage = (title, messageHtml, type) => {
        const newFlag = auiFlag({
            type,
            title,
            body: messageHtml,
            persistent: false,
            duration: 3_000,
            close: 'auto',
        });

        // Animate away while keeping one flag at a time
        flag?.close();
        flag = newFlag;
    };

    const contextPath = wrmContextPath();
    if (!contextPath) {
        console.warn('Failed to determine context path, QuickReload batch mode switcher not enabled');
        return;
    }

    let batchingEnabledState = false;
    // Fetch initial state
    try {
        const initialStateResponse = await fetch(getCurrentBatchModeStateUrl(contextPath), {
            headers: {
                'accept': 'application/json'
            }
        });
        if (!initialStateResponse.ok) {
            throw new Error('Network response was not ok')
        }
        const initialStateData = await initialStateResponse.json();
        if (initialStateData.batchingEnabled === undefined || initialStateData.batchingEnabled === null) {
            throw new Error('The quickreload batching API did not contain the field: batchingEnabled')
        }
        batchingEnabledState = initialStateData.batchingEnabled;
    } catch (error) {
        console.error('Quick Reload - Failed to get initial batching state', error);
        toastError('Quick Reload - Failed to initialise batching toggle', `
            The error was:
            <pre>${error}</pre>
        `)
        return;
    }

    const throttledToggleBatchingCallback = throttle(async () => {
        try {
            const enableBatching = !batchingEnabledState;
            const toggleResponse = await fetch(getUpdateBatchModeUrl(contextPath, enableBatching), {
                headers: {
                    'accept': 'application/json'
                }
            })
            if (!toggleResponse.ok) {
                throw new Error('Network response was not ok');
            }
            const toggleData = await toggleResponse.json();
            if (toggleData.batchingEnabled === undefined || toggleData.batchingEnabled === null) {
                throw new Error('The quickreload batching API did not contain the field: batchingEnabled')
            }
            batchingEnabledState = toggleData.batchingEnabled;
            const successTitle = batchingEnabledState ? 'Batching - On' : 'Batching - Off';
            const successMessageHtml = batchingEnabledState ? 'Web resource batching enabled' : 'Web resource batching disabled';
            toastInfo(successTitle, successMessageHtml);
        } catch (error) {
            const userFacingErrorMessage = `Quick Reload - Failed to update batch mode state (currently: ${batchingEnabledState ? 'enabled' : 'disabled'})`;
            console.error(userFacingErrorMessage, error);
            toastError(userFacingErrorMessage, `
                The error was:
                <pre>${error}</pre>
            `);
        }
    }, 150, {trailing: false});

    whenIType('b').execute(throttledToggleBatchingCallback);
}

// We don't know what web resource to get jQuery from (because we could be anywhere in JIRA / Customer Portal / Confluence),
// so register this to run after page load with vanilla JavaScript.
if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', () => {
        quickReloadBatchModeSwitcherInit();
    }, false);
} else {
    quickReloadBatchModeSwitcherInit();
}
