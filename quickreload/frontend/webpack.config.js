const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');

const TARGET_CLASSES_DIR = path.resolve(__dirname, '..', 'target', 'classes')

const IS_PRODUCTION = Boolean(process.env.NODE_ENV === 'production');

module.exports = {
    mode: IS_PRODUCTION ? 'production' : 'development',
    devtool: 'source-map',
    entry: {
        batchedmode: path.resolve(__dirname, 'src', 'batchedmode', 'quickreload-batchedmode-switcher.js')
    },
    module: {
        rules: [
            {
                test: /.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
        ],
    },
    resolve: {
        extensions: ['.js'],
    },
    output: {
        filename: '[name]/[name].js',
        path: path.resolve(TARGET_CLASSES_DIR),
    },
    plugins: [
        /**
         * More info about WRM Webpack Plugin here:
         * https://www.npmjs.com/package/atlassian-webresource-webpack-plugin
         */
        new WrmPlugin({
            pluginKey: 'com.atlassian.labs.plugins.quickreload.reloader',
            contextMap: {
                batchedmode: ['atl.general', 'atl.admin', 'customerportal'],
            },
            webresourceKeyMap: {
                batchedmode: 'qr-batched-mode-twizzler',
            },
            providedDependencies: {
                'aui/flag': {
                    'dependency': 'com.atlassian.auiplugin:aui-flag',
                    'import': {
                        'var': 'AJS.flag',
                    },
                },
                'aui/when-i-type': {
                    'dependency': 'com.atlassian.auiplugin:aui-keyboard-shortcuts',
                    'import': {
                        'var': 'AJS.whenIType',
                    },
                },
                'wrm/context-path': {
                    'dependency': 'com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path',
                    'import': {
                        'var': 'WRM.contextPath',
                    },
                }
            },
            xmlDescriptors: path.resolve(
                TARGET_CLASSES_DIR,
                'META-INF',
                'plugin-descriptors',
                'wr-webpack-bundles.xml',
            ),
        }),
    ],
    optimization: {
        minimize: IS_PRODUCTION,
    },
};
