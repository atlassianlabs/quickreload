package com.atlassian.labs.plugins.quickreload.rest;

import com.tngtech.archunit.core.domain.JavaAnnotation;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaCodeUnit;
import com.tngtech.archunit.core.domain.JavaParameter;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.Test;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.tngtech.archunit.core.importer.ImportOption.Predefined.DO_NOT_INCLUDE_TESTS;
import static com.tngtech.archunit.lang.SimpleConditionEvent.violated;
import static java.lang.String.format;
import static java.util.Arrays.asList;

public class TestJavaxJakarta {

    private static final List<Class<? extends Annotation>> javaxAnnotations = asList(Inject.class, Path.class, HttpMethod.class, DELETE.class, HEAD.class, GET.class, POST.class, PUT.class, OPTIONS.class, DefaultValue.class, CookieParam.class, HeaderParam.class, FormParam.class, MatrixParam.class, PathParam.class, QueryParam.class, Consumes.class, Produces.class);
    private static final List<Class<? extends Annotation>> jakartaAnnotations = asList(jakarta.inject.Inject.class, jakarta.ws.rs.Path.class, jakarta.ws.rs.HttpMethod.class, jakarta.ws.rs.DELETE.class, jakarta.ws.rs.HEAD.class, jakarta.ws.rs.GET.class, jakarta.ws.rs.POST.class, jakarta.ws.rs.PUT.class, jakarta.ws.rs.OPTIONS.class, jakarta.ws.rs.DefaultValue.class, jakarta.ws.rs.CookieParam.class, jakarta.ws.rs.HeaderParam.class, jakarta.ws.rs.FormParam.class, jakarta.ws.rs.MatrixParam.class, jakarta.ws.rs.PathParam.class, jakarta.ws.rs.QueryParam.class, jakarta.ws.rs.Consumes.class, jakarta.ws.rs.Produces.class);

    @Test
    public void testRestResources_shouldBeDualAnnotatedForBackAndForwardCompatibility() {
        if (javaxAnnotations.size() != jakartaAnnotations.size()) {
            throw new IllegalArgumentException("Javax and Jakarta annotations list aren't the same size. They should be matching lists");
        }

        JavaClasses importedClasses = new ClassFileImporter()
                .withImportOption(DO_NOT_INCLUDE_TESTS)
                .importPackages("com.atlassian.labs.plugins.quickreload.rest");

        ArchRuleDefinition.classes()
                .should(new HaveJavaxAndJakartaAnnotations())
                .check(importedClasses);
    }

    private static class HaveJavaxAndJakartaAnnotations extends ArchCondition<JavaClass> {

        HaveJavaxAndJakartaAnnotations() {
            super("have should have matching Javax & Jakarta annotations at the class-level, on methods, and parameters");
        }

        @Override
        public void check(JavaClass javaClass, ConditionEvents events) {
            for (int i = 0; i < javaxAnnotations.size(); i++) {
                Class<? extends Annotation> javaxAnnotation = javaxAnnotations.get(i);
                Class<? extends Annotation> jakartaAnnotation = jakartaAnnotations.get(i);
                Optional<? extends Annotation> javaxAnnotationInstance = javaClass.tryGetAnnotationOfType(javaxAnnotation);
                Optional<? extends Annotation> jakartaAnnotationInstance = javaClass.tryGetAnnotationOfType(jakartaAnnotation);

                if (javaxAnnotationInstance.isPresent() && jakartaAnnotationInstance.isPresent()) {
                    if (!annotationValuesEqual(javaxAnnotationInstance.get(), jakartaAnnotationInstance.get())) {
                        String message = format("Class %s has different values for annotations %s and %s",
                                javaClass.getName(), javaxAnnotation.getCanonicalName(), jakartaAnnotation.getCanonicalName());
                        events.add(violated(javaClass, message));
                    }
                } else if (javaxAnnotationInstance.isPresent() || jakartaAnnotationInstance.isPresent()) {
                    String message = format("Class %s does not have both annotations %s and %s",
                            javaClass.getName(), javaxAnnotation.getCanonicalName(), jakartaAnnotation.getCanonicalName());
                    events.add(violated(javaClass, message));
                }
            }

            final Set<JavaCodeUnit> codeUnits = new HashSet<>(javaClass.getMethods());
            codeUnits.addAll(javaClass.getAllConstructors());
            for (JavaCodeUnit javaCodeUnit : codeUnits) {
                for (int i = 0; i < javaxAnnotations.size(); i++) {
                    Class<? extends Annotation> javaxAnnotation = javaxAnnotations.get(i);
                    Class<? extends Annotation> jakartaAnnotation = jakartaAnnotations.get(i);
                    Optional<? extends Annotation> javaxAnnotationInstance = javaCodeUnit.tryGetAnnotationOfType(javaxAnnotation);
                    Optional<? extends Annotation> jakartaAnnotationInstance = javaCodeUnit.tryGetAnnotationOfType(jakartaAnnotation);

                    if (javaxAnnotationInstance.isPresent() && jakartaAnnotationInstance.isPresent()) {
                        if (!annotationValuesEqual(javaxAnnotationInstance.get(), jakartaAnnotationInstance.get())) {
                            String message = format("Method %s has different values for annotations %s and %s",
                                    javaCodeUnit.getFullName(), javaxAnnotation.getCanonicalName(), jakartaAnnotation.getCanonicalName());
                            events.add(violated(javaCodeUnit, message));
                        }
                    } else if (javaxAnnotationInstance.isPresent() || jakartaAnnotationInstance.isPresent()) {
                        String message = format("Method %s does not have both annotations %s and %s",
                                javaCodeUnit.getFullName(), javaxAnnotation.getCanonicalName(), jakartaAnnotation.getCanonicalName());
                        events.add(violated(javaCodeUnit, message));
                    }
                }

                for (Set<JavaAnnotation<JavaParameter>> annotations : javaCodeUnit.getParameterAnnotations()) {
                    for (int i = 0; i < javaxAnnotations.size(); i++) {
                        Class<? extends Annotation> javaxAnnotation = javaxAnnotations.get(i);
                        Class<? extends Annotation> jakartaAnnotation = jakartaAnnotations.get(i);
                        Optional<JavaAnnotation<JavaParameter>> javaxAnnotationInstance = annotations.stream()
                                .filter(annotation ->
                                        annotation.getType().getName().equals(javaxAnnotation.getCanonicalName()))
                                .findAny();
                        Optional<JavaAnnotation<JavaParameter>> jakartaAnnotationInstance = annotations.stream()
                                .filter(annotation ->
                                        annotation.getType().getName().equals(jakartaAnnotation.getCanonicalName()))
                                .findAny();

                        if (javaxAnnotationInstance.isPresent() && jakartaAnnotationInstance.isPresent()) {
                            if (!annotationValuesEqual(javaxAnnotationInstance.get().get("value").get(), jakartaAnnotationInstance.get().get("value").get())) {
                                String message = format("Parameter %s has different values for annotations %s and %s",
                                        javaxAnnotationInstance.get().getAnnotatedElement(), javaxAnnotation.getCanonicalName(), jakartaAnnotation.getCanonicalName());
                                events.add(violated(javaCodeUnit, message));
                            }
                        } else if (javaxAnnotationInstance.isPresent() || jakartaAnnotationInstance.isPresent()) {
                            JavaAnnotation<JavaParameter> annotation = javaxAnnotationInstance.orElseGet(jakartaAnnotationInstance::get);
                            String message = format("%s does not have both annotations %s and %s",
                                    annotation.getAnnotatedElement(), javaxAnnotation.getName(), jakartaAnnotation.getCanonicalName());
                            events.add(violated(javaCodeUnit, message));
                        }
                    }
                }
            }
        }

        private boolean annotationValuesEqual(Object valueA, Object valueB) {
            if (valueA instanceof String && valueB instanceof  String) {
                return Objects.equals(valueA, valueB);
            } else if (valueA instanceof String[] && valueB instanceof String[]) {
                return Arrays.equals((String[]) valueA, (String[]) valueB);
            }

            throw new RuntimeException("Need to implement a new equality comparison");
        }

        private boolean annotationValuesEqual(Annotation annotationA, Annotation annotationB) {
            try {
                Method method;
                try {
                    method = annotationA.annotationType().getMethod("value");
                } catch (NoSuchMethodException e) {
                    return true;
                }
                Object valueA = method.invoke(annotationA);
                Object valueB = annotationB.annotationType().getMethod("value").invoke(annotationB);
                return annotationValuesEqual(valueA, valueB);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
