package com.atlassian.labs.plugins.quickreload.utils;

import org.apache.commons.io.FileUtils;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestFiles
{
    private File startDir;

    @Before
    public void setUp() throws Exception
    {
        startDir = new File(File.createTempFile("abc", "xyz").getParentFile(),"root");
        assertTrue(startDir.mkdir());
    }

    @After
    public void tearDown() throws Exception
    {
        FileUtils.forceDelete(startDir);
    }

    @Test
    public void test_traverseAndFind() throws Exception
    {
        File depthDir = new File(startDir, "level1/level2/level3/level4/level5");
        FileUtils.forceMkdir(depthDir);

        File pomAt4 = new File(startDir, "level1/level2/level3/level4/pom.xml");
        FileUtils.touch(pomAt4);

        File pomAt3 = new File(startDir, "level1/level2/level3/pom.xml");
        FileUtils.touch(pomAt3);

        File pomAt1 = new File(startDir, "level1/pom.xml");
        FileUtils.touch(pomAt1);

        Optional<File> found = Files.traverseUpForFileThenYouFindItAndTheCant("pom.xml", depthDir);
        assertThat(found.isPresent(), CoreMatchers.equalTo(true));
        assertThat(found.get(), CoreMatchers.equalTo(pomAt3));

        found = Files.traverseUpForFileThenYouFindItAndTheCant("notFound.xml", depthDir);
        assertThat(found.isPresent(), CoreMatchers.equalTo(false));
    }

    @Test
    public void test_traverseDownLookingForFile() throws Exception
    {
        mkdirWithPomXml("level1/level2/level3_1");
        mkdirWithPomXml("level1/level2/level3_2");
        mkdirWithPomXml("level1/ignore/somedir");

        mkfile("level1/pom.xml");
        mkfile("level1/level2/pom.xml");

        AtomicInteger count = new AtomicInteger();
        Files.traverseDownLookingForFile("pom.xml", new File(startDir, "."),
            (p) -> !p.endsWith("ignore"),
            (f) ->
                count.getAndIncrement()
        );
        assertThat(count.get(), CoreMatchers.equalTo(4));
    }

    private File mkdirWithPomXml(String dir) throws Exception
    {
        File newDir = new File(startDir, dir);
        FileUtils.forceMkdir(newDir);
        mkfile(dir + "/pom.xml");
        return newDir;
    }

    private void mkfile(String filename) throws Exception
    {
        File pomAt1 = new File(startDir, filename);
        FileUtils.touch(pomAt1);
    }
}