package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.config.PomConventionStrategy;
import com.atlassian.labs.plugins.quickreload.utils.Files;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;

public class PomConventionStrategyTest
{

    @Test
    public void testGetTargetDirs() throws Exception
    {
        //arrange
        PomConventionStrategy pomConventionStrategy = new PomConventionStrategy(Files.pwd());
        List<File> targetDirs = pomConventionStrategy.getTargetDirs();
        Assert.assertThat(targetDirs.size(), equalTo(6));

    }
}