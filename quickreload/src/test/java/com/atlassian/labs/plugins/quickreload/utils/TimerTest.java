package com.atlassian.labs.plugins.quickreload.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TimerTest
{

    class MutabeClock implements Timer.Clock
    {
        private long currentMillis = 0;

        @Override
        public long currentMillis()
        {
            return currentMillis;
        }

        public void setCurrentMillis(final long currentMillis)
        {
            this.currentMillis = currentMillis;
        }


    }

    MutabeClock clock = new MutabeClock();

    @Before
    public void setUp() throws Exception
    {
        clock = new MutabeClock();
    }

    @Test
    public void testBasicElapsed() throws Exception
    {
        clock.setCurrentMillis(500);
        Timer t = new Timer(clock);
        assertThat(t.elapsedMillis(), equalTo(0L));
        assertThat(t.hasElapsed(100), equalTo(false));
        assertThat(t.isInside(100), equalTo(true));

        clock.setCurrentMillis(1000);
        assertThat(t.elapsedMillis(), equalTo(500L));

        assertThat(t.hasElapsed(100), equalTo(true));
        assertThat(t.isInside(100), equalTo(false));

        assertThat(t.hasElapsed(499), equalTo(true));
        assertThat(t.hasElapsed(500), equalTo(true));
        assertThat(t.hasElapsed(1000), equalTo(false));
        assertThat(t.hasElapsed(20000), equalTo(false));
        assertThat(t.isInside(20000), equalTo(true));

        assertThat(t.hasElapsed(1, TimeUnit.SECONDS), equalTo(false));
        assertThat(t.hasElapsed(1, TimeUnit.HOURS), equalTo(false));
        assertThat(t.hasElapsed(1, TimeUnit.MILLISECONDS), equalTo(true));
        assertThat(t.hasElapsed(10, TimeUnit.MICROSECONDS), equalTo(true));
        assertThat(t.hasElapsed(500, TimeUnit.MILLISECONDS), equalTo(true));
        assertThat(t.isInside(500, TimeUnit.MILLISECONDS), equalTo(false));

    }

    @Test
    public void testReset() throws Exception
    {
        clock.setCurrentMillis(1000);

        Timer t = new Timer(clock);
        assertThat(t.elapsedMillis(), equalTo(0L));

        clock.setCurrentMillis(2000);
        assertThat(t.elapsedMillis(), equalTo(1000L));

        clock.setCurrentMillis(3000);
        assertThat(t.elapsedMillis(), equalTo(2000L));


        t.reset();
        assertThat(t.elapsedMillis(), equalTo(0L));

        clock.setCurrentMillis(4000);
        assertThat(t.elapsedMillis(), equalTo(1000L));

    }

}