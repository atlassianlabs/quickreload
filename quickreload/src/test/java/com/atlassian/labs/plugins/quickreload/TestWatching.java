package com.atlassian.labs.plugins.quickreload;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

/**
 * Run this to watch what happens on file change, add or delete
 */
public class TestWatching {

    public static void main(String[] args) {
        try {
            String homeDir = System.getProperty("user.home");
            File directoryToWatch = new File(homeDir, "watching");
            if (!directoryToWatch.exists()){
                directoryToWatch.mkdir();
            }

            WatchService watchService = WatchServiceHelper.getWatchService();
            WatchServiceHelper.registerDirectoryForWatching(watchService, directoryToWatch);

            System.out.println(String.format("Watching %s....", directoryToWatch.toPath()));

            try {
                while (true) {
                    System.out.println("\nwaiting....");
                    WatchKey signalledKey = watchService.take();
                    System.out.println("change detected");
                    List<WatchEvent<?>> x = signalledKey.pollEvents();
                    for (WatchEvent<?> event : x) {
                        String fullPath = WatchServiceHelper.getFullPath((Path) event.context(),
                                directoryToWatch.toPath());
                        System.out.println(String.format("%s - %s", event, fullPath));
                    }
                    signalledKey.reset();
                }
            } catch (Exception breakOut) {
                // other thread closed watch service
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
