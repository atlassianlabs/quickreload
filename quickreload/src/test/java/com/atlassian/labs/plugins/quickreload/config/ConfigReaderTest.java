package com.atlassian.labs.plugins.quickreload.config;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertThat;

public class ConfigReaderTest
{

    @Test
    public void testParseProperties() throws Exception
    {
        ConfigReader configReader = new ConfigReader(null, null, null);

        ConfigReader.ConfigEntry actual;

        actual = configReader.parseEntry("");
        assertThat(actual.getPathProperties().size(), Matchers.equalTo(0));

        actual = configReader.parseEntry("/somepath/with /even/spaces");
        assertThat(actual.getPath(), Matchers.equalTo("/somepath/with /even/spaces"));
        assertThat(actual.getPathProperties().size(), Matchers.equalTo(0));

        actual = configReader.parseEntry("prop1=true ; prop2=5;=withnokeydissapears;:/some/path");
        assertThat(actual.getPath(), Matchers.equalTo("/some/path"));
        assertThat(actual.getPathProperties(), Matchers.allOf(
                hasSize(2),
                Matchers.hasEntry("prop1", "true"),
                Matchers.hasEntry("prop2", "5")
        ));

        actual = configReader.parseEntry("prop1 = true ; prop2 = 5:/some/path");
        assertThat(actual.getPath(), Matchers.equalTo("/some/path"));
        assertThat(actual.getPathProperties(), Matchers.allOf(
                hasSize(2),
                Matchers.hasEntry("prop1", "true"),
                Matchers.hasEntry("prop2", "5")
        ));

        // defaulting to boolean when no equals
        actual = configReader.parseEntry("prop1=true; prop2 = 5; delay=1000; resource:/some/path");
        assertThat(actual.getPath(), Matchers.equalTo("/some/path"));
        assertThat(actual.getPathProperties(), Matchers.allOf(
                hasSize(4),
                Matchers.hasEntry("resource", "true"),
                Matchers.hasEntry("delay", "1000"),
                Matchers.hasEntry("prop1", "true"),
                Matchers.hasEntry("prop2", "5")
        ));
    }

    @Test
    public void testConfigurationReading() throws Exception
    {
        ConfigReader configReader = new ConfigReader(null, null, null);

        ConfigReader.ConfigEntry actual;

        actual = configReader.parseEntry("    qr:someentry");
        assertThat(actual.isQrConfigEntry(), Matchers.equalTo(true));
        assertThat(actual.getQrConfigProperties(), Matchers.allOf(
                hasSize(1),
                Matchers.hasEntry("someentry", "true")
        ));

        actual = configReader.parseEntry("    qr:someentry;someotherentry=5");
        assertThat(actual.isQrConfigEntry(), Matchers.equalTo(true));
        assertThat(actual.getQrConfigProperties(), Matchers.allOf(
                hasSize(2),
                Matchers.hasEntry("someentry", "true"),
                Matchers.hasEntry("someotherentry", "5")
        ));

    }

    private Matcher<Map> hasSize(final int size) {
        return new BaseMatcher<Map>()
        {
            @Override
            public boolean matches(final Object item)
            {
                return item instanceof Map && ((Map) item).size() == size;
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText(String.format("A map of size %d", size));
            }
        };
    }
}