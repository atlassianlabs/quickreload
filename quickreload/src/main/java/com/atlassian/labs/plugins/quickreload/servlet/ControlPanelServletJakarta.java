package com.atlassian.labs.plugins.quickreload.servlet;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static com.atlassian.labs.plugins.quickreload.servlet.ControlPanelServletCommon.SERVLET_CONTENT_TYPE;
import static com.atlassian.labs.plugins.quickreload.servlet.ControlPanelServletCommon.getApiLink;

@UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
public class ControlPanelServletJakarta extends HttpServlet {

    private final ControlPanelServletCommon controlPanelServletCommon;

    public ControlPanelServletJakarta(
            final ControlPanelServletCommon controlPanelServletCommon) {
        this.controlPanelServletCommon = controlPanelServletCommon;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws IOException {
        // Bitbucket needs a charset specifically
        response.setContentType(SERVLET_CONTENT_TYPE);
        String apiLink = getApiLink(req.getScheme(), req.getServerName(), req.getServerPort(), req.getContextPath());
        controlPanelServletCommon.render(response.getWriter(), apiLink);
    }
}
