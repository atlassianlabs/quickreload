package com.atlassian.labs.plugins.quickreload.install;

import com.atlassian.labs.plugins.quickreload.WittyQuoter;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Supplier;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * The code to do plugin install
 */
class PluginInstallerMechanic {
    private static final Logger log = setInfo(getLogger(PluginInstallerMechanic.class));

    private final PluginController pluginController;
    private final WittyQuoter wittyQuoter;
    private final PluginAccessor pluginAccessor;

    PluginInstallerMechanic(final PluginController pluginController, final WittyQuoter wittyQuoter, final PluginAccessor pluginAccessor)
    {
        this.pluginController = pluginController;
        this.wittyQuoter = wittyQuoter;
        this.pluginAccessor = pluginAccessor;
    }

    void onShutdown()
    {
    }

    void enablePlugin(final String pluginKey)
    {
        downArrowMsg(format("\n\nEnabling plugin(s) - '%s'....\n\n", pluginKey));
        pluginGuard(pluginKey, () -> {
            if (isModuleKey(pluginKey))
            {
                pluginController.enablePluginModule(pluginKey);
            }
            else
            {
                pluginController.enablePlugins(pluginKey);
            }
            return null;
        });
    }

    void disablePlugin(final String pluginKey)
    {
        downArrowMsg(format("\n\nDisabling plugin(s) - '%s'....\n\n", pluginKey));
        pluginGuard(pluginKey, () -> {
            if (isModuleKey(pluginKey))
            {
                pluginController.disablePluginModule(pluginKey);
            }
            else
            {
                pluginController.disablePlugin(pluginKey);
            }
            return null;
        });
    }

    boolean pluginEnabled(final String pluginKey)
    {
        return pluginGuard(pluginKey, () -> {
            if (isModuleKey(pluginKey))
            {
                return pluginAccessor.isPluginModuleEnabled(pluginKey);
            }
            else
            {
                return pluginAccessor.isPluginEnabled(pluginKey);
            }
        }).orElse(Boolean.FALSE);
    }

    private boolean isModuleKey(final String pluginKey)
    {
        return pluginKey.contains(":");
    }

    private void validPlugin(final String pluginKey)
    {
        Object toCheck;
        if (isModuleKey(pluginKey))
        {
            toCheck = pluginAccessor.isPluginModuleEnabled(pluginKey);
        }
        else
        {
            toCheck = pluginAccessor.getPlugin(pluginKey);
        }
        if (toCheck == null)
        {
            throw new IllegalArgumentException(String.format("The plugin or module with key '%s' can not be found", pluginKey));
        }
    }

    private <T> Optional<T> pluginGuard(final String pluginKey, final Supplier<T> supplier)
    {
        validPlugin(pluginKey);

        return Optional.ofNullable(supplier.get());
    }

    Optional<Exception> installPluginImmediately(File pluginFile)
    {
        long then = System.currentTimeMillis();
        String pluginPath = pluginFile.getAbsolutePath();

        downArrowMsg(format("\n\nStarting Quick Reload - '%s'....\n\n", pluginPath));

        PluginArtifact pluginArtifact = new JarPluginArtifact(pluginFile);
        Exception installException = null;
        try
        {
            pluginController.installPlugins(pluginArtifact);
        }
        catch (Exception e)
        {
            installException = e;
        }
        long ms = System.currentTimeMillis() - then;

        StringBuilder sb = new StringBuilder();
        upArrow(sb);
        sb.append("\n\n");

        if (installException == null)
        {
            String wit = wittyQuoter.halfWit();
            sb.append(format("        %s\n\n"
                            + "Quick Reload Finished (%d ms) - '%s'", wit, ms, pluginFile.getName()
            ));

            notifyExternally(format("Quick Reload Finished - %s", wit));

        }
        else
        {
            sb.append(format("Quick Reload FAILED because of %s!!\n", installException));
        }

        log.info(sb.toString());
        return Optional.ofNullable(installException);
    }

    private void downArrowMsg(String msg)
    {
        StringBuilder sb = new StringBuilder();
        //
        // make some white space so that after plugin load the logs have a bit of breathing space
        // so the dev can know - ahh here was a reload and hence new code
        //
        downArrow(sb);
        sb.append(msg);
        log.info(sb.toString());
    }

    private void notifyExternally(final String msg)
    {
        try
        {
            Runtime.getRuntime().exec("quickreloadnotify '" + msg + "'");
        }
        catch (IOException ignored)
        {
        }
    }

    private void downArrow(final StringBuilder sb)
    {
        makeArrow(sb, true);
    }

    private void upArrow(final StringBuilder sb)
    {
        makeArrow(sb, false);
    }

    private void makeArrow(final StringBuilder sb, boolean down)
    {
        String spacing = "\n                        ";
        String lines = StringUtils.repeat(spacing + "|", 5);

        if (down)
        {
            sb.append(lines);
            sb.append(format("%sv", spacing));
        }
        else
        {
            sb.append(format("%s^", spacing));
            sb.append(lines);
        }
    }

    void uninstallPlugin(String pluginKey)
    {
        Plugin plugin = pluginAccessor.getPlugin(pluginKey);
        if (plugin == null)
        {
            throw new IllegalArgumentException(String.format("The plugin key %s can not be found", pluginKey));
        }

        pluginController.uninstall(plugin);
    }
}
