package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.DirectoryWatcher;
import com.atlassian.labs.plugins.quickreload.install.PluginInstaller;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.PluginLinks;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.badRequest;
import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.notFound;
import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.serverError;
import static com.atlassian.labs.plugins.quickreload.utils.Files.getAllAtlassianPlugins;
import static com.atlassian.labs.plugins.quickreload.utils.Files.hasPomDotXml;
import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setWarn;
import static java.io.File.createTempFile;
import static java.lang.String.join;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Has functionality to enable, disable, install and uninstall plugins
 */
@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/plugin") @jakarta.ws.rs.Path("/plugin")
public class PluginRestResource {

    private static final Logger log = setWarn(getLogger(PluginRestResource.class));

    private final Links links;
    private final DirectoryTracker directoryTracker;
    private final DirectoryWatcher directoryWatcher;
    private final PluginInstaller pluginInstaller;

    @Inject @jakarta.inject.Inject
    public PluginRestResource(
            final Links links,
            final PluginInstaller pluginInstaller,
            final DirectoryTracker directoryTracker,
            final DirectoryWatcher directoryWatcher) {
        this.links = requireNonNull(links, "links");
        this.pluginInstaller = requireNonNull(pluginInstaller, "pluginInstaller");
        this.directoryTracker = requireNonNull(directoryTracker, "directoryTracker");
        this.directoryWatcher = requireNonNull(directoryWatcher, "directoryWatcher");
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/enabled/{pluginKey}") @jakarta.ws.rs.Path("/enabled/{pluginKey}")
    public PluginState enabled(@PathParam("pluginKey") @jakarta.ws.rs.PathParam("pluginKey") String pluginKey) {
        boolean isEnabled;
        try {
            isEnabled = pluginInstaller.pluginEnabled(pluginKey);
        } catch (IllegalArgumentException e) {
            throw badRequest(new ExceptionDTO(links.apiLinks(), e));
        } catch (Exception e) {
            throw serverError(new ExceptionDTO(links.apiLinks(), e));
        }
        PluginState pluginState = new PluginState(pluginKey, isEnabled, links.pluginLinks(pluginKey), links.apiLinks());
        if (!isEnabled) {
            throw notFound(pluginState);
        }
        return pluginState;
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @POST @jakarta.ws.rs.POST
    @Path("/enabled/{pluginKey}") @jakarta.ws.rs.Path("/enabled/{pluginKey}")
    public PluginState enable(@PathParam("pluginKey") @jakarta.ws.rs.PathParam("pluginKey") String pluginKey) {
        try {
            pluginInstaller.enablePlugin(pluginKey);
        } catch (Exception e) {
            throw serverError(new ExceptionDTO(links.apiLinks(), e));
        }
        return enabled(pluginKey);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @DELETE @jakarta.ws.rs.DELETE
    @Path("/enabled/{pluginKey}") @jakarta.ws.rs.Path("/enabled/{pluginKey}")
    public PluginState disable(@PathParam("pluginKey") @jakarta.ws.rs.PathParam("pluginKey") String pluginKey) {
        try {
            pluginInstaller.disablePlugin(pluginKey);
        } catch (Exception e) {
            throw serverError(new ExceptionDTO(links.apiLinks(), e));
        }
        return enabled(pluginKey);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/install/{fullPathToPlugin : .+?}") @jakarta.ws.rs.Path("/install/{fullPathToPlugin : .+?}")
    public Installed installGet(@PathParam("fullPathToPlugin") @jakarta.ws.rs.PathParam("fullPathToPlugin") String fullPathToPlugin) {
        // mutation via GET is allowed under the REST do-ocracy
        return install(fullPathToPlugin);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @POST @jakarta.ws.rs.POST
    @Path("/install/{fullPathToPlugin : .+?}") @jakarta.ws.rs.Path("/install/{fullPathToPlugin : .+?}")
    public Installed install(@PathParam("fullPathToPlugin") @jakarta.ws.rs.PathParam("fullPathToPlugin") String fullPathToPlugin) {
        ApiLinks api = links.apiLinks();
        if (isEmpty(fullPathToPlugin)) {
            throw badRequest(api);
        }
        File file = new File(fullPathToPlugin);
        if (!file.exists()) {
            throw badRequest(api);
        }

        if (file.isDirectory()) {
            // ok if they give us an existing directory then we will try to install all jars in there
            return installDirectoryOfPlugins(api, file);
        }

        installPlugin(api, file);
        return new Installed(api, file.getAbsolutePath());
    }

    /**
     * Most REST clients will send the .jar file as the whole body
     */
    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @POST @jakarta.ws.rs.POST
    @Path("/upload") @jakarta.ws.rs.Path("/upload")
    @Consumes({APPLICATION_OCTET_STREAM, "application/java-archive"}) @jakarta.ws.rs.Consumes({APPLICATION_OCTET_STREAM, "application/java-archive"})
    public Installed uploadFile(final File uploaded) {
        final ApiLinks apiLinks = links.apiLinks();
        try {
            if (!uploaded.exists() || uploaded.length() == 0) {
                throw notFound(new Installed(apiLinks,"There are no plugins uploaded"));
            }
            // The file provided by Jersey will fail for some reason
            final File safeTempFile = createTempFile("qr-" + uploaded.getName(), ".jar");
            Files.copy(uploaded.toPath(), safeTempFile.toPath(), REPLACE_EXISTING);
            installPlugin(apiLinks, safeTempFile);
            final String message = safeTempFile.getAbsolutePath();
            return new Installed(apiLinks, message);
        } catch (IOException ioException) {
            throw serverError(ioException);
        }
    }

    /**
     * Using the cURL example will include the file as binary as part of a form
     */
    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @POST @jakarta.ws.rs.POST
    @Path("/upload") @jakarta.ws.rs.Path("/upload")
    @Consumes(MULTIPART_FORM_DATA) @jakarta.ws.rs.Consumes(MULTIPART_FORM_DATA)
    public Installed uploadInBody(@MultipartFormParam("file") @com.atlassian.plugins.rest.api.multipart.MultipartFormParam("file") final Collection<Object> fileParts) {
        final ApiLinks apiLinks = links.apiLinks();
        try {
            if (fileParts.isEmpty()) {
                throw notFound(new Installed(apiLinks,"There are no plugins uploaded"));
            }
            List<String> installedFiles = new ArrayList<>();
            for (Object filePart : fileParts) {
                boolean restv1;
                try {
                    // assuming we don't have restv2 migration tag
                    Class.forName("com.atlassian.plugins.rest.common.multipart.FilePart");
                    restv1 = true;
                } catch (ClassNotFoundException e) {
                    restv1 = false;
                }
                Class filePartClass = restv1
                        ? Class.forName("com.atlassian.plugins.rest.common.multipart.FilePart")
                        : Class.forName("com.atlassian.plugins.rest.api.multipart.FilePart");
                Method getNameMethod = filePartClass.getMethod("getName");
                String filename = "qr-" + getNameMethod.invoke(filePart).toString();
                if (filename.endsWith(".jar")) { // Make the filename easier to read
                    filename = filename.substring(0, filename.lastIndexOf(".jar")) + '-';
                }
                final File pluginFile = createTempFile(filename, ".jar");
                Method writeMethod = filePartClass.getMethod("write", File.class);
                writeMethod.invoke(filePart, pluginFile);
                installPlugin(apiLinks, pluginFile);
                installedFiles.add(pluginFile.getAbsolutePath());
            }
            final String msg = installedFiles.isEmpty()
                    ? "Zero files provided - Nothing installed"
                    : join(", ", installedFiles);
            return new Installed(apiLinks, msg);
        } catch (IOException e) {
            throw serverError(e);
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Installed installDirectoryOfPlugins(ApiLinks apiLinks, File dir) {
        Collection<File> plugins = getAllAtlassianPlugins(dir);
        if (plugins.isEmpty()) {
            // ok so no plugin. let's check if it's a pom.xml and try ./target if so
            if (hasPomDotXml(dir)) {
                File targetDir = new File(dir, "target");
                plugins = getAllAtlassianPlugins(targetDir);
            }
        }
        if (plugins.isEmpty()) {
            throw serverError(new Installed(apiLinks,"There are no plugins to install"));
        }
        for (File plugin : plugins) {
            installPlugin(apiLinks, plugin);
        }
        return new Installed(apiLinks, dir.getAbsolutePath());
    }

    private void installPlugin(ApiLinks apiLinks, File pluginFile) {
        log.info("Installing plugin '{}'....", pluginFile);
        final Optional<Exception> result = pluginInstaller.installPluginImmediately(pluginFile);
        if (result.isPresent()) {
            final Exception e = result.get();
            log.error("Unable to install plugin '{}'", pluginFile, e);
            throw serverError(new Installed(apiLinks, pluginFile.getAbsolutePath(), e.toString()));
        }

        // from now on we track this directory
        final File trackedDir = pluginFile.getParentFile();
        directoryTracker.add(trackedDir.getAbsolutePath());
        directoryWatcher.watch(trackedDir);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @DELETE @jakarta.ws.rs.DELETE
    @Path("/install/{pluginKey}") @jakarta.ws.rs.Path("/install/{pluginKey}")
    public ApiLinks uninstall(@PathParam("pluginKey") @jakarta.ws.rs.PathParam("pluginKey") String pluginKey) {
        ApiLinks api = links.apiLinks();
        try {
            pluginInstaller.uninstallPlugin(pluginKey);

            return api;
        } catch (RuntimeException e) {
            log.error("Unable to uninstall '{}'", pluginKey, e);

            if (e instanceof IllegalArgumentException) {
                UnInstalled unInstalled = new UnInstalled(api, pluginKey, "plugin key not found");
                throw notFound(unInstalled);
            }

            UnInstalled unInstalled = new UnInstalled(api, pluginKey, e.toString());
            throw serverError(unInstalled);
        }
    }

    public static class ExceptionDTO {
        public final String error;
        public final String stackTrace;
        public final ApiLinks api;

        ExceptionDTO(final ApiLinks api, final Exception e) {
            this.api = api;
            this.error = e.toString();
            this.stackTrace = makeStackTrace(e);
        }

        private String makeStackTrace(final Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            return sw.toString();
        }
    }

    public static class PluginState {
        public final boolean enabled;
        public final String key;
        public final PluginLinks links;
        public final ApiLinks api;

        PluginState(final String key, final boolean enabled, final PluginLinks links, final ApiLinks api) {
            this.key = key;
            this.enabled = enabled;
            this.links = links;
            this.api = api;

        }
    }

    static class Installed {
        public final String installed;
        public final String error;
        public final ApiLinks api;

        Installed(final ApiLinks api, final String installed) {
            this.api = api;
            this.installed = installed;
            this.error = null;
        }

        Installed(final ApiLinks api, final String installed, String error) {
            this.api = api;
            this.installed = installed;
            this.error = error;
        }
    }

    static class UnInstalled {
        public final ApiLinks api;
        public final String pluginKey;
        public final String error;

        UnInstalled(final ApiLinks api, final String pluginKey, String error) {
            this.api = api;
            this.pluginKey = pluginKey;
            this.error = error;
        }
    }
}
