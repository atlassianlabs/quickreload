package com.atlassian.labs.plugins.quickreload;

/**
 * A simple interface that says a a component understands lifecycle
 */
public interface LifecycledComponent
{
    void onStartup();

    void onShutdown();
}
