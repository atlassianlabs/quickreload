package com.atlassian.labs.plugins.quickreload.install;

import com.atlassian.labs.plugins.quickreload.utils.Timer;

import java.io.File;

/**
 * A promise to act on a jar file once it becomes stable
 */
public class StableJarPromise
{
    private final File jarFile;
    private final String fullPath;
    private final Timer timer;

    public StableJarPromise(final File jarFile, final Timer timer)
    {
        this.jarFile = jarFile;
        this.fullPath = jarFile.getAbsolutePath();
        this.timer = timer;
    }

    public File getPluginFile()
    {
        return jarFile;
    }

    public Timer getTimer()
    {
        return timer;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final StableJarPromise that = (StableJarPromise) o;

        return fullPath.equals(that.fullPath);
    }

    @Override
    public int hashCode()
    {
        return fullPath.hashCode();
    }

    @Override
    public String toString()
    {
        //noinspection StringBufferReplaceableByString
        return new StringBuilder(super.toString())
                .append(" fullPath : ").append(fullPath)
                .append(" timeSoFar : ").append(timer.elapsedMillis())
                .toString();
    }
}
