package com.atlassian.labs.plugins.quickreload.rest.dto;

import com.atlassian.labs.plugins.quickreload.rest.dto.Links.LinkBuilder;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.replace;

/**
 * A basic link in resource terms
 */
class ResourceLink {
    public final String url;
    public final String method;
    public final String params;

    private ResourceLink(final String method, final LinkBuilder url, final String params) {
        this.url = replaceSpecials(url.build());
        this.method = method;
        this.params = params;
    }

    private static String replaceSpecials(String url) {
        // work around that we want {} in otherwise encoded URLS
        url = replace(url, "%3C", "{");
        url = replace(url, "%3E", "}");
        return url;
    }

    public static ResourceLink get(LinkBuilder url, String... params) {
        return new ResourceLink("GET", url, makeParams(params));
    }

    public static ResourceLink post(LinkBuilder url) {
        return new ResourceLink("POST", url, null);
    }

    public static ResourceLink delete(LinkBuilder url) {
        return new ResourceLink("DELETE", url, null);
    }

    public static ResourceLink put(LinkBuilder url) {
        return new ResourceLink("PUT", url, null);
    }

    private static String makeParams(final String[] params) {
        if (isNull(params)) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (String param : params) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(param);
        }
        return sb.length() == 0 ? null : sb.toString();
    }
}
