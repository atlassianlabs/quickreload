package com.atlassian.labs.plugins.quickreload.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.status;

/**
 * All because {@link Provider} doesn't work before Platform 8 (at least not on 6).
 * Throws the exception for you but "returns" the exception to keep the nicer syntax and avoid errors.
 */
public class RestApiExceptions {

    public static RuntimeException badRequest(Object entity) throws WebApplicationException {
        return throwException(BAD_REQUEST, entity);
    }

    public static RuntimeException notFound(Object entity) throws WebApplicationException {
        return throwException(NOT_FOUND, entity);
    }

    public static RuntimeException serverError(Object entity) throws WebApplicationException {
        return throwException(INTERNAL_SERVER_ERROR, entity);
    }

    private static RuntimeException throwException(Status status, Object entity) throws WebApplicationException {
        boolean isJavax;
        try {
            Class.forName("jakarta.ws.rs.Path");
            isJavax = false;
        } catch (ClassNotFoundException e) {
            isJavax = true;
        }

        if (isJavax) {
            throw new WebApplicationException(null, status(status).type(dumbContentTypeDetect(entity)).entity(entity).build());
        }
        throw new AlternateStatusCodeException(status.getStatusCode(), dumbContentTypeDetect(entity), entity);
    }

    private static String dumbContentTypeDetect(Object entity) {
        if (entity instanceof String) {
            return TEXT_HTML;
        }

        return APPLICATION_JSON;
    }
}
