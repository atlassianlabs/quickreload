package com.atlassian.labs.plugins.quickreload.cache;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.DummyPlugin;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setWarn;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

public class DefaultWebResourceUrlFlusher implements WebResourceUrlFlusher {
    private static final Logger log = setWarn(getLogger(DefaultWebResourceUrlFlusher.class));
    private final EventPublisher eventPublisher;

    public DefaultWebResourceUrlFlusher(final EventPublisher eventPublisher) {
        this.eventPublisher = requireNonNull(eventPublisher);
    }

    @Override
    public void flushWebResourceUrls() {
        try {
            eventPublisher.publish(new PluginEnabledEvent(new EmptyPlugin()));
        } catch (Exception e) {
            log.error("Failed to flush the web-resources after Quick Reloading - " +
                    "Expect to not see your frontend changes. Please report this along with the product and version", e);
        }
        log.info("Flushed web-resource cache");
    }

    private static class EmptyPlugin extends DummyPlugin {

        @Override
        public List<ResourceDescriptor> getResourceDescriptors() {
            return emptyList();
        }

        @Override
        public String getName() {
            return this.getClass().getSimpleName();
        }

        @Override
        public String getKey() {
            return "com.atlassian.quickreload." + getName();
        }

        @Override
        public Collection<ModuleDescriptor<?>> getModuleDescriptors() {
            return emptyList();
        }

        @Override
        public <M> List<ModuleDescriptor<M>> getModuleDescriptorsByModuleClass(Class<M> moduleClass) {
            return emptyList();
        }

        @Override
        public InstallationMode getInstallationMode() {
            return InstallationMode.LOCAL;
        }

        @Override
        public PluginInformation getPluginInformation() {
            return new PluginInformation();
        }

        @Override
        public PluginState getPluginState() {
            return PluginState.DISABLED;
        }

        @Override
        public Date getDateLoaded() {
            return Date.from(Instant.now());
        }

        @Override
        public Date getDateInstalled() {
            return Date.from(Instant.now());
        }

        @Override
        public boolean isUninstallable() {
            return true;
        }

        @Override
        public boolean isDeleteable() {
            return true;
        }

        @Override
        public <T> Class<T> loadClass(String clazz, Class<?> callingClass) {
            return null;
        }

        @Override
        public ClassLoader getClassLoader() {
            return DefaultWebResourceUrlFlusher.class.getClassLoader();
        }

        @Nonnull
        @Override
        public PluginDependencies getDependencies() {
            return PluginDependencies.builder().build();
        }

        @Override
        public Set<String> getActivePermissions() {
            return emptySet();
        }

        @Nullable
        @Override
        public Date getDateEnabling() {
            return Date.from(Instant.now());
        }

        @Nullable
        @Override
        public Date getDateEnabled() {
            return Date.from(Instant.now());
        }
    }
}
