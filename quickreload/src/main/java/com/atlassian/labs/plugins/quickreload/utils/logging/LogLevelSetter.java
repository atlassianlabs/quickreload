package com.atlassian.labs.plugins.quickreload.utils.logging;

import org.slf4j.Logger;

public interface LogLevelSetter {

    org.slf4j.Logger set(Logger slf4jLogger, Level logLevelToSet);

    enum Level {
        OFF,
        FATAL,
        ERROR,
        WARN,
        INFO,
        DEBUG,
        TRACE,
        ALL,
    }
}
