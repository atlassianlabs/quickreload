package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.inspectors.BundleHeaderParser;
import com.atlassian.labs.plugins.quickreload.inspectors.BundleInspector;
import com.atlassian.labs.plugins.quickreload.inspectors.PluginBundle;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.BundleLinks;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ModuleLinks;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.PluginLinks;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Bundle;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.atlassian.labs.plugins.quickreload.inspectors.BundleInspector.getPluginKey;
import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.notFound;
import static java.time.ZoneId.systemDefault;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * Shows bundle / plugin information
 */
@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/") @jakarta.ws.rs.Path("/")
public class BundlesResource {

    private final BundleInspector bundleInspector;
    private final Links links;

    @Inject @jakarta.inject.Inject
    public BundlesResource(final BundleInspector bundleInspector, Links links) {
        this.bundleInspector = bundleInspector;
        this.links = links;
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/services") @jakarta.ws.rs.Path("/services")
    public AllServices getBundleServices() {
        return bundleServices();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/bundles") @jakarta.ws.rs.Path("/bundles")
    public AllBundles getAllBundles() {
        return bundles(pluginBundle -> true);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/plugins") @jakarta.ws.rs.Path("/plugins")
    public AllBundles getAllPlugins() {
        return bundles(pluginsOnly());
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/plugins/{pluginKey}") @jakarta.ws.rs.Path("/plugins/{pluginKey}")
    public SingleBundle getPlugin(@PathParam("pluginKey")  @jakarta.ws.rs.PathParam("pluginKey") String pluginKey) {
        Optional<SingleBundle> bundle = singlePlugin(pluginKey);
        if (bundle.isPresent()) {
            return bundle.get();
        }
        throw notFound("Could not find the provided plugin: " + pluginKey);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/bundles/{bundleId : \\d+}") @jakarta.ws.rs.Path("/bundles/{bundleId : \\d+}")
    public SingleBundle getBundle(@PathParam("bundleId") @jakarta.ws.rs.PathParam("bundleId") long bundleId) {
        Optional<SingleBundle> bundle = singleBundle(bundleId);
        if (bundle.isPresent()) {
            return bundle.get();
        }
        throw notFound("Could not find the provided bundle: " + bundleId);
    }

    private Predicate<PluginBundle> pluginsOnly() {
        return PluginBundle::isAtlassianPlugin;
    }

    private AllBundles bundles(final Predicate<PluginBundle> predicate) {
        final List<BundleInfo> bundles = new ArrayList<>();
        bundleInspector.inspectBundles(pluginBundle -> {
            if (predicate.test(pluginBundle)) {
                BundleLinks links = createBundleLinks(pluginBundle.getBundle());
                BundleInfo bundleInfo = new BundleInfo(links, pluginBundle.getPluginKey(), pluginBundle.getBundle());
                bundles.add(bundleInfo);
            }
        });
        return new AllBundles(links.apiLinks(), bundles);

    }

    private Optional<SingleBundle> singlePlugin(String pluginKey) {
        Optional<PluginBundle> pluginBundle = bundleInspector.newPluginBundle(pluginKey);
        if (pluginBundle.isPresent()) {
            return singleBundle(pluginBundle.get().getBundle().getBundleId());
        }
        return Optional.empty();
    }


    private Optional<SingleBundle> singleBundle(long bundleId) {
        final List<BeanInfo> beans = new ArrayList<>();
        final AtomicReference<PluginBundle> pluginBundleRef = new AtomicReference<>();
        final AtomicReference<ApplicationContext> springContextRef = new AtomicReference<>();
        bundleInspector.inspectBundle(bundleId, new BundleInspector.BeanCallback() {
            @Override
            public void onStart(final PluginBundle pluginBundle) {
                pluginBundleRef.set(pluginBundle);
            }

            @Override
            public void onSpringContext(final ApplicationContext applicationContext) {
                springContextRef.set(applicationContext);
            }

            @Override
            public void onSpringBean(final BundleInspector.BeanInspectionDefinition def) {
                BeanInfo bean;
                Optional<String> beanClassGuess = def.bean().map(bundleInspector::guessBeanClass);
                Optional<String> beanException = def.beanException().map(BundlesResource.this::makeExceptionStr);
                String[] beanAndClass = beanAndClass(def.bean().orElse(null));
                bean = new BeanInfo(def.beanName(), beanAndClass[0], beanAndClass[1], beanClassGuess.orElse(null), beanException.orElse(null), def.singleton(), def.prototype());
                beans.add(bean);
            }
        });
        Collections.sort(beans);

        PluginBundle pluginBundle = pluginBundleRef.get();
        if (pluginBundle == null) {
            return Optional.empty();
        }
        Bundle bundle = pluginBundle.getBundle();
        //
        // full info here thanks
        Services offeredServices = buildOfferedServices(bundle);
        Services consumedServices = buildConsumedServices(bundle);
        BundleLinks bundleLinks = createBundleLinks(bundle);
        FullBundleInfo bundleInfo = new FullBundleInfo(bundleLinks, pluginBundle.getPluginKey(), bundle, offeredServices, consumedServices);
        //
        // possible to have no Spring context - pure OSGi bundles don't have one say
        //
        SpringInfo springInfo = null;
        Optional<ApplicationContext> applicationContext = Optional.ofNullable(springContextRef.get());
        if (applicationContext.isPresent()) {
            springInfo = new SpringInfo(applicationContext.get(), beans);
        }
        //
        // get the plugin if if its there
        final List<ModuleInfo> modules = new ArrayList<>();
        final Optional<Plugin> plugin = bundleInspector.inspectBundlePluginModules(bundleId, (moduleDescriptor, enabled) -> modules.add(toModuleInfo(moduleDescriptor)));
        PluginInfo pluginInfo = null;
        if (plugin.isPresent()) {
            pluginInfo = toPluginInfo(plugin.get(), modules);
        }
        return Optional.of(new SingleBundle(links.apiLinks(), buildBundleDescriptiveName(bundle), bundleInfo, springInfo, pluginInfo));
    }

    /**
     * Its possible for the bean to explode on contact, maybe with an OSGI service not available etc so lets not
     * explode
     *
     * @param bean - the bean to inspect
     *
     * @return a pair of bean toString() and bean.getClass() as a string
     */
    private String[] beanAndClass(final Object bean) {
        try {
            return new String[]{String.valueOf(bean), bean == null ? "null" : bean.getClass().getName()};
        } catch (Exception e) {
            return new String[]{e.toString(), e.getClass().getName()};
        }
    }

    private AllServices bundleServices() {
        final List<ServiceInfo> services = new ArrayList<>();
        bundleInspector.inspectBundleServices((pluginBundle, service) -> services.add(toServiceInfo(true,true).apply(service)));
        return new AllServices(links.apiLinks(), services);
    }

    private PluginInfo toPluginInfo(final Plugin p2, final List<ModuleInfo> modules) {
        final PluginInfo pluginInfo;
        PluginInformation information = p2.getPluginInformation();
        pluginInfo = new PluginInfo(p2.getKey(), p2.getName(), information.getDescription(), information.getVersion(), information.getVendorName(), bundleInspector.isEnabled(p2), bundleInspector.isSystemPlugin(p2), createPluginLinks(p2), modules);
        return pluginInfo;
    }

    private ModuleInfo toModuleInfo(final ModuleDescriptor<?> moduleDescriptor) {
        return new ModuleInfo(moduleDescriptor.getCompleteKey(), moduleDescriptor.getName(), moduleDescriptor.getDescription(), moduleDescriptor.getClass().getName(), String.valueOf(moduleDescriptor.getModuleClass()), true, createModuleLinks(moduleDescriptor));
    }

    private ModuleLinks createModuleLinks(final ModuleDescriptor<?> moduleDescriptor) {
        return links.moduleLinks(moduleDescriptor.getCompleteKey(), moduleDescriptor.getPluginKey());
    }

    private PluginLinks createPluginLinks(final Plugin p2) {
        return links.pluginLinks(p2.getKey());
    }

    private static String buildBundleDescriptiveName(final Bundle bundle) {
        return String.format("%s - %s (%d)", bundle.getHeaders().get("Bundle-Name"), bundle.getSymbolicName(), bundle.getBundleId());
    }

    private static String buildBundleName(final Bundle bundle) {
        Object name = bundle.getHeaders().get("Bundle-Name");
        return Objects.toString(name, bundle.getSymbolicName());
    }

    private Services buildOfferedServices(final Bundle bundle) {
        List<BundleInspector.ServiceRef> serviceRefs = bundleInspector.inspectRegisteredServices(bundle);
        Collection<ServiceInfo> services = serviceRefs.stream().map(toServiceInfo(false, true)).collect(toList());
        Collection<String> servicesSummary = serviceRefs.stream().map(toServiceSummary()).collect(toList());
        return new Services(servicesSummary, services);
    }

    private Services buildConsumedServices(final Bundle bundle) {
        List<BundleInspector.ServiceRef> serviceRefs = bundleInspector.inspectServicesInUse(bundle);
        Collection<ServiceInfo> services = serviceRefs.stream().map(toServiceInfo(true, false)).collect(toList());
        Collection<String> servicesSummary = serviceRefs.stream().map(toServiceSummary()).collect(toList());
        return new Services(servicesSummary, services);
    }

    private Function<BundleInspector.ServiceRef, ServiceInfo> toServiceInfo(final boolean includeOfferingBundles, boolean includeUsingBundles) {
        return from -> {
            BundleLinks offeringBundle = null;
            if (includeOfferingBundles) {
                offeringBundle = toBundleLinks().apply(from.offeringBundle);
            }
            Collection<BundleLinks> usingLinks = null;
            if (includeUsingBundles) {
                usingLinks = from.usingBundles.stream().map(toBundleLinks()).collect(toList());
            }
            return new ServiceInfo(from.serviceId, from.objectClasses, offeringBundle, from.serviceProperties, usingLinks);
        };
    }

    private Function<BundleInspector.ServiceRef, String> toServiceSummary() {
        return from -> from.serviceId +
                " - " +
                StringUtils.join(from.objectClasses, ",");
    }

    private Function<Bundle, BundleLinks> toBundleLinks() {
        return from -> createBundleLinks(from);
    }

    private String makeExceptionStr(final Exception from) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        from.printStackTrace(pw);
        return sw.toString();
    }

    BundleLinks createBundleLinks(Bundle bundle) {
        return links.bundleLinks(buildBundleDescriptiveName(bundle), bundle.getBundleId(), getPluginKey(bundle));
    }

    //-----------------------------
    //
    // JSON Structures
    //
    //-----------------------------

    static class AllServices {
        public final Collection<ServiceInfo> serviceInfo;
        public final ApiLinks api;

        AllServices(ApiLinks api, final Collection<ServiceInfo> services) {
            this.api = api;
            this.serviceInfo = services;
        }
    }

    static class AllBundles {
        public final Collection<BundleInfo> bundles;
        public final ApiLinks api;

        AllBundles(ApiLinks api, final Collection<BundleInfo> bundles) {
            this.api = api;
            this.bundles = bundles;
        }
    }

    static class SingleBundle {
        public final String name;
        public final SpringInfo springInfo;
        public final FullBundleInfo bundleInfo;
        public final PluginInfo pluginInfo;
        public final ApiLinks api;

        SingleBundle(final ApiLinks api, final String name, final FullBundleInfo bundleInfo, final SpringInfo springInfo, final PluginInfo pluginInfo) {
            this.name = name;
            this.pluginInfo = pluginInfo;
            this.api = api;
            this.bundleInfo = bundleInfo;
            this.springInfo = springInfo;
        }
    }


    private static final Map<Integer, String> STATES = new HashMap<>();

    static {
        STATES.put(Bundle.ACTIVE, "ACTIVE");
        STATES.put(Bundle.INSTALLED, "INSTALLED");
        STATES.put(Bundle.RESOLVED, "RESOLVED");
        STATES.put(Bundle.STARTING, "STARTING");
        STATES.put(Bundle.STOPPING, "STOPPING");
        STATES.put(Bundle.UNINSTALLED, "UNINSTALLED");
    }

    static class BundleInfo implements Comparable<BundleInfo> {
        public final long bundleId;
        public final String name;
        public final String version;
        public final String state;
        public final String bundleSymbolicName;
        public final String pluginKey;
        public final String location;
        public final BundleLinks links;

        BundleInfo(final BundleLinks links, final String pluginKey, final Bundle bundle) {
            this.links = links;
            this.pluginKey = pluginKey;
            this.name = buildBundleName(bundle);
            this.bundleSymbolicName = bundle.getSymbolicName();
            this.bundleId = bundle.getBundleId();
            this.location = bundle.getLocation();
            this.version = bundle.getVersion().toString();
            this.state = STATES.get(bundle.getState());
        }

        @Override
        public int compareTo(BundleInfo o) {
            return Long.compare(this.bundleId,o.bundleId);
        }
    }

    static class ServiceInfo {
        public final String serviceId;
        public final Collection<String> objectClasses;
        public final Map<String, String> serviceProperties;
        public final BundleLinks offeringBundle;
        public final Collection<BundleLinks> usingBundles;

        ServiceInfo(final String serviceId, final Collection<String> objectClasses, final BundleLinks offeringBundle, final Map<String, String> serviceProperties, final Collection<BundleLinks> usingBundles) {
            this.serviceId = serviceId;
            this.objectClasses = objectClasses;
            this.offeringBundle = offeringBundle;
            this.serviceProperties = serviceProperties;
            this.usingBundles = usingBundles;
        }
    }


    static class Services {
        public final Collection<String> servicesSummary;
        public final Collection<ServiceInfo> services;

        Services(Collection<String> servicesSummary, final Collection<ServiceInfo> services) {
            this.servicesSummary = servicesSummary;
            this.services = services;
        }
    }

    static class FullBundleInfo {
        public final long bundleId;
        public final String name;
        public final String version;
        public final String state;
        public final String bundleSymbolicName;
        public final String pluginKey;
        public final String location;
        public final Services offeredServices;
        public final Services consumedServices;
        public final Collection<BundleHeaderParser.ParsedHeader> parsedHeaders;
        public final Map<String, String> simpleHeaders;
        public final BundleLinks links;

        FullBundleInfo(final BundleLinks bundleLinks, final String pluginKey, final Bundle bundle, final Services offeredServices, final Services consumedServices) {
            this.links = bundleLinks;
            this.pluginKey = pluginKey;
            this.name = buildBundleName(bundle);
            this.bundleSymbolicName = bundle.getSymbolicName();
            this.bundleId = bundle.getBundleId();
            this.location = bundle.getLocation();
            this.version = bundle.getVersion().toString();
            this.state = STATES.get(bundle.getState());

            this.parsedHeaders = BundleHeaderParser.getParseableHeaders(bundle);
            this.simpleHeaders = BundleHeaderParser.getSimpleHeaders(bundle);
            this.offeredServices = offeredServices;
            this.consumedServices = consumedServices;
        }
    }

    static class PluginInfo {
        public final String pluginKey;
        public final String name;
        public final String description;
        public final String version;
        public final boolean enabled;
        public final String vendor;
        public final boolean systemPlugin;
        public final Collection<ModuleInfo> modules;
        public final PluginLinks links;

        PluginInfo(final String pluginKey, final String name, final String description, final String version, final String vendor, final boolean enabled, final boolean systemPlugin, final PluginLinks links, final Collection<ModuleInfo> modules) {
            this.pluginKey = pluginKey;
            this.name = name;
            this.description = description;
            this.version = version;
            this.vendor = vendor;
            this.enabled = enabled;
            this.systemPlugin = systemPlugin;
            this.links = links;
            this.modules = modules;
        }
    }

    static class ModuleInfo {
        public final String completeKey;
        public final String name;
        public final String description;
        public final String moduleDescriptorClass;
        public final String moduleClass;
        public final boolean enabled;
        public final ModuleLinks moduleLinks;


        ModuleInfo(final String completeKey, final String name, final String description, final String moduleDescriptorClass, final String moduleClass, final boolean enabled, final ModuleLinks moduleLinks) {
            this.completeKey = completeKey;
            this.name = name;
            this.description = description;
            this.moduleDescriptorClass = moduleDescriptorClass;
            this.moduleClass = moduleClass;
            this.enabled = enabled;
            this.moduleLinks = moduleLinks;
        }
    }

    static class SpringInfo {
        public final String id;
        public final String displayName;
        public final String startDate;
        public final int beanCount;
        public final Collection<BeanInfo> beans;

        SpringInfo(final ApplicationContext applicationContext, final Collection<BeanInfo> beans) {
            this.id = applicationContext.getId();
            this.displayName = applicationContext.getDisplayName();
            Instant startupDateInstant = Instant.ofEpochMilli(applicationContext.getStartupDate());
            this.startDate = ZonedDateTime.ofInstant(startupDateInstant, systemDefault()).format(ISO_OFFSET_DATE_TIME);
            this.beanCount = beans.size();
            this.beans = beans;
        }
    }

    static class BeanInfo implements Comparable<BeanInfo>{
        public final String beanName;
        public final String bean;
        public final String beanClass;
        public final String beanClassGuess;
        public final String beanException;
        public final boolean singleton;
        public final boolean prototype;

        BeanInfo(final String beanName, final String bean, final String beanClass, final String beanClassGuess, final String beanException, final boolean singleton, final boolean prototype) {
            this.beanName = beanName;
            this.bean = bean == null ? null : String.valueOf(bean);
            this.beanClass = beanClass;
            this.beanClassGuess = beanClassGuess;
            this.beanException = beanException;
            this.singleton = singleton;
            this.prototype = prototype;
        }

        @Override
        public int compareTo(BeanInfo o) {
            return this.beanName.compareTo(o.beanName);
        }
    }

}
