package com.atlassian.labs.plugins.quickreload.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 * A simple thread factory to name the executor threads of this plugin
 */
public class QuickReloadThreads implements ThreadFactory
{
    private final String name;

    QuickReloadThreads(final String name)
    {
        this.name = "QuickReload - " + name;
    }

    @Override
    public Thread newThread(final Runnable runnable)
    {
        return new Thread(runnable, name);
    }

    public static QuickReloadThreads forClass(Class clazz)
    {
        return new QuickReloadThreads(clazz.getSimpleName());
    }

    public static QuickReloadThreads withName(String name)
    {
        return new QuickReloadThreads(name);
    }

    public static ExecutorService singleThreadExecutorForClass(Class clazz)
    {
        return Executors.newSingleThreadExecutor(forClass(clazz));
    }

    public static ExecutorService singleThreadExecutorWithName(String name)
    {
        return Executors.newSingleThreadExecutor(withName(name));
    }

    public static ScheduledExecutorService newSingleThreadScheduledExecutor(String name) {
        return Executors.newSingleThreadScheduledExecutor(withName(name));
    }
}
