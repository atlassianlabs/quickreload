package com.atlassian.labs.plugins.quickreload;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The current configuration variables of the QuickReload system
 */
public class QrConfiguration
{
    public static final String WEB_RESOURCE_BATCHING_KEY = "webresourcebatching";

    private final Map<String, String> properties = new HashMap<>();

    public void addConfiguration(Map<String, String> properties)
    {
        this.properties.putAll(properties);
    }

    public String getValue(String key)
    {
        return properties.get(key);
    }

    public Optional<Boolean> getFlag(String key) {
        return Optional.ofNullable(getValue(key))
                .map(Boolean::parseBoolean);
    }
}
