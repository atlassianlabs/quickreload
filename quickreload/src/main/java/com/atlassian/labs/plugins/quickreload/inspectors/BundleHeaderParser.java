package com.atlassian.labs.plugins.quickreload.inspectors;

import org.apache.sling.commons.osgi.ManifestHeader;
import org.osgi.framework.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;

import static org.osgi.framework.Constants.BUNDLE_CLASSPATH;
import static org.osgi.framework.Constants.BUNDLE_NATIVECODE;
import static org.osgi.framework.Constants.BUNDLE_REQUIREDEXECUTIONENVIRONMENT;
import static org.osgi.framework.Constants.DYNAMICIMPORT_PACKAGE;
import static org.osgi.framework.Constants.EXPORT_PACKAGE;
import static org.osgi.framework.Constants.FRAGMENT_HOST;
import static org.osgi.framework.Constants.IMPORT_PACKAGE;
import static org.osgi.framework.Constants.REQUIRE_BUNDLE;

/**
 * Can parse headers inside an OSGi bundle
 */
public class BundleHeaderParser
{
    private static final Predicate<String> PARSED_HEADERS = Arrays.asList(
            BUNDLE_CLASSPATH,
            BUNDLE_NATIVECODE,
            BUNDLE_REQUIREDEXECUTIONENVIRONMENT,
            DYNAMICIMPORT_PACKAGE,
            EXPORT_PACKAGE,
            FRAGMENT_HOST,
            "Ignore-Package",
            IMPORT_PACKAGE,
            "Private-Package",
            REQUIRE_BUNDLE
    )::contains;

    /**
     * REST ready representation of a parsed OSGi header
     */
    public static class ParsedHeader
    {
        public final String name;
        public final Collection<ParsedHeaderEntry> entries;

        ParsedHeader(final String name, final Collection<ParsedHeaderEntry> entries)
        {
            this.name = name;
            this.entries = entries;
        }
    }

    /**
     * REST ready representation of a parsed OSGi header entry
     */
    public static class ParsedHeaderEntry
    {
        public final String path;
        public final Map<String, String> attributes;

        public final Map<String, String> directives;

        ParsedHeaderEntry(final String path, final Map<String, String> attributes, final Map<String, String> directives)
        {
            this.path = path;
            this.attributes = attributes;
            this.directives = directives;
        }
    }


    public static Map<String, String> getSimpleHeaders(Bundle bundle)
    {
        return sortedHeaders(bundle.getHeaders(), PARSED_HEADERS.negate());
    }

    static final Comparator<Object> BY_STRING_VALUE_OF = Comparator.comparing(String::valueOf);

    private static Map<String, String> sortedHeaders(final Dictionary headers, Predicate<String> suitableHeader)
    {
        final Map<String, String> map = new TreeMap<>(BY_STRING_VALUE_OF);
        Enumeration keys = headers.keys();
        while (keys.hasMoreElements())
        {
            String key = String.valueOf(keys.nextElement());
            if (suitableHeader.test(key))
            {
                map.put(key, String.valueOf(headers.get(key)));
            }
        }
        return map;
    }

    public static Collection<ParsedHeader> getParseableHeaders(Bundle bundle)
    {
        List<ParsedHeader> headers = new ArrayList<>();
        Map<String, String> suitableHeaders = sortedHeaders(bundle.getHeaders(), PARSED_HEADERS);
        for (String key : suitableHeaders.keySet())
        {
            headers.add(parseHeader(key, suitableHeaders.get(key)));
        }
        return headers;
    }

    private static ParsedHeader parseHeader(final String headerKey, final String headerValue)
    {
        final ManifestHeader manifestHeader = ManifestHeader.parse(headerValue);
        Collection<ParsedHeaderEntry> headerEntries = makeEntries(manifestHeader);
        return new ParsedHeader(headerKey, headerEntries);
    }

    private static Collection<ParsedHeaderEntry> makeEntries(final ManifestHeader manifestValue)
    {
        List<ParsedHeaderEntry> entries = new ArrayList<>();
        for (ManifestHeader.Entry entry : manifestValue.getEntries())
        {
            entries.add(new ParsedHeaderEntry(entry.getValue(), toMap(entry.getAttributes()), toMap(entry.getDirectives())));
        }
        return entries;
    }

    private static Map<String, String> toMap(final ManifestHeader.NameValuePair[] pairs)
    {
        if (pairs == null || pairs.length == 0)
        {
            return null;
        }
        Map<String, String> nvMap = new HashMap<>();
        for (ManifestHeader.NameValuePair pair : pairs)
        {
            nvMap.put(pair.getName(), pair.getValue());
        }
        return nvMap;
    }
}
