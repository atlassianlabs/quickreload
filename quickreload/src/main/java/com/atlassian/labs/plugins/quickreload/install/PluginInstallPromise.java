package com.atlassian.labs.plugins.quickreload.install;

import com.atlassian.labs.plugins.quickreload.utils.Timer;

import java.io.File;

/**
 * A promise to install a plugin in the future.  Plugin jars can turn up via a tryIn as the zip file is created but they
 * are NOT yet valid (eg maven might still be filling it with data) so we need a delayed mechanism to install plugins
 */
public class PluginInstallPromise
{
    private final Timer timer;
    private final File pluginFile;
    private final long attempts;
    private final String fullPath;

    private PluginInstallPromise(long attempts, File pluginFile, Timer timer)
    {
        this.pluginFile = pluginFile;
        this.fullPath = pluginFile.getAbsolutePath();
        this.attempts = attempts;
        this.timer = timer;
    }

    public static PluginInstallPromise promise(File pluginFile)
    {
        return new PluginInstallPromise(0, pluginFile, new Timer());
    }

    public static PluginInstallPromise retryFailureAgain(PluginInstallPromise promise)
    {
        return new PluginInstallPromise(promise.attempts + 1, promise.pluginFile, promise.timer);
    }

    public File getPluginFile()
    {
        return pluginFile;
    }

    public long getAttempts()
    {
        return attempts;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final PluginInstallPromise that = (PluginInstallPromise) o;

        return fullPath.equals(that.fullPath);

    }

    @Override
    public int hashCode()
    {
        return fullPath.hashCode();
    }

    @Override
    public String toString()
    {
        //noinspection StringBufferReplaceableByString
        return new StringBuilder(super.toString())
                .append(" attempts : ").append(attempts)
                .append(" : ").append(fullPath)
                .toString();
    }
}


