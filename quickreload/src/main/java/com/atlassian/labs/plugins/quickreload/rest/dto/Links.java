package com.atlassian.labs.plugins.quickreload.rest.dto;

import com.atlassian.labs.plugins.quickreload.utils.VersionKit;
import com.atlassian.sal.api.ApplicationProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.atlassian.labs.plugins.quickreload.rest.dto.Links.LinkBuilder.restLinkBuilder;
import static com.atlassian.labs.plugins.quickreload.rest.dto.ResourceLink.delete;
import static com.atlassian.labs.plugins.quickreload.rest.dto.ResourceLink.get;
import static com.atlassian.labs.plugins.quickreload.rest.dto.ResourceLink.post;
import static com.atlassian.labs.plugins.quickreload.rest.dto.ResourceLink.put;
import static com.atlassian.sal.api.UrlMode.ABSOLUTE;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.concat;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.apache.commons.lang3.StringUtils.removeStart;

public class Links {
    private final ApplicationProperties applicationProperties;

    public Links(ApplicationProperties applicationProperties) {
        this.applicationProperties = requireNonNull(applicationProperties, "applicationProperties");
    }

    public ApiLinks apiLinks() {
        return new ApiLinks();
    }

    public BundleLinks bundleLinks(final String name, final long bundleId, final String pluginKey) {
        return new BundleLinks(name, bundleId, pluginKey);
    }

    public ModuleLinks moduleLinks(final String moduleKey, final String pluginKey) {
        return new ModuleLinks(moduleKey, pluginKey);
    }

    public PluginLinks pluginLinks(String pluginKey) {
        return new PluginLinks(pluginKey);
    }

    public class BundleLinks {
        public final String name;
        public final ResourceLink self;
        public final ResourceLink upmRepresentation;
        public final ResourceLink enabledState;
        public final ResourceLink enable;
        public final ResourceLink disable;

        private BundleLinks(final String name, final long bundleId, final String pluginKey) {
            LinkBuilder linkBuilder = restLinkBuilder(applicationProperties);

            this.name = name;
            this.self = get(linkBuilder.addToPath("bundles").addToPath(bundleId));
            this.upmRepresentation = get(linkBuilder.setBaseServlet("rest/plugins/1.0/bundles").addToPath(bundleId));

            final LinkBuilder enabledUrl = linkBuilder.addToPath("plugin/enabled").addToPath(pluginKey);
            this.enabledState = get(enabledUrl);
            this.enable = post(enabledUrl);
            this.disable = delete(enabledUrl);
        }
    }

    public class PluginLinks {
        public final ResourceLink enabledState;
        public final ResourceLink enable;
        public final ResourceLink disable;
        public final ResourceLink pluginInfo;

        private PluginLinks(final String pluginKey) {
            LinkBuilder linkBuilder = restLinkBuilder(applicationProperties);

            this.pluginInfo = get(linkBuilder.addToPath("plugins").addToPath(pluginKey));

            final LinkBuilder enableUri = linkBuilder.addToPath("plugin/enabled").addToPath(pluginKey);
            this.enabledState = get(enableUri);
            this.enable = post(enableUri);
            this.disable = delete(enableUri);
        }
    }

    public class ModuleLinks {
        public final ResourceLink enabledState;
        public final ResourceLink enable;
        public final ResourceLink disable;
        public final ResourceLink pluginInfo;

        private ModuleLinks(final String moduleKey, final String pluginKey) {
            LinkBuilder linkBuilder = restLinkBuilder(applicationProperties);

            final LinkBuilder pluginEnabledUrl = linkBuilder.addToPath("plugin/enabled").addToPath(moduleKey);
            this.enabledState = get(pluginEnabledUrl);
            this.enable = post(pluginEnabledUrl);
            this.disable = delete(pluginEnabledUrl);

            final LinkBuilder pluginUrl = linkBuilder.addToPath("plugin").addToPath(moduleKey);
            this.pluginInfo = get(pluginUrl.addToPath(pluginKey));
        }
    }

    public class ApiLinks {
        public final String qrVersion;
        public final ResourceLink api;

        public final ResourceLink bundles;
        public final ResourceLink bundleInformation;
        public final ResourceLink plugins;
        public final ResourceLink pluginInformation;
        public final ResourceLink services;
        public final ResourceLink events;

        public final ResourceLink systemproperties;
        public final ResourceLink systempropertiesPut;
        public final ResourceLink systempropertiesDelete;

        public final ResourceLink controlPanel;

        public final ResourceLink state;
        public final ResourceLink stateEnable;
        public final ResourceLink stateDisable;

        public final ResourceLink pluginInstall;
        public final ResourceLink pluginUninstall;

        public final ResourceLink pluginUpload;

        public final ResourceLink pluginState;
        public final ResourceLink pluginEnable;
        public final ResourceLink pluginDisable;

        public final ResourceLink tracked;
        public final ResourceLink track;
        public final ResourceLink untrack;
        public final ResourceLink trackedResourceDirs;
        public final ResourceLink trackResourceDir;
        public final ResourceLink untrackResourceDir;

        public final ResourceLink batching;
        public final ResourceLink batchingEnable;
        public final ResourceLink batchingDisable;
        public final ResourceLink batchingSetState;

        private ApiLinks() {
            LinkBuilder linkBuilder = restLinkBuilder(applicationProperties);

            this.qrVersion = VersionKit.getDisplayedVersion();

            this.api = get(linkBuilder.addToPath("api"));

            LinkBuilder stateUrl = linkBuilder.addToPath("state");
            this.state = get(stateUrl);
            this.stateEnable = put(stateUrl);
            this.stateDisable = delete(stateUrl);

            LinkBuilder installPluginUrl = linkBuilder.addToPath("plugin/install");
            this.pluginInstall = post(installPluginUrl.addToPath("{fullJarPath}"));
            this.pluginUninstall = delete(linkBuilder.addToPath("{pluginKey}"));

            this.pluginUpload = post(linkBuilder.addToPath("plugin/upload"));

            LinkBuilder pluginEnabledUrl = linkBuilder.addToPath("/plugin/enabled/{pluginOrModuleKey}");
            this.pluginState = get(pluginEnabledUrl);
            this.pluginEnable = post(pluginEnabledUrl);
            this.pluginDisable = delete(pluginEnabledUrl);

            LinkBuilder trackedUrl = linkBuilder.addToPath("tracked");
            this.tracked = get(trackedUrl);
            LinkBuilder trackedDirectoryUrl = trackedUrl.addToPath("{fullDirectoryPath}");
            this.track = post(trackedDirectoryUrl);
            this.untrack = delete(trackedDirectoryUrl);

            LinkBuilder trackedResourcesUrl = linkBuilder.addToPath("resources");
            this.trackedResourceDirs = get(trackedResourcesUrl);
            LinkBuilder trackedResourceDirectoryUrl = trackedResourcesUrl.addToPath("{fullDirectoryPath}");
            this.trackResourceDir = put(trackedResourceDirectoryUrl);
            this.untrackResourceDir = delete(trackedResourceDirectoryUrl);

            LinkBuilder batchingURL = linkBuilder.addToPath("batching");
            this.batching = get(batchingURL);
            this.batchingEnable = put(batchingURL);
            this.batchingDisable = delete(batchingURL);

            this.batchingSetState = get(linkBuilder.addToPath("batching/setState"), "?enabled={true|false}");

            this.bundles = get(linkBuilder.addToPath("bundles"));
            this.bundleInformation = get(linkBuilder.addToPath("bundles").addToPath("{bundleId}"));
            this.plugins = get(linkBuilder.addToPath("plugins"));
            this.pluginInformation = get(linkBuilder.addToPath("plugins").addToPath("{pluginKey}"));

            this.services = get(linkBuilder.addToPath("services"));

            this.events = get(linkBuilder.addToPath("events"));

            LinkBuilder sysPropURL = linkBuilder.addToPath("systemproperties");
            this.systemproperties = get(sysPropURL);
            this.systempropertiesPut = put(sysPropURL.addToPath("{name}").addToPath("{value}"));
            this.systempropertiesDelete = delete(sysPropURL.addToPath("{name}"));

            this.controlPanel = get(linkBuilder.setBaseServlet("plugins/servlet/qr"));
        }
    }

    public static class LinkBuilder implements Cloneable {
        private static final String REST_PLUGINS_QR = "rest/qr";

         // Immutability makes it easier to use the builder since it's always cloning
        private final String absoluteBaseUrl;
        private final String baseServletPathPart;
        private final List<String> pathParts;

        private LinkBuilder(String absoluteBaseUrl, String baseServletPathPart, List<String> pathParts) {
            this.absoluteBaseUrl = absoluteBaseUrl;
            this.baseServletPathPart = baseServletPathPart;
            this.pathParts = unmodifiableList(pathParts);
        }

        public static LinkBuilder restLinkBuilder(ApplicationProperties applicationProperties) {
            return new LinkBuilder(applicationProperties.getBaseUrl(ABSOLUTE), REST_PLUGINS_QR, emptyList());
        }

        public LinkBuilder addToPath(long part) {
            return addToPath(String.valueOf(part));
        }

        public LinkBuilder addToPath(String part) {
            List<String> newPathParts = new ArrayList<>(pathParts);
            newPathParts.add(part);
            return new LinkBuilder(absoluteBaseUrl, baseServletPathPart, newPathParts);
        }

        public LinkBuilder setBaseServlet(String baseServletPathPart) {
            return new LinkBuilder(absoluteBaseUrl, baseServletPathPart, pathParts);
        }

        /**
         * Won't mess with whether consumers have got a trailing '/' or not, but will
         * build a valid URL even if arguments have a '/' at the start or end of each part.
         */
        public String build() {
            return concat(
                    Stream.of(removeEnd(absoluteBaseUrl, "/"),
                            removeStart(removeEnd(baseServletPathPart, "/"), "/")),
                    pathParts.stream()
                            .map(part -> removeStart(removeEnd(part, "/"), "/"))
            ).collect(joining("/"));
        }
    }
}
