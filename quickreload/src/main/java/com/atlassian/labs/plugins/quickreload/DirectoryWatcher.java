package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.install.PluginInstaller;
import com.atlassian.labs.plugins.quickreload.utils.QuickReloadThreads;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * The implementation is taken heavily from the FastDev plugin.
 * <p>
 * When a filesystem change event is detected we also gather all the events and only broadcast a single event per
 * file. (e.g. linux gives both a delete and create for mods but we only want 1 change event)
 */
public class DirectoryWatcher implements LifecycledComponent {
    private static final Logger log = setInfo(getLogger(DirectoryWatcher.class));

    private final ExecutorService exec;
    private final WatchService watchService;
    private final DirectoryTracker directoryTracker;
    private final PluginInstaller pluginInstaller;
    private final Map<WatchKey, Path> watchKeysToPaths;
    private final Map<File, WatchKey> directoriesToWatchKeys;
    private final Map<File, Function<File, Void>> specificFilesToWatch;
    private final Map<WatchKey, Path> watchKeysToParentPaths;
    private final Map<File, WatchKey> parentDirectoriesToWatchKeys;

    public DirectoryWatcher(DirectoryTracker directoryTracker, PluginInstaller pluginInstaller)
    {
        this.watchService = WatchServiceHelper.getWatchService();
        this.directoryTracker = directoryTracker;
        this.pluginInstaller = pluginInstaller;
        this.watchKeysToPaths = new HashMap<>();
        this.watchKeysToParentPaths = new HashMap<>();
        this.directoriesToWatchKeys = new HashMap<>();
        this.parentDirectoriesToWatchKeys = new HashMap<>();
        this.specificFilesToWatch = new HashMap<>();
        this.exec = QuickReloadThreads.singleThreadExecutorForClass(getClass());
    }

    public void onStartup()
    {
        onRefresh();
        exec.execute(this::watchingThread);
    }

    public void onShutdown()
    {
        try
        {
            watchService.close();
            exec.shutdown();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void onRefresh()
    {
        for (File directory : directoryTracker.getTracked())
        {
            watch(directory);
        }
    }

    public void watchSpecificFile(File f, Function<File, Void> callBack)
    {
        log.info("Watching specific file: {}", f);
        watch(f);
        specificFilesToWatch.put(f, callBack);
    }

    /**
     * Called to add a directory to the list of watched directories
     *
     * @param directory the directory to add
     */
    public void watch(final File directory) {
        try {
            if (directoryTracker.isBlackListed(directory)) {
                log.warn("{} was blacklisted. It will not be watched", directory.toString());
                return;
            }

            if (directory.exists() && directory.isDirectory()) {
                log.info("Monitoring directory '{}'...", directory);
                // the watch code can only watch directories
                WatchKey key = WatchServiceHelper.registerDirectoryForWatching(watchService, directory);
                watchKeysToPaths.put(key, directory.toPath());
                directoriesToWatchKeys.put(directory, key);
            }

            // if the watched directory is deleted (say via maven clean) then the watch key becomes invalid and we will
            // never be told when it "turns" up again.  So we watch its parent to see it come back into existence and then
            // we re-install the the child as a new watch key
            File parentFile = directory.getParentFile();
            if (parentFile != null && parentFile.exists()) {
                log.debug("Monitoring parent directory '{}'...", parentFile);
                WatchKey key = WatchServiceHelper.registerDirectoryForWatching(watchService, parentFile);
                watchKeysToParentPaths.put(key, parentFile.toPath());
                parentDirectoriesToWatchKeys.put(parentFile, key);
            } else {
                log.warn("The parent directory of '{}' does not exist. " +
                        "This file will not be watched for changes", directory);
            }
        } catch (Exception e) {
            log.error("Unable to watch directory '{}' - It will not be monitored!", directory, e);
        }
    }

    public void unwatch(final File trackedDir)
    {
        WatchKey watchKey = directoriesToWatchKeys.get(trackedDir);
        if (watchKey != null)
        {
            watchKeysToPaths.remove(watchKey);
            directoriesToWatchKeys.remove(trackedDir);
            watchKey.cancel();
        }

        watchKey = parentDirectoriesToWatchKeys.get(trackedDir.getParentFile());
        if (watchKey != null)
        {
            parentDirectoriesToWatchKeys.remove(trackedDir.getParentFile());
            watchKeysToParentPaths.remove(watchKey);
            watchKey.cancel();
        }
    }

    /**
     * This function runs in an executor thread since it waits on a watch key queue.
     */
    private void watchingThread()
    {
        while (!exec.isShutdown())
        {
            // take() will block until a file has been created/deleted
            WatchKey signalledKey;
            try
            {
                signalledKey = watchService.take();
            }
            catch (Exception breakOut)
            {
                // other thread closed watch service
                break;
            }

            // get list of events from key
            List<WatchEvent<?>> events = signalledKey.pollEvents();

            // VERY IMPORTANT! call reset() AFTER pollEvents() to allow the
            // key to be reported again by the watch service
            signalledKey.reset();

            //get the affected directory
            Path parentPath = watchKeysToPaths.get(signalledKey);
            if (parentPath == null)
            {
                // ok it might have been a parent directory change
                parentPath = watchKeysToParentPaths.get(signalledKey);
            }

            if (null != parentPath)
            {
                Set<ChangeType> changes = determineChangeTypes(events, parentPath);

                for (ChangeType change : changes)
                {
                    File changedFile = change.file;

                    checkForTrackedComingsAndGoings(change);

                    Function<File, Void> callBack = specificFilesToWatch.get(changedFile);
                    if (callBack != null)
                    {
                        callBack.apply(changedFile);
                    }

                    // is it a change to a file in a directory we track and hence perhaps a plugin inside it?
                    if (directoryTracker.isTracked(changedFile.getParentFile()))
                    {
                        pluginInstaller.promiseToInstall(changedFile);
                    }
                }
            }
        }
    }

    private Set<ChangeType> determineChangeTypes(final List<WatchEvent<?>> events, final Path parentPath)
    {
        Set<ChangeType> changes = new HashSet<ChangeType>();

        //loop over the events and grab the ones we care about
        for (WatchEvent<?> e : events)
        {
            WatchEvent.Kind<?> kind = e.kind();
            if (kind.equals(ENTRY_CREATE) || kind.equals(ENTRY_DELETE) || kind.equals(ENTRY_MODIFY))
            {
                String fullPath = WatchServiceHelper.getFullPath((Path) e.context(), parentPath);
                File changedFile = new File(fullPath);
                changes.add(new ChangeType(changedFile, kind));

                log.debug("{} - {} - {}", kind.name(), fullPath, changedFile.length());
            }
            //if we get too many events, we just won't fire a change
            else if (kind.equals(StandardWatchEventKinds.OVERFLOW))
            {
                log.debug("OVERFLOW: more changes happened than we could retrieve");
            }
        }
        return changes;
    }

    /**
     * If a tracked directory is deleted then the jwatch service will loses it abaility to track it in the future.  So
     * we have to watch for their comings and goings and clean up when they go and re-register when they come back.  We
     * can do all this because we are watching their parent as well.
     *
     * @param change the type of change
     */
    private void checkForTrackedComingsAndGoings(final ChangeType change)
    {
        File changedFile = change.file;
        //
        // is it a change to a directory we track?
        if (directoryTracker.isTracked(changedFile))
        {
            if (change.kind == ENTRY_DELETE)
            {
                // ok it is one our guys so we need to clean up our entries.  we will latter add a watch key
                // when the directory turns up again, because we are watching the parent.
                WatchKey watchKey = directoriesToWatchKeys.get(changedFile);
                watchKeysToPaths.remove(watchKey);
            }

            else if (change.kind == ENTRY_CREATE)
            {
                // ok a prodigal son is back and we need to re-register them with the watch service because it
                // loses it mind when they where previous deleted
                watch(changedFile);
            }
        }
    }

    private static class ChangeType
    {
        private final File file;
        private final WatchEvent.Kind kind;

        private ChangeType(final File file, final WatchEvent.Kind kind)
        {
            this.file = file;
            this.kind = kind;
        }
    }
}
