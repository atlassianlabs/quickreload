package com.atlassian.labs.plugins.quickreload.inspectors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.joor.Reflect;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.springframework.context.ApplicationContext;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atlassian.labs.plugins.quickreload.utils.ArrayKit.asList;

/**
 * Inspects a bundle for a series of plugins
 */
public class BundleInspector
{
    private static final Comparator<Bundle> BY_BUNDLE_ID = Comparator.comparingLong(Bundle::getBundleId);

    private static final Function<Object, String> TO_STRING_OR_NULL = from -> from == null ? null : from.toString();

    private static final Comparator<Object> BY_STRING_VALUE_OF = Comparator.comparing(String::valueOf);

    private static final String ATLASSIAN_PLUGIN_KEY = "Atlassian-Plugin-Key";

    private final BundleContext bundleContext;
    private final PluginAccessor pluginAccessor;


    public BundleInspector(final BundleContext bundleContext, final PluginAccessor pluginAccessor)
    {
        this.bundleContext = bundleContext;
        this.pluginAccessor = pluginAccessor;
    }

    public interface BeanInspectionDefinition
    {
        String beanName();

        boolean singleton();

        boolean prototype();

        /**
         * Some times its possible when asking for a bean for it to blow up.  You get an exception or a bean
         *
         * @return an optional bean
         */
        Optional<Object> bean();

        /**
         * Some times its possible when asking for a bean for it to blow up.  You get an exception or a bean
         * @return an optional exception if it blows up while asking for it
         */
        Optional<RuntimeException> beanException();
    }

    public interface BeanCallback
    {
        void onStart(PluginBundle pluginBundle);

        void onSpringContext(ApplicationContext applicationContext);

        void onSpringBean(BeanInspectionDefinition beanDefinition);
    }

    public interface BundleCallback
    {
        void onBundle(PluginBundle pluginBundle);
    }

    public interface BundleServiceCallback
    {
        void onBundle(PluginBundle pluginBundle, ServiceRef service);
    }

    /**
     * A callback inspection of all services (by objectClass) in the OSGI system
     *
     * @param callback the callback on each unique service
     */
    public void inspectBundleServices(BundleServiceCallback callback)
    {
        final Set<String> seen = new HashSet<>();
        final List<Bundle> bundles = asList(bundleContext.getBundles());
        for (Bundle bundle : bundles)
        {
            final List<ServiceRef> serviceRefs = inspectRegisteredServices(bundle);
            serviceRefs.stream().filter(serviceRef -> !seen.contains(serviceRef.serviceId)).forEach(serviceRef -> {
                seen.add(serviceRef.serviceId);

                callback.onBundle(newPluginBundle(bundle), serviceRef);
            });
        }
    }

    /**
     * A callback inspection of all services in the OSGi system
     *
     * @param bundleCallback the callback for each bundle
     */
    public void inspectBundles(BundleCallback bundleCallback)
    {
        Bundle[] b = bundleContext.getBundles();
        if (b == null || b.length == 0)
        {
            return;
        }

        final List<Bundle> bundles = new ArrayList<>();
        Collections.addAll(bundles, b);
        bundles.sort(BY_BUNDLE_ID);

        for (Bundle bundle : bundles)
        {
            PluginBundle pluginBundle = newPluginBundle(bundle);
            bundleCallback.onBundle(pluginBundle);
        }
    }


    /**
     * A callback inspection of each bundle and its spring context in the OSGi system
     *
     * @param bundleId the bundle id to inspect
     * @param beanCallback the callback for each spring bean
     */
    public void inspectBundle(long bundleId, BeanCallback beanCallback)
    {
        Bundle bundle = bundleContext.getBundle(bundleId);
        if (bundle == null)
        {
            return;
        }
        PluginBundle pluginBundle = newPluginBundle(bundle);
        beanCallback.onStart(pluginBundle);

        final List<ServiceReference> srs = getSpringContext(bundleId);
        for (ServiceReference sr : srs)
        {
            inspectSpringContext(beanCallback, bundleContext, sr);
        }
    }


    public interface PluginCallback
    {
        void onModuleDescriptor(ModuleDescriptor<?> moduleDescriptor, boolean enabled);
    }

    /**
     * A callback inspection of each bundle and its spring context in the OSGi system
     *
     * @param bundleId the bundle id to inspect
     * @param callback the callback for each plugin module
     */
    public Optional<Plugin> inspectBundlePluginModules(long bundleId, PluginCallback callback)
    {
        Bundle bundle = bundleContext.getBundle(bundleId);
        if (bundle == null)
        {
            return Optional.empty();
        }
        PluginBundle pluginBundle = newPluginBundle(bundle);
        Plugin plugin = pluginAccessor.getPlugin(pluginBundle.getPluginKey());
        if (plugin == null)
        {
            return Optional.empty();
        }
        for (ModuleDescriptor<?> moduleDescriptor : plugin.getModuleDescriptors())
        {
            callback.onModuleDescriptor(moduleDescriptor, pluginAccessor.isPluginModuleEnabled(moduleDescriptor.getCompleteKey()));
        }
        return Optional.of(plugin);
    }

    public boolean isSystemPlugin(Plugin plugin)
    {
        return pluginAccessor.isSystemPlugin(plugin.getKey());
    }

    public boolean isEnabled(Plugin plugin)
    {
        return pluginAccessor.isPluginEnabled(plugin.getKey());
    }

    public static class ServiceRef implements Comparable<ServiceRef>
    {
        public final String serviceId;
        public final Bundle offeringBundle;
        public final Map<String, String> serviceProperties;
        public final List<Bundle> usingBundles;
        public final List<String> objectClasses;

        ServiceRef(final String serviceId, final Bundle offeringBundle, final Map<String, String> serviceProperties,
                   final List<Bundle> usingBundles, final List<String> objectClasses)
        {
            this.serviceId = serviceId;
            this.offeringBundle = offeringBundle;
            this.objectClasses = objectClasses;
            this.serviceProperties = new HashMap<>(serviceProperties);
            this.usingBundles = Collections.unmodifiableList(new ArrayList<>(usingBundles));
        }

        @Override
        public int compareTo(@Nonnull ServiceRef o) {
            return Integer.compare(toInt(this.serviceId),toInt(o.serviceId));
        }

        private int toInt(String id) {
            try {
                // service ids can be not present
                return Integer.parseInt(id);
            } catch (NumberFormatException e) {
                return Integer.MIN_VALUE;
            }
        }
    }

    public List<ServiceRef> inspectRegisteredServices(Bundle bundle)
    {
        List<ServiceReference> registeredSR = asList(bundle.getRegisteredServices());
        return getServiceRefs(registeredSR);
    }

    public List<ServiceRef> inspectServicesInUse(Bundle bundle)
    {
        List<ServiceReference> registeredSR = asList(bundle.getServicesInUse());
        return getServiceRefs(registeredSR);
    }

    private List<ServiceRef> getServiceRefs(final List<ServiceReference> registeredSRs)
    {
        final List<ServiceRef> serviceRefs = new ArrayList<>();
        for (ServiceReference serviceReference : registeredSRs)
        {
            List<Bundle> usingBundles = asList(serviceReference.getUsingBundles());
            Map<String, String> properties = new TreeMap<>(BY_STRING_VALUE_OF);
            for (String name : serviceReference.getPropertyKeys())
            {
                Object value = serviceReference.getProperty(name);
                properties.put(name, prettyServiceValue(value));
            }
            String serviceId = Optional.ofNullable(serviceReference.getProperty("service.id")).map(TO_STRING_OR_NULL).orElse("???");

            serviceRefs.add(new ServiceRef(serviceId, serviceReference.getBundle(), properties, usingBundles, makeObjectClasses(serviceReference)));
        }
        Collections.sort(serviceRefs);
        return serviceRefs;
    }

    private List<String> makeObjectClasses(final ServiceReference serviceReference)
    {
        Object objectClass = serviceReference.getProperty("objectClass");
        List<String> objectClasses = new ArrayList<>();
        if (objectClass != null)
        {
            if (objectClass instanceof String[])
            {
                Collections.addAll(objectClasses, (String[]) objectClass);
            }
            else
            {
                objectClasses.add(String.valueOf(objectClass));
            }
        }
        return objectClasses;
    }

    private String prettyServiceValue(final Object value)
    {
        if (value instanceof String[])
        {
            return String.join(",", (String[]) value);
        }
        return String.valueOf(value);
    }

    private void inspectSpringContext(final BeanCallback beanCallback, final BundleContext bundleContext, final ServiceReference sr)
    {
        final Optional<ApplicationContext> springContext = Optional.ofNullable((ApplicationContext) bundleContext.getService(sr));
        if (springContext.isPresent())
        {
            ApplicationContext applicationContext = springContext.get();
            beanCallback.onSpringContext(applicationContext);
            List<String> beanDefinitionNames = asList(applicationContext.getBeanDefinitionNames());
            for (final String beanName : beanDefinitionNames)
            {
                BeanInspectionDefinition bDef;
                try
                {
                    Object bean = applicationContext.getBean(beanName);
                    bDef = new BeanInspectionDefinitionImpl(beanName, applicationContext.isSingleton(beanName),
                            applicationContext.isPrototype(beanName), bean);
                }
                catch (RuntimeException e)
                {
                    bDef = new BeanInspectionDefinitionImpl(beanName, e);
                }

                beanCallback.onSpringBean(bDef);
            }
        }
    }

    private List<ServiceReference> getSpringContext(final long bundleId)
    {
        try
        {
            List<ServiceReference> fromList = asList(bundleContext.getAllServiceReferences(ApplicationContext.class.getName(), null));
            return fromList.stream().filter(input -> input.getBundle().getBundleId() == bundleId).collect(Collectors.toList());
        }
        catch (InvalidSyntaxException e)
        {
            return Collections.emptyList();
        }
    }

    private static class BeanInspectionDefinitionImpl implements BeanInspectionDefinition
    {
        private final RuntimeException beanException;
        private final String beanName;
        private final Object bean;
        private final boolean singleton;
        private final boolean prototype;

        BeanInspectionDefinitionImpl(final String beanName, final boolean singleton,
                                     final boolean prototype, final Object bean)
        {
            this.beanException = null;
            this.beanName = beanName;
            this.bean = bean;
            this.singleton = singleton;
            this.prototype = prototype;
        }

        BeanInspectionDefinitionImpl(final String beanName, final RuntimeException beanException)
        {
            this.beanException = beanException;
            this.beanName = beanName;

            bean = null;
            prototype = singleton = false;
        }

        @Override
        public String beanName()
        {
            return beanName;
        }

        @Override
        public boolean singleton()
        {
            return singleton;
        }

        @Override
        public boolean prototype()
        {
            return prototype;
        }

        @Override
        public Optional<RuntimeException> beanException()
        {
            return Optional.ofNullable(beanException);
        }

        @Override
        public Optional<Object> bean()
        {
            return Optional.ofNullable(bean);
        }
    }

    public PluginBundle newPluginBundle(Bundle bundle)
    {
        String pluginKey = getPluginKey(bundle);
        return new PluginBundle(pluginKey, bundle, isAtlassianPlugin(pluginKey));
    }

    public Optional<PluginBundle> newPluginBundle(final String pluginKey)
    {
        Plugin plugin = pluginAccessor.getPlugin(pluginKey);
        if (plugin == null)
        {
            return Optional.empty();
        }
        Optional<Bundle> bundle = findBundle(plugin);
        return bundle.map(this::newPluginBundle);
    }

    private Optional<Bundle> findBundle(final Plugin plugin)
    {
        String pluginKey = plugin.getKey();
        List<Bundle> bundles = asList(bundleContext.getBundles());
        for (Bundle bundle : bundles)
        {
            if (pluginKey.equals(getPluginKey(bundle)))
            {
                return Optional.of(bundle);
            }
        }
        return Optional.empty();
    }


    private boolean isAtlassianPlugin(final String pluginKey)
    {
        return pluginAccessor.getPlugin(pluginKey) != null;
    }

    public String guessBeanClass(Object bean)
    {
        String className = bean.getClass().getName();
        if (className.startsWith("com.sun.proxy"))
        {
            try
            {
                //
                // clearly we are reaching into the bowels of the implementation here and guessing at the structure
                // if you know a better way to turn a SpringDM proxy into the interface / actual implementation
                // then by all means change this.
                //
                Object h = Reflect.on(bean).get("h");
                Object advised = Reflect.on(h).get("advised");
                List<Class> interfaces = Reflect.on(advised).get("interfaces");
                if (interfaces != null && interfaces.size() > 0 && interfaces.get(0) != null)
                {
                    return interfaces.get(0).getName();
                }
            }
            catch (RuntimeException ignored)
            {
            }
        }
        return className;
    }

    public static String getPluginKey(final Bundle bundle)
    {
        return Optional.ofNullable(OsgiHeaderUtil.getPluginKey(bundle))
                .orElse("Unknown?? : " + bundle.getBundleId());
    }

}
