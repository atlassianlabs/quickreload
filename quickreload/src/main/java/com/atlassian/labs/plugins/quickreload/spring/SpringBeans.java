package com.atlassian.labs.plugins.quickreload.spring;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.DirectoryWatcher;
import com.atlassian.labs.plugins.quickreload.Launcher;
import com.atlassian.labs.plugins.quickreload.QrConfiguration;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.WittyQuoter;
import com.atlassian.labs.plugins.quickreload.cache.DefaultWebResourceUrlFlusher;
import com.atlassian.labs.plugins.quickreload.cache.JiraWebResourceUrlFlusher;
import com.atlassian.labs.plugins.quickreload.cache.WebResourceUrlFlusher;
import com.atlassian.labs.plugins.quickreload.config.AutoRefresher;
import com.atlassian.labs.plugins.quickreload.config.ConfigReader;
import com.atlassian.labs.plugins.quickreload.inspectors.BundleInspector;
import com.atlassian.labs.plugins.quickreload.inspectors.EventInspector;
import com.atlassian.labs.plugins.quickreload.install.PluginInstaller;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.servlet.ControlPanelServletCommon;
import com.atlassian.labs.plugins.quickreload.utils.VersionKit;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.JiraOnly;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.osgi.framework.BundleContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.List;

@Import({OsgiImports.class, OsgiExports.class})
@Configuration
public class SpringBeans {
    @Bean
    public AutoRefresher autoRefresher(
            final ApplicationProperties applicationProperties,
            final ConfigReader configReader,
            final DirectoryWatcher directoryWatcher,
            final DirectoryTracker directoryTracker,
            final StateManager stateManager,
            final WittyQuoter wittyQuoter
    ) {
        return new AutoRefresher(applicationProperties, directoryWatcher, configReader, directoryTracker, wittyQuoter, stateManager);
    }

    @Bean
    public BundleInspector bundleInspector(final BundleContext bundleContext, final PluginAccessor pluginAccessor) {
        return new BundleInspector(bundleContext, pluginAccessor);
    }

    @Bean
    public ConfigReader configReader(
            final DirectoryTracker directoryTracker,
            final QrConfiguration qrConfiguration,
            final StateManager stateManager
    ) {
        return new ConfigReader(directoryTracker, qrConfiguration, stateManager);
    }

    @Bean
    public ControlPanelServletCommon controlPanelServletCommon(
            final SoyTemplateRenderer soyTemplateRenderer,
            final DirectoryTracker directoryTracker,
            final PageBuilderService pageBuilderService,
            final StateManager stateManager,
            final WittyQuoter wittyQuoter) {
        return new ControlPanelServletCommon(
                soyTemplateRenderer,
                directoryTracker,
                pageBuilderService,
                stateManager,
                wittyQuoter);
    }

    @Bean
    public DirectoryTracker directoryTracker() {
        return new DirectoryTracker();
    }

    @Bean
    public DirectoryWatcher directoryWatcher(
            final DirectoryTracker directoryTracker,
            final PluginInstaller pluginInstaller
    ) {
        return new DirectoryWatcher(directoryTracker, pluginInstaller);
    }

    @Bean
    public EventInspector eventInspector(final EventPublisher eventPublisher) {
        return new EventInspector(eventPublisher);
    }

    @Bean
    public Launcher launcher(
            final AutoRefresher autoRefresher,
            final ConfigReader configReader,
            final DirectoryTracker directoryTracker,
            final DirectoryWatcher directoryWatcher,
            final EventInspector eventInspector,
            final PluginInstaller pluginInstaller,
            final VersionKit versionKit,
            final WittyQuoter wittyQuoter
    ) {
        return new Launcher(
                directoryWatcher,
                configReader,
                pluginInstaller,
                directoryTracker,
                autoRefresher,
                versionKit,
                wittyQuoter,
                eventInspector
        );
    }

    @Bean
    public Links links(ApplicationProperties applicationProperties) {
        return new Links(applicationProperties);
    }

    @Bean
    public PluginInstaller pluginInstaller(
            final PluginAccessor pluginAccessor,
            final PluginController pluginController,
            final StateManager stateManager,
            final WittyQuoter wittyQuoter
    ) {
        return new PluginInstaller(pluginController, pluginAccessor, wittyQuoter, stateManager);
    }

    @Bean
    public QrConfiguration qrConfiguration() {
        return new QrConfiguration();
    }

    @Bean
    public StateManager stateManager(
            final ApplicationProperties applicationProperties,
            final List<WebResourceUrlFlusher> webResourceUrlFlushers) {
        return new StateManager(applicationProperties, webResourceUrlFlushers);
    }

    @Bean
    public VersionKit versionKit(final BundleContext bundleContext) {
        return new VersionKit(bundleContext);
    }

    @Bean
    @Conditional(JiraOnly.class)
    public WebResourceUrlFlusher jiraWebResourceUrlFlusher() {
        return new JiraWebResourceUrlFlusher();
    }

    @Bean
    public WebResourceUrlFlusher defaultWebResourceUrlFlusher(final EventPublisher eventPublisher) {
        return new DefaultWebResourceUrlFlusher(eventPublisher);
    }

    @Bean
    public WittyQuoter wittyQuoter() {
        return new WittyQuoter();
    }
}
