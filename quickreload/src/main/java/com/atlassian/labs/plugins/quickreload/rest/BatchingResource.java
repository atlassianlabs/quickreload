package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/batching") @jakarta.ws.rs.Path("/batching")
public class BatchingResource {

    private final Links links;
    private final StateManager stateManager;

    @Inject @jakarta.inject.Inject
    public BatchingResource(
            final Links links,
            final StateManager stateManager) {
        this.links = requireNonNull(links, "links");
        this.stateManager = requireNonNull(stateManager, "stateManager");
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public Batching getBatchingState() {
        return ok();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("setState") @jakarta.ws.rs.Path("setState")
    public Batching setBatchingState(@QueryParam("enabled") @jakarta.ws.rs.QueryParam("enabled") String enableFlag) {
        // mutation of GET is allowed under the REST do-ocracy
        if (StringUtils.isNotEmpty(enableFlag)) {
            boolean enabled = Boolean.valueOf(enableFlag);
            if (enabled) {
                stateManager.enableBatching();
            } else {
                stateManager.disableBatching();
            }
        }
        return ok();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @PUT @jakarta.ws.rs.PUT
    public Batching batchingOn() {
        stateManager.enableBatching();

        return ok();
    }

    private Batching ok() {
        return new Batching(links.apiLinks(), stateManager.isBatchingEnabled());
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @DELETE @jakarta.ws.rs.DELETE
    public Batching batchingOff() {
        stateManager.disableBatching();

        return new Batching(links.apiLinks(), stateManager.isBatchingEnabled());
    }

    static class Batching {
        public final boolean batchingEnabled;
        public final ApiLinks api;

        public Batching(final ApiLinks api, final boolean batchingEnabled) {
            this.api = api;
            this.batchingEnabled = batchingEnabled;
        }
    }
}
