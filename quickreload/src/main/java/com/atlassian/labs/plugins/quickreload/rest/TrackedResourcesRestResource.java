package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.rest.TrackedRestResource.Error;
import com.atlassian.labs.plugins.quickreload.rest.TrackedRestResource.Tracked;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.File;
import java.util.List;

import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.notFound;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 */
@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/resources") @jakarta.ws.rs.Path("/resources")
public class TrackedResourcesRestResource {

    private final DirectoryTracker directoryTracker;
    private final Links links;

    @Inject @jakarta.inject.Inject
    public TrackedResourcesRestResource(final DirectoryTracker directoryTracker, final Links links) {
        this.directoryTracker = directoryTracker;
        this.links = links;
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public Tracked getTrackedResources() {
        return trackedResources();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/{directory : .+?}") @jakarta.ws.rs.Path("/{directory : .+?}")
    public Tracked trackTheseResourcesGet(@PathParam("directory") @jakarta.ws.rs.PathParam("directory") String directory) {
        // mutation via GET is allowed under the REST do-ocracy
        return trackTheseResources(directory);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @PUT @jakarta.ws.rs.PUT
    @Path("/{directory : .+?}") @jakarta.ws.rs.Path("/{directory : .+?}")
    public Tracked trackTheseResources(@PathParam("directory") @jakarta.ws.rs.PathParam("directory") String input) {
        if (isEmpty(input)) {
            throw notFound(new Error(links.apiLinks(), "No directory provided"));
        }

        File directory = new File(input);

        if (directoryTracker.getTrackedResources().contains(directory)) {
            return getTrackedResources();
        }

        if (directory.exists()) {
            directoryTracker.addResourceDir(directory);
            return getTrackedResources();
        }

        throw notFound(new Error(links.apiLinks(), format("Could not find the directory %s", directory.getAbsolutePath())));
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @DELETE @jakarta.ws.rs.DELETE
    @Path("/{directory : .+?}") @jakarta.ws.rs.Path("/{directory : .+?}")
    public Tracked untrackTheseResources(@PathParam("directory") @jakarta.ws.rs.PathParam("directory") String input) {
        if (isEmpty(input)) {
            throw notFound(new Error(links.apiLinks(), "No directory provided"));
        }

        File directory = new File(input);
        if (directoryTracker.getTrackedResources().contains(directory)) {
            directoryTracker.removeResourceDir(directory);
            return getTrackedResources();
        }

        throw notFound(new Error(links.apiLinks(), "No existing tracked resource directory found"));
    }

    private Tracked trackedResources() {
        List<String> tracked = directoryTracker.getTrackedResources().stream()
                .map(File::getAbsolutePath)
                .sorted()
                .collect(toList());
        return new Tracked(links.apiLinks(), tracked);
    }
}
