package com.atlassian.labs.plugins.quickreload.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ArrayKit
{
    private ArrayKit() {}

    public static <T> List<T> asList(final T[] arr)
    {
        if (arr == null)
        {
            return Collections.emptyList();
        }
        return new ArrayList<>(Arrays.asList(arr)); // Return a mutable result
    }

}
