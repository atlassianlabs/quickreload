package com.atlassian.labs.plugins.quickreload;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Some helpers around System Properties
 */
public class SystemProperties {
    public static Map<String, String> getSystemProperties() {
        Map<String, String> propsAsStrings = new LinkedHashMap<>();
        List<String> names = new ArrayList<>(System.getProperties().stringPropertyNames());
        Collections.sort(names);
        names.stream()
                .forEach(
                        key -> propsAsStrings.put(key, System.getProperty(key))
                );
        return propsAsStrings;
    }

    public static void setSystemProperty(String key, String value) {
        System.setProperty(key, value);
    }

    public static void clearSystemProperty(String key) {
        Properties properties = System.getProperties();
        properties.remove(key);
    }

    public static Collection<String> getSystemPropertySplit(String systemProperty, String splitRegex) {
        String propertyValue = System.getProperty(systemProperty);
        List<String> set = new ArrayList<>();
        if (propertyValue != null) {
            Collections.addAll(set, propertyValue.split(splitRegex));
        }
        return set;
    }

    public static String setSystemProperty(String systemProperty, Collection<String> values, char delimeter) {
        String newPropertyValue = StringUtils.join(values, delimeter);
        System.setProperty(systemProperty, newPropertyValue);
        return newPropertyValue;
    }
}
