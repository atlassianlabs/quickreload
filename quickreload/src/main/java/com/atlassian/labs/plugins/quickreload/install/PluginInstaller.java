package com.atlassian.labs.plugins.quickreload.install;

import com.atlassian.labs.plugins.quickreload.LifecycledComponent;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.WittyQuoter;
import com.atlassian.labs.plugins.quickreload.utils.Files;
import com.atlassian.labs.plugins.quickreload.utils.QuickReloadThreads;
import com.atlassian.labs.plugins.quickreload.utils.Timer;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import org.slf4j.Logger;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static java.lang.Math.max;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * This can install plugins via Atlassian plugin code
 */
public class PluginInstaller implements LifecycledComponent {
    private static final Logger log = setInfo(getLogger(PluginInstaller.class));

    public static final long HALF_A_SEC = MILLISECONDS.toMillis(500);
    public static final long A_TAD = MILLISECONDS.toMillis(50);
    public static final long MAX_STABILITY_WAIT_TIME = SECONDS.toMillis(20);
    public static final long ABSOLUTE_MAX_STABILITY_WAIT_TIME = SECONDS.toMillis(60);
    public static final long MAX_INSTALL_ATTEMPTS = 5;

    private final StateManager stateManager;
    private final ExecutorService pluginInstallExecutor;
    private final ExecutorService stableFileExecutor;
    private final PluginInstallerMechanic pluginInstallerMechanic;
    private final ConcurrentLinkedQueue<PluginInstallPromise> installQueue;
    private final ConcurrentLinkedQueue<StableJarPromise> jarQueue;


    public PluginInstaller(final PluginController pluginController, PluginAccessor pluginAccessor, WittyQuoter wittyQuoter, StateManager stateManager) {
        this.stateManager = stateManager;
        this.pluginInstallerMechanic = new PluginInstallerMechanic(pluginController, wittyQuoter, pluginAccessor);

        this.pluginInstallExecutor = QuickReloadThreads.singleThreadExecutorWithName("Plugin Installer");
        this.stableFileExecutor = QuickReloadThreads.singleThreadExecutorWithName("Stable File Watcher");

        this.installQueue = new ConcurrentLinkedQueue<>();
        this.jarQueue = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void onStartup() {
        pluginInstallExecutor.submit(this::loopWaitingForInstallPromises);
        stableFileExecutor.submit(this::loopWaitingForStableFiles);
    }

    @Override
    public void onShutdown() {
        pluginInstallerMechanic.onShutdown();
        pluginInstallExecutor.shutdown();
        stableFileExecutor.shutdown();
        kickJarQueue();
        kickInstallQueue();
    }

    /**
     * Enables a plugin by key
     *
     * @param pluginKey the plugin to enable
     */
    public void enablePlugin(final String pluginKey) {
        pluginInstallerMechanic.enablePlugin(pluginKey);
    }

    /**
     * Disables a plugin by key
     *
     * @param pluginKey the plugin to disable
     */
    public void disablePlugin(final String pluginKey) {
        pluginInstallerMechanic.disablePlugin(pluginKey);
    }

    /**
     * Returns whether a plugin is enabled or not
     *
     * @param pluginKey the plugin to check
     *
     * @return either an exception or a plugin state
     */
    public boolean pluginEnabled(final String pluginKey) {
        return pluginInstallerMechanic.pluginEnabled(pluginKey);
    }

    /**
     * Returns true if the plugin appears to be a possible plugin
     *
     * @param candidate the file to check
     *
     * @return true if it smells like a plugin
     */
    public boolean smellsLikePlugin(final File candidate)
    {
        return candidate.exists() && candidate.getName().endsWith(".jar") && !Files.isSourceJar(candidate);
    }

    public void promiseToInstall(File candidatePlugin) {
        if (smellsLikePlugin(candidatePlugin)) {
            StableJarPromise stableJarPromise = new StableJarPromise(candidatePlugin, new Timer());
            if (!jarQueue.contains(stableJarPromise)) {
                log.info("Changes noticed in '{}'...", candidatePlugin.getAbsolutePath());
                jarQueue.offer(stableJarPromise);
            }
        }
    }

    private void loopWaitingForStableFiles() {
        while (!stableFileExecutor.isShutdown()) {
            LinkedList<StableJarPromise> stillUnstableJars = new LinkedList<>();
            while (true) {
                StableJarPromise jarFilePromise = jarQueue.poll();
                if (jarFilePromise == null) {
                    break;
                }
                //
                // ok squizz at the file for a while until it becomes stable. If it does
                // then have a crack at installing it later in another thread
                //
                File jarFile = jarFilePromise.getPluginFile();
                if (waitUntilFileIsStableFor(HALF_A_SEC, MAX_STABILITY_WAIT_TIME, jarFile)) {
                    // ok it become stable.  Is it ACTUALLY an Atlassian plugin?  We don't look until its stable because we cant
                    // really know earlier.
                    if (Files.isAtlassianPlugin(jarFile)) {
                        PluginInstallPromise pluginInstallPromise = PluginInstallPromise.promise(jarFile);
                        //
                        // no point offering it up if its already due to be installed
                        if (!installQueue.contains(pluginInstallPromise)) {
                            log.info("'{}' is indeed an Atlassian plugin and will be installed shortly...", jarFile.getName());
                            installQueue.offer(pluginInstallPromise);
                            kickInstallQueue();
                        }
                    } else {
                        log.info("'{}' is not an Atlassian plugin after all.  Ignoring...", jarFile.getName());
                    }
                } else {
                    stillUnstableJars.add(jarFilePromise);
                }
            }
            // re-queue unstable jars
            for (StableJarPromise stillUnstable : stillUnstableJars) {
                // has it take too long to become stable?
                if (stillUnstable.getTimer().hasElapsed(ABSOLUTE_MAX_STABILITY_WAIT_TIME, MILLISECONDS)) {
                    log.info("Abandoning '{}' because it did not become stable in {} seconds", stillUnstable.getPluginFile(), MILLISECONDS.toSeconds(ABSOLUTE_MAX_STABILITY_WAIT_TIME));
                    continue;
                }
                jarQueue.offer(stillUnstable);
            }
            // now wait some time for things to become stable or for new work
            if (!waitOnQueue(jarQueue, HALF_A_SEC)) {
                return;
            }
        }
    }

    /**
     * We have a window on how long between one reload to another.  If
     * the jar file changes within this window, it wont be reloaded
     */
    private static final int INSTALL_WINDOW_SECS = 10;

    private void loopWaitingForInstallPromises() {
        Map<File, Long> recentInstallations = new ConcurrentHashMap<>();

        while (!pluginInstallExecutor.isShutdown()) {
            LinkedList<PluginInstallPromise> retryLater = new LinkedList<>();
            while (true) {
                PluginInstallPromise installPromise = installQueue.poll();
                if (installPromise == null) {
                    break;
                }

                if (wasTooRecentlyInstalled(recentInstallations, installPromise)) {
                    break;
                }

                //
                // This is a serial process that can take some time.  So other plugins will wait behind this
                // but that's ok since firing many plugin installs in at once will probably end in tears.  Perhaps
                // in some future we can be more aggressive and fire many asynch install attempts in but for now
                // lets keep it to one at a time.
                //
                Optional<Exception> installAttempt = attemptInstall(installPromise);
                if (installAttempt.isPresent()) {
                    if (installPromise.getAttempts() > MAX_INSTALL_ATTEMPTS) {
                        log.info("Failed to install '{}' after {} attempts.  I am giving up!!", installPromise.getPluginFile(), MAX_INSTALL_ATTEMPTS);
                    } else {
                        log.info("Failed to install '{}' because of '{}'. I will retry in a short while.", installPromise.getPluginFile(), installAttempt.get().getMessage());

                        // ok retry later
                        PluginInstallPromise retryPromise = PluginInstallPromise.retryFailureAgain(installPromise);
                        log.debug("Will retry this promise {}", retryPromise);

                        retryLater.add(retryPromise);
                    }
                } else {
                    //
                    // record that we have recently installed this plugin
                    recentInstallations.put(installPromise.getPluginFile(), System.currentTimeMillis());
                }
            }
            for (PluginInstallPromise missedPlugin : retryLater) {
                installQueue.offer(missedPlugin);
            }

            if (!waitOnQueue(installQueue, HALF_A_SEC)) {
                return;
            }
        }
    }

    private boolean wasTooRecentlyInstalled(Map<File, Long> recentInstallations, PluginInstallPromise installPromise) {
        long now = System.currentTimeMillis();
        long windowMS = TimeUnit.MILLISECONDS.convert(INSTALL_WINDOW_SECS, TimeUnit.SECONDS);
        long previousInstallTimeMS = Optional.ofNullable(recentInstallations.get(installPromise.getPluginFile())).orElse(0L);
        long previousInstallTimeSecs = TimeUnit.SECONDS.convert(now - previousInstallTimeMS, TimeUnit.MILLISECONDS);

        boolean jarTooRecent = false;
        if ((now - windowMS) < previousInstallTimeMS) {
            jarTooRecent = true;
            log.warn("Since {} was installed only {} seconds ago, it wont be installed again", installPromise.getPluginFile(), previousInstallTimeSecs);
            log.warn("Are you sure your build process is not creating the plugin jar in multiple stages?");
            log.warn("Because QR seems to be noticing multiple 'updates' to the file in quick succession.");
        }

        //
        // clean up old entries in the map
        Iterator<Map.Entry<File, Long>> iterator = recentInstallations.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<File, Long> entry = iterator.next();
            Long jarInstallTimeMS = Optional.ofNullable(recentInstallations.get(entry.getKey())).orElse(0L);
            if ((now - windowMS) >= jarInstallTimeMS) {
                iterator.remove();
            }
        }
        return jarTooRecent;
    }


    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    private boolean waitOnQueue(final Object queue, final long nextWaitTime) {
        try {
            log.debug("Waiting on queue for {}", nextWaitTime);
            synchronized (queue) {
                // if we wait for zero then it waits forever.  We don't want that!
                queue.wait(max(nextWaitTime, 1));
            }
        } catch (InterruptedException e) {
            log.debug("Thread interruption exception during wait");
            return false;
        }
        return true;
    }

    private void kickInstallQueue() {
        synchronized (installQueue) {
            installQueue.notifyAll();
        }
    }

    private void kickJarQueue() {
        synchronized (jarQueue) {
            jarQueue.notifyAll();
        }
    }

    private Optional<Exception> attemptInstall(final PluginInstallPromise promise) {
        File pluginFile = promise.getPluginFile();
        if (!pluginFile.exists()) {
            log.info("'{}' has disappeared just as I was trying to install it.", pluginFile);
        }
        return installPluginImmediately(pluginFile);
    }

    public Optional<Exception> installPluginImmediately(File pluginFile) {
        if (!stateManager.isQuickReloadEnabled()) {
            log.info("QuickReload is currently disabled....'{}' will not be installed", pluginFile);
            return Optional.empty();
        }
        return pluginInstallerMechanic.installPluginImmediately(pluginFile);
    }

    public void uninstallPlugin(String pluginKey) {
        if (!stateManager.isQuickReloadEnabled()) {
            log.info("QuickReload is currently disabled....'{}' will not be un-installed", pluginKey);
            return;
        }

        pluginInstallerMechanic.uninstallPlugin(pluginKey);
    }

    /**
     * A jar file is built progressively.  So we see a change event when its at near zero bytes but zip entries are then
     * streamed into it by maven say.  We have to wait until it stays stable (that is the same length for a time) before
     * we decide to inspect it as an Atlassian plugin.
     *
     * @param stableForMS how long to wait for it to be considered stable
     * @param maxWaitMS   the maximum time we will wait for it to ever become stable
     * @param candidate   the file to become stable
     *
     * @return true if its stable inside maxWaitMS
     */
    private boolean waitUntilFileIsStableFor(long stableForMS, long maxWaitMS, final File candidate) {
        Timer startTimer = new Timer();
        Timer msgTimer = new Timer();
        Timer stableTimer = new Timer();
        long prevLen = candidate.length();

        String fileName = candidate.getName();

        log.info("Waiting for '{}' to become stable ({} bytes)...", fileName, prevLen);
        while (startTimer.isInside(maxWaitMS) && !pluginInstallExecutor.isShutdown()) {
            sleepFor(A_TAD); // just a tad of CPU / IO efficiency
            long newLen = candidate.length();
            if (newLen == prevLen) {
                if (stableTimer.hasElapsed(stableForMS)) {
                    log.info("'{}' now appears to be stable ({} bytes)", fileName, prevLen);
                    return true;
                }
            } else {
                // nah the length is still changing.  reset the timer
                prevLen = newLen;
                stableTimer.reset();
            }
            //
            // show a message every now and again about what we are doing
            if (msgTimer.hasElapsed(3, SECONDS)) {
                log.info("Waiting for the plugin file to become stable ({} bytes)...", prevLen);
                msgTimer.reset();
            }
        }
        log.info("Plugin file has not become stable in {} seconds", MILLISECONDS.toSeconds(maxWaitMS));
        return false;
    }

    private void sleepFor(final long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
