package com.atlassian.labs.plugins.quickreload.utils;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;

import java.util.concurrent.atomic.AtomicReference;

/**
 */
public class VersionKit
{
    private final static AtomicReference<BundleContext> bundleContext = new AtomicReference<BundleContext>();

    public VersionKit(final BundleContext bundleCtx)
    {
        bundleContext.set(bundleCtx);
    }

    public static Version getVersion()
    {
        return bundleContext().getBundle().getVersion();
    }

    public static String getDisplayedVersion()
    {
        return getVersion().toString();
    }

    private static BundleContext bundleContext()
    {
        BundleContext context = bundleContext.get();
        if (context == null)
        {
            throw new IllegalStateException("VersionKit accessed before wiring");
        }
        return context;
    }
}
