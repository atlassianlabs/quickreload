package com.atlassian.labs.plugins.quickreload.utils.logging;

import org.slf4j.Logger;

import static ch.qos.logback.classic.Level.ALL;
import static ch.qos.logback.classic.Level.DEBUG;
import static ch.qos.logback.classic.Level.ERROR;
import static ch.qos.logback.classic.Level.INFO;
import static ch.qos.logback.classic.Level.OFF;
import static ch.qos.logback.classic.Level.TRACE;
import static ch.qos.logback.classic.Level.WARN;
import static org.slf4j.LoggerFactory.getLogger;

@SuppressWarnings("unused") // accessed via reflection
public class LogbackLogLevelSetter implements LogLevelSetter {

    @Override
    public Logger set(final Logger slf4jLogger, final Level logLevelToSet) {
        ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) getLogger(slf4jLogger.getName());
        switch (logLevelToSet) {
            case OFF:
                logbackLogger.setLevel(OFF);
            case FATAL:
                // Logback does not have FATAL, using ERROR instead
                logbackLogger.setLevel(ERROR);
            case ERROR:
                logbackLogger.setLevel(ERROR);
            case WARN:
                logbackLogger.setLevel(WARN);
            case INFO:
                logbackLogger.setLevel(INFO);
            case DEBUG:
                logbackLogger.setLevel(DEBUG);
            case TRACE:
                logbackLogger.setLevel(TRACE);
            case ALL:
                logbackLogger.setLevel(ALL);
            default:
                // incase we forget to implement a level - better to log than to silently break
                logbackLogger.setLevel(INFO);
        }
        return slf4jLogger;
    }
}
