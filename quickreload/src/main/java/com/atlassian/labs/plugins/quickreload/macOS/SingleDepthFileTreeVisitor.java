package com.atlassian.labs.plugins.quickreload.macOS;

import io.methvin.watcher.visitor.FileTreeVisitor;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

/**
 * This is a copy of {@link io.methvin.watcher.visitor.DefaultFileTreeVisitor} but only looks at files at depth 1 for a
 * directory instead of all levels of the file tree. This is the same behaviour as the file watching functionality on
 * windows/linux.
 *
 * @since 4.0.0
 */
public class SingleDepthFileTreeVisitor implements FileTreeVisitor {

    @Override
    public void recursiveVisitFiles(Path file, Callback onDirectory, Callback onFile) throws IOException {
        SimpleFileVisitor<Path> visitor =
                new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        if (exc != null) {onFailure(dir, exc);}
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                            throws IOException {
                        onDirectory.call(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                            throws IOException {
                        onFile.call(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                        onFailure(file, exc);
                        return FileVisitResult.CONTINUE;
                    }
                };

        // Only look in current depth
        Files.walkFileTree(file, EnumSet.noneOf(FileVisitOption.class), 1, visitor);
    }

    // To be overridden if needed
    protected void onFailure(Path path, IOException exception) throws IOException {}
}
