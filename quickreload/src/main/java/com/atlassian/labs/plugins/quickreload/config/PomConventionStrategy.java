package com.atlassian.labs.plugins.quickreload.config;

import com.atlassian.labs.plugins.quickreload.utils.Files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This config strategy is to go from the specified directory and traverse up intil we run out of pom.xml files
 * <p>
 * Then it traverses down and looks for all pom.xml files and watches their ./target directory for changes.
 * </p>
 * <p>
 * This is a convention based approach to alleviate the need for quickreload.properties files.
 * </p>
 * Then assumption is that all maven ./target directories from the highest pom should be watched
 *
 * @since v0.x
 */
public class PomConventionStrategy
{
    private final File startDir;

    public PomConventionStrategy(final File startDir)
    {
        this.startDir = startDir;
    }

    public List<File> getTargetDirs()
    {
        Optional<File> lastPomFile = Files.traverseUpForFileThenYouFindItAndTheCant("pom.xml", startDir);
        if (lastPomFile.isPresent())
        {
            final List<File> trackedDirs = new ArrayList<>();
            File startDir = lastPomFile.get().getParentFile();
            Files.traverseDownLookingForFile("pom.xml", startDir,
                path ->
                    !path.endsWith("target") && !path.endsWith("node_modules") &&
                    !path.toFile().isHidden(),
                file ->
                    addTargetToList(trackedDirs, file)
            );
            return trackedDirs;
        }

        return Collections.emptyList();
    }

    private void addTargetToList(final List<File> trackedDirs, final File file)
    {
        File target = Files.smooshNames(file.getParent(), "target");
        try
        {
            trackedDirs.add(target.getCanonicalFile());
        }
        catch (IOException ignored)
        {
        }
    }
}
