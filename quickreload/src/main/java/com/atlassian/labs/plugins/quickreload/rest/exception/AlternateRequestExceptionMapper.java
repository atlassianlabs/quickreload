package com.atlassian.labs.plugins.quickreload.rest.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import static jakarta.ws.rs.core.Response.status;

@Provider
public class AlternateRequestExceptionMapper implements ExceptionMapper<AlternateStatusCodeException> {

    @Override
    public Response toResponse(AlternateStatusCodeException exception) {
        return status(exception.status)
                .type(exception.contentType)
                .entity(exception.entity)
                .build();
    }
}
