package com.atlassian.labs.plugins.quickreload.utils.logging;

import org.apache.log4j.Category;

import static org.apache.log4j.Level.ALL;
import static org.apache.log4j.Level.DEBUG;
import static org.apache.log4j.Level.ERROR;
import static org.apache.log4j.Level.FATAL;
import static org.apache.log4j.Level.INFO;
import static org.apache.log4j.Level.OFF;
import static org.apache.log4j.Level.TRACE;
import static org.apache.log4j.Level.WARN;
import static org.apache.log4j.Logger.getLogger;

@SuppressWarnings("unused") // accessed via reflection
public class Log4j1LevelSetter implements LogLevelSetter {

    @Override
    public org.slf4j.Logger set(org.slf4j.Logger slf4jLogger, Level logLevelToSet) {
        Category lo4jLogger = getLogger(slf4jLogger.getName());
        switch (logLevelToSet) {
            case OFF:
                lo4jLogger.setLevel(OFF);
                break;
            case FATAL:
                lo4jLogger.setLevel(FATAL);
                break;
            case ERROR:
                lo4jLogger.setLevel(ERROR);
                break;
            case WARN:
                lo4jLogger.setLevel(WARN);
            case INFO:
                lo4jLogger.setLevel(INFO);
                break;
            case DEBUG:
                lo4jLogger.setLevel(DEBUG);
                break;
            case TRACE:
                lo4jLogger.setLevel(TRACE);
                break;
            case ALL:
                lo4jLogger.setLevel(ALL);
                break;
            default:
                // incase we forget to implement a level - better to log than to silently break
                lo4jLogger.setLevel(INFO);
                break;
        }
        return slf4jLogger;
    }
}
