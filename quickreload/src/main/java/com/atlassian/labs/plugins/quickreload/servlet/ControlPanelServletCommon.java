package com.atlassian.labs.plugins.quickreload.servlet;

import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.WittyQuoter;
import com.atlassian.labs.plugins.quickreload.utils.VersionKit;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.labs.plugins.quickreload.BuildProperties.PLUGIN_KEY;
import static com.atlassian.labs.plugins.quickreload.DirectoryTracker.PLUGIN_RESOURCE_DIRECTORIES;
import static com.atlassian.labs.plugins.quickreload.SystemProperties.getSystemPropertySplit;
import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setWarn;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

public class ControlPanelServletCommon {

    static final String SERVLET_CONTENT_TYPE = "text/html; charset=utf-8";

    private static final Logger log = setWarn(getLogger(ControlPanelServletCommon.class));

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final DirectoryTracker directoryTracker;
    private final PageBuilderService pageBuilderService;
    private final StateManager stateManager;
    private final WittyQuoter wittyQuoter;

    public ControlPanelServletCommon(
            final SoyTemplateRenderer soyTemplateRenderer,
            final DirectoryTracker directoryTracker,
            final PageBuilderService pageBuilderService,
            final StateManager stateManager,
            final WittyQuoter wittyQuoter) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.directoryTracker = directoryTracker;
        this.pageBuilderService = pageBuilderService;
        this.stateManager = stateManager;
        this.wittyQuoter = wittyQuoter;
    }

    static String getApiLink(String scheme, String serverName, int servetPort, String contextPath) {
        return String.format("%s://%s:%s%s/rest/qr", scheme, serverName, servetPort, contextPath);
    }

    void render(PrintWriter writer, String apiLink) {
        try {
            pageBuilderService.assembler()
                    .resources()
                    .requireWebResource("com.atlassian.labs.plugins.quickreload.reloader:qr-control-panel-resources");

            soyTemplateRenderer.render(
                    writer,
                    PLUGIN_KEY + ":qr-control-panel-resources",
                    "QuickReload.Page.ControlPanel.main",
                    getRenderParams(apiLink)
            );
        } catch (SoyException e) {
            log.error("Failed to render template for QuickReload control panel", e);
        }
    }

    private Map<String, Object> getRenderParams(final String apiLink) {
        Map<String, String> quickReloadManagedProperties = stateManager.getBatchingSystemPropertyStates();

        Map<String, Object> builder = new LinkedHashMap<>();
        builder.put("trackedDirectories", getTrackedDirectories());
        builder.put("trackedPluginResourceDirectories", getPluginResourceDirectories());
        builder.put("systemPropertyStates", quickReloadManagedProperties);
        builder.put("isBatchedMode", stateManager.isBatchingEnabled());
        builder.put("apiLinkBase", apiLink);
        builder.put("qrVersion", VersionKit.getDisplayedVersion());
        builder.put("wit", wittyQuoter.halfWit());

        return Collections.unmodifiableMap(builder);
    }

    private List<String> getTrackedDirectories() {
        return directoryTracker.getTracked().stream().
                map(File::getAbsolutePath)
                .collect(toList());
    }

    private List<String> getPluginResourceDirectories() {
        return new ArrayList<>(getSystemPropertySplit(PLUGIN_RESOURCE_DIRECTORIES, ","));
    }
}
