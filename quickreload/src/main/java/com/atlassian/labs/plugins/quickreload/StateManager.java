package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.cache.WebResourceUrlFlusher;
import com.atlassian.sal.api.ApplicationProperties;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static java.util.Collections.unmodifiableMap;
import static org.slf4j.LoggerFactory.getLogger;

/**
 */
public class StateManager {

    public static final String PLUGIN_CONTEXT_BATCHING = "plugin.webresource.context.batch.enabled";
    public static final String PLUGIN_WEBRESOURCE_BATCHING_OFF = "plugin.webresource.batching.off";
    public static final String ATLASSIAN_DEV_MODE = "atlassian.dev.mode";
    public static final String JIRA_DEV_MODE = "jira.dev.mode";
    public static final String JIRA_CONTEXT_BATCHING_DISABLED = "jira.contextbatching.disabled";
    public static final String BAMBOO_CONTEXT_BATCHING_DISABLED = "bamboo.context.batching.disable";
    public static final String CONFLUENCE_DEV_MODE = "confluence.devmode";
    public static final String CONFLUENCE_CONTEXT_BATCHING_OFF = "confluence.context.batching.disable";

    private static final Logger log = setInfo(getLogger(StateManager.class));

    private final ApplicationProperties applicationProperties;
    private final List<WebResourceUrlFlusher> resourceUrlFlushers;

    private volatile boolean quickReloadEnabled = true;

    public StateManager(
            final ApplicationProperties applicationProperties,
            final List<WebResourceUrlFlusher> resourceUrlFlushers) {
        this.applicationProperties = applicationProperties;
        this.resourceUrlFlushers = resourceUrlFlushers;
    }

    public boolean isQuickReloadEnabled() {
        return quickReloadEnabled;
    }

    public void enableQuickReload()
    {
        quickReloadEnabled = true;
    }

    public void disableQuickReload()
    {
        quickReloadEnabled = false;
    }

    public boolean isBatchingEnabled()
    {
        String batchingOff = System.getProperty(PLUGIN_WEBRESOURCE_BATCHING_OFF);
        return !Boolean.valueOf(batchingOff);
    }

    public void enableBatching()
    {
        setProperties(true);
        resourceUrlFlushers.forEach(WebResourceUrlFlusher::flushWebResourceUrls);
        log.info("Fast mode - web resource batching is now ENABLED, dev mode DISABLED");
    }

    public void disableBatching()
    {
        setProperties(false);
        resourceUrlFlushers.forEach(WebResourceUrlFlusher::flushWebResourceUrls);
        log.info("Dev mode - web resource batching is now DISABLED");
    }

    /*
     * To enable batching for JIRA (JiraWebResourceBatchingConfiguration), we need to set these to false:
     * - "atlassian.dev.mode"
     * - "jira.contextbatching.disabled"
     *
     * To enable batching for Confluence (ConfluenceResourceBatchingConfiguration), we need to set these to false:
     * - "atlassian.dev.mode"
     * - "confluence.context.batching.disable"
     *
     * It looks like "jira.dev.mode" and "confluence.devmode" were federated into "atlassian.dev.mode" some time
     * ago, so they should match each other to support older dev utilities.
     */
    private List<BatchingSystemProperty> getBatchingPropertiesForApplication() {
        List<BatchingSystemProperty> batchingSystemProperties = new ArrayList<>();

        batchingSystemProperties.add(new BatchingSystemProperty(ATLASSIAN_DEV_MODE, false));
        batchingSystemProperties.add(new BatchingSystemProperty(PLUGIN_CONTEXT_BATCHING, true));
        batchingSystemProperties.add(new BatchingSystemProperty(PLUGIN_WEBRESOURCE_BATCHING_OFF, false));

        switch (applicationProperties.getPlatformId()) {
            case "jira":
                batchingSystemProperties.add(new BatchingSystemProperty(JIRA_DEV_MODE, false));
                batchingSystemProperties.add(new BatchingSystemProperty(JIRA_CONTEXT_BATCHING_DISABLED, false));
                break;
            case "confluence":
                batchingSystemProperties.add(new BatchingSystemProperty(CONFLUENCE_DEV_MODE, false));
                batchingSystemProperties.add(new BatchingSystemProperty(CONFLUENCE_CONTEXT_BATCHING_OFF, false));
                break;
            case "bamboo":
                batchingSystemProperties.add(new BatchingSystemProperty(BAMBOO_CONTEXT_BATCHING_DISABLED, false));
                break;
            default:
                break;
        }

        return batchingSystemProperties;
    }

    private void setProperties(boolean batchingEnabled)
    {
        for (BatchingSystemProperty property : getBatchingPropertiesForApplication()) {
            if (batchingEnabled) {
                System.setProperty(property.key, String.valueOf(property.valueForBatchingEnabled));
            } else {
                System.setProperty(property.key, String.valueOf(!property.valueForBatchingEnabled));
            }
        }
    }

    public Map<String, String> getBatchingSystemPropertyStates() {
        Map<String, String> states = new LinkedHashMap<>();

        for (BatchingSystemProperty batchingSystemProperty : getBatchingPropertiesForApplication()) {
            String key = batchingSystemProperty.key;
            String value = System.getProperty(key);
            if (value != null)
            {
                states.put(key, value);
            }
        }

        return unmodifiableMap(states);
    }

    private static class BatchingSystemProperty
    {
        public final String key;
        public final boolean valueForBatchingEnabled;

        public BatchingSystemProperty(String key, boolean valueForBatchingEnabled)
        {
            this.key = key;
            this.valueForBatchingEnabled = valueForBatchingEnabled;
        }
    }
}
