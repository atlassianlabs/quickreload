package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.utils.Files;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.labs.plugins.quickreload.SystemProperties.getSystemPropertySplit;
import static com.atlassian.labs.plugins.quickreload.SystemProperties.setSystemProperty;
import static com.atlassian.labs.plugins.quickreload.utils.Files.directoryExists;
import static com.atlassian.labs.plugins.quickreload.utils.Files.smooshNames;
import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static java.io.File.separator;
import static java.lang.String.join;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static java.util.Objects.isNull;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toSet;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Keeps track of the plugin directories we know about
 */
public class DirectoryTracker {

    private static final Logger log = setInfo(getLogger(DirectoryWatcher.class));

    public static final String PLUGIN_RESOURCE_DIRECTORIES = "plugin.resource.directories";
    private static final String RESOURCE_COLON = "resource:";
    private static final String SRC_MAIN_RESOURCES = join(separator, asList(".", "src", "main", "resources"));
    /**
     * Could be found in a quickreload.properties file, it's valid configuration. e.g.
     * <pre>alternateResources=./target/classes:jira-ical-feed</pre>
     */
    private static final String QR_ALT_RESOURCES_PROPERTY_KEY = "alternateResources";

    private final Set<File> trackedDirs;
    private final Set<File> notToTrackedDirs;

    public DirectoryTracker() {
        trackedDirs = new TreeSet<>();
        notToTrackedDirs = new TreeSet<>();
    }

    public Set<File> getTracked() {
        return new HashSet<>(trackedDirs);
    }

    public boolean isTracked(File directory) {
        return trackedDirs.contains(directory);
    }

    public DirectoryTracker remove(final File trackedDir) {
        return removeFromTrackedAndBlacklistThisDir(trackedDir.getAbsolutePath(), empty(), new HashMap<>());
    }


    public DirectoryTracker add(String directoryName) {
        return add(directoryName, empty(), emptyMap());
    }

    public DirectoryTracker add(File dir) {
        return add(dir.getAbsolutePath(), empty(), emptyMap());
    }

    public DirectoryTracker add(String directoryName, final Optional<File> parentDir, Map<String, String> properties) {
        File directory = smooshNames(parentDir.orElseGet(() -> new File(".")), directoryName);

        // Be nice and look up a bit.
        File candidateDirectory = directory;
        while (true) { // There can only be so many directories
            if (hasPomFile(candidateDirectory)) {
                directory = candidateDirectory;
                break;
            }
            candidateDirectory = candidateDirectory.getParentFile();
            if (isNull(candidateDirectory)) {
                return this;
            }
        }

        if (!isBlackListed(directoryName)) {
            // we have a heuristic around how we track directories.  If the directory exists and has a pom.xml
            // file in it then we assume a maven project and hence a ./target and a ./src/main/resources
            // directory as well
            File targetDir = smooshNames(directory, "./target");
            File targetClassesDir = smooshNames(targetDir, "./classes");
            File resourcesDir = getResourceDirectory(directory, properties);

            if (trackedDirs.contains(candidateDirectory)) {
                return this; // We already got it, go home
            }

            if (isValidResourceDirectory(directory, resourcesDir)) {
                trackedDirs.add(targetDir);
                addResourceDir(resourcesDir);
            } else {
                trackedDirs.add(directory);
            }
            // src/main/resources should take priority over target/classes since it's more likely to be fresh
            addResourceDir(targetClassesDir);
        }
        return this;
    }

    public DirectoryTracker removeFromTrackedAndBlacklistThisDir(String directoryName, final Optional<File> parentDir, Map<String, String> properties) {
        blackListDirectory(directoryName);
        File directory = smooshNames(parentDir.orElseGet(() -> new File(".")), directoryName);

        //
        // we have a heuristic around how we track directories.  If the directory exists and has a pom.xml
        // file in it then we assume a maven project and hence a ./target and a ./src/main/resources
        // directory as well
        //
        File targetDir = smooshNames(directory, "./target");
        File targetClassesDir = smooshNames(targetDir, "./classes");
        File resourcesDir = getResourceDirectory(directory, properties);

        if (isValidResourceDirectory(directory, resourcesDir)) {
            trackedDirs.remove(targetDir);
            removeResourceDir(resourcesDir);
        } else {
            trackedDirs.remove(directory);
        }
        removeResourceDir(targetClassesDir);
        return this;
    }

    private boolean hasPomFile(final File directory) {
        File pomFile = smooshNames(directory, "./pom.xml");
        return pomFile.exists() && !pomFile.isDirectory();
    }

    public void addResourceEntry(final String dirName, final Optional<File> parentDir) {
        String newDirName = StringUtils.remove(dirName, RESOURCE_COLON);
        if (parentDir.isPresent()) {
            addResourceDir(smooshNames(parentDir.get(), newDirName));
        } else {
            addResourceDir(new File(newDirName));
        }
    }

    public void blackListDirectory(String directory) {
        log.warn("Adding {} to NOT TO WATCH list", directory);
        notToTrackedDirs.add(new File(directory));
    }

    public boolean isBlackListed(String directory) {
        return isBlackListed(new File(directory));
    }

    public boolean isBlackListed(File file) {
        return notToTrackedDirs.contains(file);
    }

    public Set<File> getTrackedResources() {
        return getSystemResourceDirs()
                .stream()
                .map(File::new)
                .collect(toSet());
    }

    //
    // we use the system property as the storage for the list.  It will take affect
    // the next time the a plugin reloads
    //
    public void addResourceDir(final File resourcesDir) {
        Set<String> alternateResourceDirs = getSystemResourceDirs();

        alternateResourceDirs.add(resourcesDir.getAbsolutePath());

        setSystemResourceDirs(alternateResourceDirs);
    }

    public void removeResourceDir(final File resourcesDir) {
        Set<String> alternateResourceDirs = getSystemResourceDirs();

        alternateResourceDirs.remove(resourcesDir.getAbsolutePath());

        setSystemResourceDirs(alternateResourceDirs);
    }

    private Set<String> getSystemResourceDirs() {
        return new HashSet<>(getSystemPropertySplit(PLUGIN_RESOURCE_DIRECTORIES, ","));
    }

    private void setSystemResourceDirs(final Set<String> alternateResourceDirs) {
        // now write out the list back to the system property
        setSystemProperty(PLUGIN_RESOURCE_DIRECTORIES, alternateResourceDirs, ',');
    }

    private File getResourceDirectory(File parentDirectory, Map<String, String> properties) {
        if (properties.containsKey(QR_ALT_RESOURCES_PROPERTY_KEY)) {
            return Files.smooshNames(parentDirectory, properties.get(QR_ALT_RESOURCES_PROPERTY_KEY));
        } else {
            return Files.smooshNames(parentDirectory, SRC_MAIN_RESOURCES);
        }
    }

    private boolean isValidResourceDirectory(File parentDirectory, File resourceDirectory) {
        return hasPomFile(parentDirectory) && directoryExists(resourceDirectory);
    }

    /**
     * Because ConfigReader has to read all the: Tracked vs NOT TO tracked From Files and Vars In the end we need to
     * refined the dir we wants to track from both lists
     */
    public void refinedTrackedDirsList() {
        for (File file : getTracked()) {
            if (isBlackListed(file)) {
                remove(file);
            }
        }
    }
}
