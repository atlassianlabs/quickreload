package com.atlassian.labs.plugins.quickreload.utils.logging;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.simple.SimpleLogger;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.apache.logging.slf4j.Log4jLogger;
import org.apache.logging.slf4j.Log4jMarkerFactory;

import java.util.Properties;

import static org.apache.logging.log4j.Level.ALL;
import static org.apache.logging.log4j.Level.DEBUG;
import static org.apache.logging.log4j.Level.ERROR;
import static org.apache.logging.log4j.Level.FATAL;
import static org.apache.logging.log4j.Level.INFO;
import static org.apache.logging.log4j.Level.OFF;
import static org.apache.logging.log4j.Level.TRACE;
import static org.apache.logging.log4j.Level.WARN;
import static org.apache.logging.log4j.LogManager.getLogger;

@SuppressWarnings("unused") // accessed via reflection
public class Log4j2LevelSetter implements LogLevelSetter {

    @Override
    public org.slf4j.Logger set(org.slf4j.Logger slf4jLogger, Level logLevelToSet) {
        final Logger oldLog4jLogger = getLogger(slf4jLogger.getName());
        final SimpleLogger lo4jLogger = new SimpleLogger(oldLog4jLogger.getName(), INFO, true, false, true, true, "HH:mm:ss,SSS", oldLog4jLogger.getMessageFactory(), new PropertiesUtil(new Properties()), System.out);
        switch (logLevelToSet) {
            case OFF:
                lo4jLogger.setLevel(OFF);
                break;
            case FATAL:
                lo4jLogger.setLevel(FATAL);
                break;
            case ERROR:
                lo4jLogger.setLevel(ERROR);
                break;
            case WARN:
                lo4jLogger.setLevel(WARN);
            case INFO:
                lo4jLogger.setLevel(INFO);
                break;
            case DEBUG:
                lo4jLogger.setLevel(DEBUG);
                break;
            case TRACE:
                lo4jLogger.setLevel(TRACE);
                break;
            case ALL:
                lo4jLogger.setLevel(ALL);
                break;
            default:
                // incase we forget to implement a level - better to log than to silently break
                lo4jLogger.setLevel(INFO);
                break;
        }

        return new Log4jLogger(new Log4jMarkerFactory(), lo4jLogger, slf4jLogger.getName());
    }
}
