package com.atlassian.labs.plugins.quickreload.inspectors;

import org.osgi.framework.Bundle;

import static java.util.Objects.requireNonNull;

/**
 * A bundle and plugin key
 */
public class PluginBundle
{
    private final String pluginKey;
    private final Bundle bundle;
    private final boolean isAtlassianPlugin;

    public PluginBundle(final String pluginKey, final Bundle bundle, final boolean isAtlassianPlugin)
    {
        this.isAtlassianPlugin = isAtlassianPlugin;
        this.pluginKey = requireNonNull(pluginKey);
        this.bundle = requireNonNull(bundle);
    }

    public Bundle getBundle()
    {
        return bundle;
    }

    public String getPluginKey()
    {
        return pluginKey;
    }

    public boolean isAtlassianPlugin()
    {
        return isAtlassianPlugin;
    }
}
