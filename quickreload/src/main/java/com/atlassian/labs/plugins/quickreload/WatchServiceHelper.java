package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.macOS.SingleDepthFileTreeVisitor;
import io.methvin.watcher.hashing.FileHasher;
import io.methvin.watcher.visitor.FileTreeVisitor;
import io.methvin.watchservice.MacOSXListeningWatchService;
import io.methvin.watchservice.WatchablePath;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.Watchable;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

/**
 * Helper class to provide watching utils for specific OS.
 *
 * @since 4.0.0
 */
public final class WatchServiceHelper {

    public static final WatchEvent.Kind<?>[] kinds = new WatchEvent.Kind<?>[]{
            ENTRY_CREATE,
            ENTRY_DELETE,
            ENTRY_MODIFY
    };

    private WatchServiceHelper() {
        throw new UnsupportedOperationException("This is a helper class");
    }

    public static String getFullPath(Path context, Path parent) {
        String fullPath = context.toString();
        // Mac OS X implementation returns the full absolute path of the file in its change notifications
        if (isMac()) {
            return fullPath;
        } else {
            return parent.toString() + "/" + fullPath;
        }
    }

    /**
     * The Mac OSX implementation of the WatchService (PollingWatchService) only looks for file updates every few seconds
     * and is not performant. Previously, {@link DirectoryWatcher} used the jpathwatch implementation as a workaround.
     * However, this was not compatible with Mac M1 architecture.
     *
     * This method loads an M1 compatible WatchService if the underlying OS is OSX, otherwise it uses the
     * default {@link java.nio.file.WatchService watchService} for linux and windows.
     */
    public static WatchService getWatchService() {
        if (isMac()) {
            return new MacOSXListeningWatchService(new MacOSXListeningWatchService.Config() {
                // Disable the file hasher to use file level events
                @Override
                public FileHasher fileHasher() {
                    return null;
                }

                @Override
                public boolean fileLevelEvents() {
                    return true;
                }

                // Use a custom file tree visitor so we don't recursively look at every level of the file tree
                @Override
                public FileTreeVisitor fileTreeVisitor() {
                    return new SingleDepthFileTreeVisitor();
                }
            });
        } else {
            try {
                return FileSystems.getDefault().newWatchService();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static boolean isMac() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }

    public static WatchKey registerDirectoryForWatching(WatchService watchService, File file) throws IOException {
        Watchable watchable;
        if (isMac()) {
            watchable = new WatchablePath(file.toPath());
        } else {
            watchable = file.toPath();
        }
        return watchable.register(watchService, kinds);
    }
}
