package com.atlassian.labs.plugins.quickreload.spring;

import com.atlassian.labs.plugins.quickreload.Launcher;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

public class OsgiExports {

    @Bean
    public FactoryBean<ServiceRegistration> exportLauncher(Launcher launcher) {
        return exportOsgiService(launcher, as(LifecycleAware.class));
    }
}
