package com.atlassian.labs.plugins.quickreload.cache;

/**
 * This will bust the URL caches after we change web resource batching state etc....
 */
public interface WebResourceUrlFlusher {
    /**
     * Busts browser caches so batched / non-batched files are re-calculated.
     */
    void flushWebResourceUrls();
}
