package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.config.BasicConfigFileReader;
import com.atlassian.labs.plugins.quickreload.utils.Files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * The wit continues!
 */
public class WittyQuoter implements LifecycledComponent
{
    private static Random r = new Random();

    private static List<String> STATIC_WIT = Arrays.asList(
            "A watched plugin never boils!",
            "Stealing back time from Maven since 2013.",
            "Just press up arrow!",
            "If you can type on a Dvorak keyboard can you automatically speak Esperanto and program in Lisp?",
            "I have a hidden agenda... I can't find the bastard anywhere!",
            "If Maven was a horse, you would shoot it, make it into dog food, feed it to a dog, and then shoot that just to be certain!",
            "If you drop a '.halfwit' file in your project, then you too can be entertained by your own pithy comments.  Navel gazing for the masses"
    );

    private List<String> trueWit = STATIC_WIT;

    public String halfWit()
    {
        int index = r.nextInt(trueWit.size());
        return trueWit.get(index);
    }

    @Override
    public void onStartup()
    {
        final List<String> wit = new ArrayList<>();
        Files.traverseUpLookingForFile(".halfwit", Files.pwd(), file -> wit.addAll(readWit(file)));

        if (wit.size() > 0)
        {
            trueWit = wit;
        }
    }

    private List<String> readWit(final File witFile)
    {
        try
        {
            return new BasicConfigFileReader().readConfigFileEntries(witFile);
        }
        catch (IOException ignored)
        {
            return Collections.emptyList();
        }
    }

    @Override
    public void onShutdown()
    {
    }
}
