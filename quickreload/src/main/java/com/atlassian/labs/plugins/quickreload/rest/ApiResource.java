package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 */
@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/api") @jakarta.ws.rs.Path("/api")
public class ApiResource {

    private final Links links;

    @Inject @jakarta.inject.Inject
    public ApiResource(Links links) {
        this.links = requireNonNull(links, "links");
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public ApiLinks api() {
        return links.apiLinks();
    }
}
