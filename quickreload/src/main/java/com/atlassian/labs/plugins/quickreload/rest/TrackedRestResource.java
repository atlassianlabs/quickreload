package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.DirectoryWatcher;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.File;
import java.util.Collection;
import java.util.List;

import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.notFound;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 */
@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/tracked") @jakarta.ws.rs.Path("/tracked")
public class TrackedRestResource {

    private final DirectoryTracker directoryTracker;
    private final DirectoryWatcher directoryWatcher;
    private final Links links;

    @Inject @jakarta.inject.Inject
    public TrackedRestResource(final DirectoryTracker directoryTracker, final DirectoryWatcher directoryWatcher, Links links) {
        this.directoryTracker = directoryTracker;
        this.directoryWatcher = directoryWatcher;
        this.links = links;
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public Tracked getTracked() {
        return tracked();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("/{directory : .+?}") @jakarta.ws.rs.Path("/{directory : .+?}")
    public Tracked trackThisGet(@PathParam("directory") @jakarta.ws.rs.PathParam("directory") String directory) {
        // mutation via GET is allowed under the REST do-ocracy
        return trackThis(directory);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @POST @jakarta.ws.rs.POST
    @Path("/{directory : .+?}") @jakarta.ws.rs.Path("/{directory : .+?}")
    public Tracked trackThis(@PathParam("directory") @jakarta.ws.rs.PathParam("directory") String directory) {
        if (isEmpty(directory)) {
            throw notFound(new Error(links.apiLinks(), "Directory not found"));
        }

        File trackedDir = new File(directory);
        directoryTracker.add(trackedDir.getAbsolutePath());
        directoryWatcher.watch(trackedDir);

        return tracked();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @DELETE @jakarta.ws.rs.DELETE
    @Path("/{directory : .+?}") @jakarta.ws.rs.Path("/{directory : .+?}")
    public Tracked untrackThis(@PathParam("directory") @jakarta.ws.rs.PathParam("directory") String directory) {
        if (isEmpty(directory)) {
            throw notFound(new Error(links.apiLinks(), "Directory not found"));
        }

        File trackedDir = new File(directory);
        directoryTracker.remove(trackedDir);
        directoryWatcher.unwatch(trackedDir);

        return tracked();
    }

    private Tracked tracked() {
        List<String> tracked = directoryTracker.getTracked().stream()
                .map(File::getAbsolutePath)
                .sorted()
                .collect(toList());
        return new Tracked(links.apiLinks(), tracked);
    }

    public static class Tracked {
        public final Collection<String> directories;
        public final ApiLinks api;

        Tracked(final ApiLinks api, final Collection<String> directories) {
            this.api = api;
            this.directories = directories;
        }
    }

    public static class Error {
        public final String error;
        public final ApiLinks api;

        Error(final ApiLinks api, final String error) {
            this.api = api;
            this.error = error;
        }
    }
}
