package com.atlassian.labs.plugins.quickreload;

import com.atlassian.labs.plugins.quickreload.config.AutoRefresher;
import com.atlassian.labs.plugins.quickreload.config.ConfigReader;
import com.atlassian.labs.plugins.quickreload.inspectors.EventInspector;
import com.atlassian.labs.plugins.quickreload.install.PluginInstaller;
import com.atlassian.labs.plugins.quickreload.utils.VersionKit;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.slf4j.Logger;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static com.atlassian.labs.plugins.quickreload.utils.QuickReloadThreads.newSingleThreadScheduledExecutor;
import static java.time.Duration.ofSeconds;
import static java.util.Collections.reverse;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Our single launch point for all things in this plugin
 */
public class Launcher implements LifecycleAware {
    private final static Logger log = setInfo(getLogger(Launcher.class));
    private final static Duration STARTUP_DELAY = ofSeconds(5);
    private final Set<LifecycledComponent> lifecycled = new LinkedHashSet<>();
    private final DirectoryTracker directoryTracker;
    private final WittyQuoter wittyQuoter;

    public Launcher(
            final DirectoryWatcher directoryWatcher,
            final ConfigReader configReader,
            final PluginInstaller pluginInstaller,
            final DirectoryTracker directoryTracker,
            final AutoRefresher autoRefresher,
            final VersionKit versionKit,  // Needs to be wired before launching
            final WittyQuoter wittyQuoter,
            final EventInspector eventInspector)
    {
        this.directoryTracker = directoryTracker;
        this.wittyQuoter = wittyQuoter;
        // order matters here
        add(wittyQuoter)
                .add(configReader)
                .add(directoryWatcher)
                .add(pluginInstaller)
                .add(autoRefresher)
                .add(eventInspector);
    }

    private Launcher add(LifecycledComponent c)
    {
        lifecycled.add(c);
        return this;
    }

    @Override
    public void onStart() {
        //
        // QuickReload needs to be quick.  So rather than block plugin startup with File I/O - we offload that another thread and do it soon-ish
        // that way the plugin system can get on with starting all the other plugins
        //
        ScheduledExecutorService startupThreadExecutor = newSingleThreadScheduledExecutor("Initial Configuration");
        startupThreadExecutor.schedule(() -> {
            lifecycled.forEach(LifecycledComponent::onStartup);
            startupThreadExecutor.shutdown();
        }, STARTUP_DELAY.getSeconds(), SECONDS);

        log.info("\n\nQuickReload plugin is present. It will in start in earnest in {}\n", STARTUP_DELAY);
    }

    @Override
    public void onStop() {
        // go down in reverse order
        List<LifecycledComponent> shutdownList = new ArrayList<>(lifecycled);
        reverse(shutdownList);
        shutdownList.forEach(lifecycledComponent -> {
            try {
                lifecycledComponent.onShutdown();
            } catch (Exception e) {
                // Soldier on
            }
        });
    }
}
