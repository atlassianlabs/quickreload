package com.atlassian.labs.plugins.quickreload.rest.exception;

public class AlternateStatusCodeException extends RuntimeException {

    final int status;
    final String contentType;
    final Object entity;

    AlternateStatusCodeException(int status, String contentType, Object entity) {
        super();
        this.status = status;
        this.contentType = contentType;
        this.entity = entity;
    }
}
