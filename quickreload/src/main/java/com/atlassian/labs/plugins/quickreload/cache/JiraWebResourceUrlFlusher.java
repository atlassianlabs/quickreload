package com.atlassian.labs.plugins.quickreload.cache;

import com.atlassian.labs.plugins.quickreload.JiraWebResourceCacheFlushingAccessor;

public class JiraWebResourceUrlFlusher implements WebResourceUrlFlusher {

    @Override
    public void flushWebResourceUrls() {
        JiraWebResourceCacheFlushingAccessor.resetStateCounter();
    }
}
