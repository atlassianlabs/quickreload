package com.atlassian.labs.plugins.quickreload.config;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.startsWith;

/**
 * A class that can read config files as lines of entries
 */
public class BasicConfigFileReader
{

    public List<String> readConfigFileEntries(final File configFile) throws IOException
    {
        List<String> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(configFile)))
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                line = StringUtils.trim(line);
                if (isSuitableEntry(line))
                {
                    list.add(line);
                }
            }
        }
        return list;
    }


    private boolean isSuitableEntry(final String line)
    {
        return !isComment(line) && isNotBlank(line);
    }

    private boolean isComment(final String line)
    {
        return startsWith(StringUtils.trim(line), "#");
    }

}
