package com.atlassian.labs.plugins.quickreload.utils;

import com.atlassian.labs.plugins.quickreload.utils.logging.LogLevelSetter;
import com.atlassian.labs.plugins.quickreload.utils.logging.LogLevelSetter.Level;
import org.slf4j.Logger;

import java.util.List;

import static com.atlassian.labs.plugins.quickreload.utils.logging.LogLevelSetter.Level.INFO;
import static com.atlassian.labs.plugins.quickreload.utils.logging.LogLevelSetter.Level.WARN;
import static java.util.Arrays.asList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * slf4j does not allow usa to set log levels but log4j does.  So lets level up!
 * <p>
 * WARNING :
 * </p>
 * <p>
 * If you are using this in an OSGI world, and these days who isn't, then make sure you import org.apache.log4j
 * explicitly as the reflective nature means that BND will never find this dependency and hence you wont be able to see
 * log4j classes
 * </p>
 */
@SuppressWarnings ("UnusedDeclaration")
public class LogLeveller {

    private static final Logger log = getLogger(LogLeveller.class);

    public static Logger setInfo(Logger slf4jLogger) {
        return setLevelImpl(slf4jLogger, INFO);
    }

    public static Logger setWarn(Logger slf4jLogger) {
        return setLevelImpl(slf4jLogger, WARN);
    }

    private static Logger setLevelImpl(final Logger slf4jLogger, Level level) {
        final List<String> classNames = asList(
                "com.atlassian.labs.plugins.quickreload.utils.logging.LogbackLogLevelSetter",
                // Prioritise this implementation so it works in RefApp & Jira Plat 7
                "com.atlassian.labs.plugins.quickreload.utils.logging.Log4j2LevelSetter",
                "com.atlassian.labs.plugins.quickreload.utils.logging.Log4j1LevelSetter");

        for (String className : classNames) {
            try {
                Class<?> setterClass = Class.forName(className);
                LogLevelSetter setter = (LogLevelSetter) setterClass.newInstance();
                return setter.set(slf4jLogger, level);
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Illegal classname hardcoded: " + className);
            } catch (Exception | NoClassDefFoundError | NoSuchMethodError e) {
                log.debug("Failed to instantiate the appropriate LogLevelSetter: {}", className, e);
            }
        }
        throw new RuntimeException("Unsupported logger configuration");
    }
}
