package com.atlassian.labs.plugins.quickreload.utils;

import java.util.concurrent.TimeUnit;

public class Timer
{
    static interface Clock
    {
        long currentMillis();
    }

    private static final Clock REAL_CLOCK = new Clock()
    {
        @Override
        public long currentMillis()
        {
            return System.currentTimeMillis();
        }
    };

    private final Clock clock;
    private long then;

    public Timer()
    {
        this(REAL_CLOCK);
    }

    Timer(Clock clock)
    {
        this.clock = clock;
        this.then = clock.currentMillis();
    }

    public long elapsedMillis()
    {
        return clock.currentMillis() - then;
    }

    public boolean hasElapsed(long duration, TimeUnit timeUnit)
    {
        long durationMS = timeUnit.toMillis(duration);
        return hasElapsed(durationMS);
    }

    public boolean hasElapsed(long durationMS)
    {
        long passedMS = elapsedMillis();
        return passedMS >= durationMS;
    }

    public boolean isInside(long durationMS)
    {
        return ! hasElapsed(durationMS);
    }

    public boolean isInside(long duration, TimeUnit timeUnit)
    {
        long durationMS = timeUnit.toMillis(duration);
        return isInside(durationMS);
    }

    public void reset()
    {
        this.then = clock.currentMillis();
    }

}
