package com.atlassian.labs.plugins.quickreload.utils;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setWarn;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * File utilities
 */
@SuppressWarnings ("UnusedDeclaration")
public class Files {
    private static final Logger log = setWarn(getLogger(Files.class));

    public static File smooshNames(String startingPath, String pathToAdd)
    {
        String smooshed = FilenameUtils.concat(startingPath, pathToAdd);
        return new File(smooshed);
    }

    public static File smooshNames(File startingPath, String pathToAdd)
    {
        String smooshed = FilenameUtils.concat(startingPath.getAbsolutePath(), pathToAdd);
        return new File(smooshed);
    }

    public static File smooshNames(File startingPath, File pathToAdd)
    {
        String smooshed = FilenameUtils.concat(startingPath.getAbsolutePath(), pathToAdd.getAbsolutePath());
        return new File(smooshed);
    }

    public static void mkdirs(final File file)
    {
        //noinspection ResultOfMethodCallIgnored
        file.mkdirs();
    }

    public static File pwd()
    {
        return new File(new File("pwd").getAbsolutePath()).getParentFile();
    }


    public static void traverseUpLookingForFile(String fileName, File startingDir, Consumer<File> sideEffect)
    {
        File dir = startingDir;
        while (dir != null)
        {
            File file = smooshNames(dir, fileName);
            if (file.exists() && file.isFile())
            {
                sideEffect.accept(file);
            }
            dir = dir.getParentFile();
        }
    }

    public static void traverseUpLookingForFileExcludingEndingDir(String fileName, File startingDir, File endingDir,
                                                                  Consumer<File> sideEffect)
    {
        File dir = startingDir;
        while (dir != null)
        {
            if (dir.equals(endingDir)) {
                break;
            }
            File file = smooshNames(dir, fileName);
            if (file.exists() && file.isFile())
            {
                sideEffect.accept(file);
            }
            dir = dir.getParentFile();
        }
    }

    public static Optional<File> traverseUpForFileThenYouFindItAndTheCant(String fileName, File startingDir)
    {
        // try and find it at least once
        Optional<File> found = traverseUpTillItsFound(fileName, startingDir);
        if (found.isPresent())
        {
            // no find the
            return traverseUpTillItsNotThere(found.get());
        }
        return found;
    }

    private static Optional<File> traverseUpTillItsFound(final String fileName, final File startingDir)
    {
        File dir = startingDir;
        while (dir != null)
        {
            File file = smooshNames(dir, fileName);
            if (file.exists() && file.isFile())
            {
                return Optional.of(file);
            }
            dir = dir.getParentFile();
        }
        return Optional.empty();
    }

    private static Optional<File> traverseUpTillItsNotThere(File startingFile)
    {
        if (!startingFile.isFile())
        {
            throw new IllegalArgumentException();
        }

        Optional<File> lastFile = Optional.of(startingFile);

        String desiredName = startingFile.getName();
        File dir = startingFile.getParentFile().getParentFile();

        while (dir != null)
        {
            File file = smooshNames(dir, desiredName);
            if (file.exists() && file.isFile())
            {
                lastFile = Optional.of(file);
                dir = dir.getParentFile();
            }
            else
            {
                // no more so out we go
                break;
            }
        }
        return lastFile;
    }

    public static void traverseDownLookingForFile(String fileName, File startingDir, Function<Path, Boolean> directoryFilter, Consumer<File> sideEffect)
    {
        try {
            java.nio.file.Files.walkFileTree(startingDir.toPath(), new DirectoryVisitor(directoryFilter, fileName, sideEffect));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean directoryExists(final File potentialDir)
    {
        return potentialDir.exists() && potentialDir.isDirectory();
    }

    public static boolean pathContainsDirectory(final File file, final String targetName)
    {
        File f = file;
        while (f != null)
        {
            if (f.isDirectory())
            {
                if (f.getName().equals(targetName))
                {
                    return true;
                }
            }
            f = f.getParentFile();
        }
        return false;
    }

    public static boolean isAtlassianPlugin(final File jarFile)
    {
        try
        {
            JarFile jar = new JarFile(jarFile);
            ZipEntry entry = jar.getEntry("atlassian-plugin.xml");
            return entry != null && !entry.isDirectory();
        }
        catch (IOException e)
        {
            return false;
        }
    }

    public static Collection<File> getAllAtlassianPlugins(File dir) {
        if (!dir.isDirectory()) {
            return emptyList();
        }

        File[] files = dir.listFiles();
        assert files != null;

        return stream(files)
                .filter(Files::isAtlassianPlugin)
                .collect(toList());
    }

    public static boolean hasPomDotXml(File dir) {
        File pom = new File(dir, "pom.xml");
        return pom.exists() && pom.isFile();
    }

    public static boolean isSourceJar(final File file)
    {
        return file.getName().endsWith("-sources.jar");
    }

    public static File getHomeDir()
    {
        return new File(System.getProperty("user.home"));
    }

    private static class DirectoryVisitor extends SimpleFileVisitor<Path> {
        private final Function<Path, Boolean> directoryFilter;
        private final String fileNameToAccept;
        private final Consumer<File> consumerForAcceptedFiles;

        public DirectoryVisitor(Function<Path, Boolean> directoryFilter, String fileName, Consumer<File> sideEffect) {
            this.directoryFilter = directoryFilter;
            this.fileNameToAccept = fileName;
            this.consumerForAcceptedFiles = sideEffect;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path file, BasicFileAttributes attrs) {
            if (!directoryFilter.apply(file)) {
                return FileVisitResult.SKIP_SUBTREE;
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            if (file.toString().endsWith(fileNameToAccept)) {
                consumerForAcceptedFiles.accept(file.toFile());
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path path, IOException exception) {
            log.error("Failed to process file: {}", path);
            log.debug("File visit exception", exception);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path path, IOException exception) {
            if (exception != null) {
                log.error("Failed to process directory: {}", path);
                log.debug("Directory visit exception", exception);
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
