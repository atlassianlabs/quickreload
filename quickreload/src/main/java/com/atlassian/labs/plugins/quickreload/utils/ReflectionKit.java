package com.atlassian.labs.plugins.quickreload.utils;

import org.joor.Reflect;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 */
public final class ReflectionKit
{
    private ReflectionKit() {}

    public static Map<String, String> asJsonMap(final Object object)
    {
        final Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("class", object.getClass().getName());
        try
        {
            Map<String, Reflect> fields = Reflect.on(object).fields();
            Map<String, String> fieldsMap = fields.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry<String, Reflect>::getKey, entry -> {
                        try
                        {
                            return String.valueOf(entry.getValue().get());
                        }
                        catch (RuntimeException ignored)
                        {
                            return "??";
                        }
                    }));
            jsonMap.putAll(fieldsMap);
        }
        catch (RuntimeException ignored)
        {
        }
        return jsonMap;
    }
}
