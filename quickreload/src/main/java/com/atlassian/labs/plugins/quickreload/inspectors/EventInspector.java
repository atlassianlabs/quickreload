package com.atlassian.labs.plugins.quickreload.inspectors;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.labs.plugins.quickreload.LifecycledComponent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class EventInspector implements LifecycledComponent {

    private final EventPublisher eventPublisher;
    private final BoundedList<Event> events = new BoundedList<Event>(200);

    public EventInspector(final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void onStartup()
    {
        eventPublisher.register(this);
    }

    @Override
    public void onShutdown()
    {
        eventPublisher.unregister(this);
    }

    @SuppressWarnings ("UnusedDeclaration")
    @EventListener
    public void onEvent(Object rawEvent)
    {
        Event event = new Event(System.currentTimeMillis(), rawEvent);
        synchronized (this)
        {
            events.add(event);
        }
    }

    public List<Event> getCurrentEvents()
    {
        ArrayList<Event> out;
        synchronized (this)
        {
            out = new ArrayList<>(events);
        }
        Collections.reverse(out);
        return out;
    }

    public static class Event {
        private final long when;
        private final Object event;

        public Event(final long when, final Object event)
        {
            this.when = when;
            this.event = requireNonNull(event);
        }

        public Object getEvent()
        {
            return event;
        }

        public long getWhen()
        {
            return when;
        }

    }

    /**
     * A simple list that holds at most the last maxSize elements and throws away from the start of the list when it exceeds
     * that size
     */
    public static class BoundedList<E> extends LinkedList<E>
    {
        private final int maxSize;

        public BoundedList(final int maxSize) {this.maxSize = maxSize;}

        @Override
        public boolean add(final E e)
        {
            if (size() >= maxSize)
            {
                removeFirst();
            }
            return super.add(e);
        }

        @Override
        public void addFirst(final E e)
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void addLast(final E e)
        {
            add(e);
        }
    }
}
