package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.Map;

import static com.atlassian.labs.plugins.quickreload.SystemProperties.clearSystemProperty;
import static com.atlassian.labs.plugins.quickreload.SystemProperties.getSystemProperties;
import static com.atlassian.labs.plugins.quickreload.SystemProperties.setSystemProperty;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED}) @jakarta.ws.rs.Consumes({APPLICATION_JSON, TEXT_PLAIN, APPLICATION_FORM_URLENCODED})
@Path("/systemproperties") @jakarta.ws.rs.Path("/systemproperties")
public class SystemPropertiesResource {

    private final Links links;

    @Inject @jakarta.inject.Inject
    public SystemPropertiesResource(Links links) {
        this.links = links;
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public SystemPropertiesResponse getProperties() {
        return ok();
    }


    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    @Path("{name}/{value : .+?}") @jakarta.ws.rs.Path("{name}/{value : .+?}")
    public SystemPropertiesResponse setPropertyAsGet(
            @PathParam("name") @jakarta.ws.rs.PathParam("name") String name,
            @PathParam("value") @jakarta.ws.rs.PathParam("value") String value) {
        // mutation via GET is allowed under the REST do-ocracy
        return setProperty(name, value);
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @PUT @jakarta.ws.rs.PUT
    @Path("{name}/{value : .+?}") @jakarta.ws.rs.Path("{name}/{value : .+?}")
    public SystemPropertiesResponse setProperty(
            @PathParam("name") @jakarta.ws.rs.PathParam("name") String name,
            @PathParam("value") @jakarta.ws.rs.PathParam("value") String value) {
        setSystemProperty(name, value);

        return ok();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @PUT @jakarta.ws.rs.PUT
    @Path("{name}") @jakarta.ws.rs.Path("{name}")
    public SystemPropertiesResponse setPropertyViaBody(
            @PathParam("name") @jakarta.ws.rs.PathParam("name") String name,
            String value) {
        setProperty(name, value);
        return ok();
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @DELETE @jakarta.ws.rs.DELETE
    @Path("{name}") @jakarta.ws.rs.Path("{name}")
    public SystemPropertiesResponse clearProperty(@PathParam("name") @jakarta.ws.rs.PathParam("name") String name) {
        clearSystemProperty(name);
        return ok();
    }

    private SystemPropertiesResponse ok() {
        return new SystemPropertiesResponse(links.apiLinks(), getSystemProperties());
    }

    static class SystemPropertiesResponse {
        public final Map<String, String> properties;
        public final ApiLinks api;

        public SystemPropertiesResponse(final ApiLinks api, Map<String, String> properties) {
            this.api = api;
            this.properties = properties;
        }
    }
}
