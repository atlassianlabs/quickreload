package com.atlassian.labs.plugins.quickreload.config;

import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.DirectoryWatcher;
import com.atlassian.labs.plugins.quickreload.LifecycledComponent;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.WittyQuoter;
import com.atlassian.labs.plugins.quickreload.utils.VersionKit;
import com.atlassian.sal.api.ApplicationProperties;
import org.slf4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.labs.plugins.quickreload.DirectoryTracker.PLUGIN_RESOURCE_DIRECTORIES;
import static com.atlassian.labs.plugins.quickreload.SystemProperties.getSystemPropertySplit;
import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static com.atlassian.sal.api.UrlMode.ABSOLUTE;
import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * We can watch our own specific config file and refresh the backing values
 *
 * @since v0.x
 */
public class AutoRefresher implements LifecycledComponent {
    private static final Logger log = setInfo(getLogger(AutoRefresher.class));

    private final ApplicationProperties applicationProperties;
    private final DirectoryWatcher directoryWatcher;
    private final ConfigReader configReader;
    private final DirectoryTracker directoryTracker;
    private final WittyQuoter wittyQuoter;
    private final StateManager stateManager;

    public AutoRefresher(
            final ApplicationProperties applicationProperties,
            final DirectoryWatcher directoryWatcher,
            final ConfigReader configReader,
            final DirectoryTracker directoryTracker,
            final WittyQuoter wittyQuoter,
            final StateManager stateManager) {
        this.applicationProperties = applicationProperties;
        this.directoryWatcher = directoryWatcher;
        this.configReader = configReader;
        this.directoryTracker = directoryTracker;
        this.wittyQuoter = wittyQuoter;
        this.stateManager = stateManager;
    }

    @Override
    public void onStartup()
    {
        // watch the home quickreload.properties for changes
        directoryWatcher.watchSpecificFile(configReader.getHomeConfigurationFile(), onChange());
        //
        // watch all the quickreload.properties from where I am running for changes
        List<File> localQRConfigFiles = configReader.getLocalQRConfigFiles();
        for (File localQRConfigFile : localQRConfigFiles) {
            directoryWatcher.watchSpecificFile(localQRConfigFile, onChange());
        }
        startupMessage();
    }

    private Function<File, Void> onChange()
    {
        return from -> {
            configReader.onRefresh();
            directoryWatcher.onRefresh();
            startupMessage();
            return null;
        };
    }

    private void startupMessage()
    {
        StringBuilder sb = new StringBuilder();
        String version = VersionKit.getDisplayedVersion();
        sb.append(format("\n\n Quick Reload v%s - %s\n", version, wittyQuoter.halfWit()));

        List<File> tracked = new ArrayList<>(directoryTracker.getTracked());
        Collections.sort(tracked);
        if (!tracked.isEmpty())
        {
            sb.append("\n    Tracking the following directories for changes : \n\n");
            for (File file : tracked) {
                sb.append(format("        %s\n", file.getAbsolutePath()));
            }
        }
        List<String> alternateResourceDirs = new ArrayList<>(new HashSet<>(getSystemPropertySplit(PLUGIN_RESOURCE_DIRECTORIES, ",")));
        Collections.sort(alternateResourceDirs);
        if (!alternateResourceDirs.isEmpty()) {
            sb.append(format("\n    The system property '%s' is as follows : \n\n", PLUGIN_RESOURCE_DIRECTORIES));
            for (String resourceDir : alternateResourceDirs) {
                sb.append(format("        %s\n", resourceDir));
            }
        }

        sb.append(format("\n    The Control Panel page is available at : \n\n        %s/qr \n", applicationProperties.getBaseUrl(ABSOLUTE)));
        sb.append(format("\n    The REST API is available at : \n\n        %s/rest/qr/api \n", applicationProperties.getBaseUrl(ABSOLUTE)));

        if(stateManager.isBatchingEnabled()) {
            sb.append("\n    Fast mode - web resource batching enabled, 'atlassian.dev.mode' disabled\n");
        } else {
            sb.append("\n    Dev mode - web resource batching disabled, 'atlassian.dev.mode' enabled\n");
        }

        log.info(sb.toString());
    }

    @Override
    public void onShutdown()
    {
    }
}
