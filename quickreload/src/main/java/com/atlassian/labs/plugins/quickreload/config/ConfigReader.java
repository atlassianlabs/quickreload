package com.atlassian.labs.plugins.quickreload.config;

import com.atlassian.labs.plugins.quickreload.DirectoryTracker;
import com.atlassian.labs.plugins.quickreload.LifecycledComponent;
import com.atlassian.labs.plugins.quickreload.QrConfiguration;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.utils.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import static com.atlassian.labs.plugins.quickreload.QrConfiguration.WEB_RESOURCE_BATCHING_KEY;
import static com.atlassian.labs.plugins.quickreload.utils.LogLeveller.setInfo;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Reads our config from well known files OR from system properties
 */
public class ConfigReader implements LifecycledComponent {
    private static final Logger log = setInfo(getLogger(ConfigReader.class));

    private static final String LEGACY_HOME_FILE = "/.quickreload/quickreload";
    private static final String HOME_FILE = "/.quickreload";
    private static final String QUICKRELOAD_TRACK = "quickreload.tracked";
    private static final String LEGACY_DOT_QUICKRELOAD = ".quickreload";
    private static final String QR_DOT_PROPERTIES = "quickreload.properties";
    private static final String START_DISCOVER_REPO_ROOT_FROM_DIR = "quick.reload.discovery.startDir";

    private final DirectoryTracker directoryTracker;
    private final QrConfiguration qrConfiguration;
    private final StateManager stateManager;

    public ConfigReader(final DirectoryTracker directoryTracker, final QrConfiguration qrConfiguration, final StateManager stateManager)
    {
        this.directoryTracker = directoryTracker;
        this.qrConfiguration = qrConfiguration;
        this.stateManager = stateManager;
    }

    public void onStartup()
    {
        reReadConfiguration();
    }

    public void onRefresh()
    {
        reReadConfiguration();
    }

    private void reReadConfiguration()
    {
        //
        // read the pom convention first.  People can use ! syntax to override this more aggressive stance later
        runPomXmlConvention();

        //
        // read the ~/quickreload.properties
        File homeFile = getHomeConfigurationFile();
        readConfigFile(homeFile, ConfigFileMode.GLOBAL);

        //
        // read the legacy ~/.quickreload
        File homeDotQuickReload = getSecondLegacyConfigurationFile();
        readConfigFile(homeDotQuickReload, ConfigFileMode.GLOBAL);

        //
        // read the legacy ~/.quickrelaod/quickreload
        homeDotQuickReload = getFirstLegacyConfigurationFile();
        readConfigFile(homeDotQuickReload, ConfigFileMode.GLOBAL);

        //
        // we can go from where we are running upwards until we find a quickreload.properties and .quickreload files
        //
        findLocalConfig();
        //
        // now read system properties
        readSystemProperty();

        directoryTracker.refinedTrackedDirsList();

        // After reading in system properties, apply QuickReload config changes if desired
        applyQrConfigChanges();
    }

    /**
     * Give the location where quickreload will start traversing up
     * looking for outter most directory that contains a pom.xml
     * With assumption that this would be the root of repo,
     * where it would discover all target/, that need to be monitored
     *
     * in case -Dquick.reload.discovery.startDir isn't provided
     * Use the location where tomcat started from
     *
     * @return a file to look from or the current directory
     */
    private File getLocationToStartLookingForRepoRoot() {
        final String startDiscoverfrom = System.getProperty(START_DISCOVER_REPO_ROOT_FROM_DIR);
        if (!StringUtils.isEmpty(startDiscoverfrom))
            return new File(startDiscoverfrom);
        return Files.pwd();
    }

    private void runPomXmlConvention()
    {
        PomConventionStrategy strategy = new PomConventionStrategy(getLocationToStartLookingForRepoRoot());
        List<File> targetDirs = strategy.getTargetDirs();
        for (File dir : targetDirs)
        {
            directoryTracker.add(dir);
        }
    }

    public File getFirstLegacyConfigurationFile()
    {
        return readHomeFile(LEGACY_HOME_FILE, FileCreation.NO_CREATE);
    }

    public File getSecondLegacyConfigurationFile()
    {
        return readHomeFile(HOME_FILE, FileCreation.NO_CREATE);
    }

    /**
     * @return the home configuration file - quickreload.properties
     */
    public File getHomeConfigurationFile()
    {
        return readHomeFile(QR_DOT_PROPERTIES, FileCreation.CREATE_WITH_DEFAULTS);
    }

    private enum FileCreation
    {
        CREATE_WITH_DEFAULTS, NO_CREATE
    }

    private File readHomeFile(String homeFile, FileCreation create)
    {
        File userHome = Files.getHomeDir();
        File configFile = new File(userHome, homeFile);

        if (create == FileCreation.CREATE_WITH_DEFAULTS && !configFile.exists())
        {
            touch(configFile);
        }
        return configFile;
    }

    private void findLocalConfig()
    {
        Consumer<File> sideEffect = dotQuickReload -> {
            // try and read as a configuration file
            readConfigFile(dotQuickReload, ConfigFileMode.LOCAL);
        };
        // legacy .quickreload name
        Files.traverseUpLookingForFileExcludingEndingDir(LEGACY_DOT_QUICKRELOAD, Files.pwd(), Files.getHomeDir(), sideEffect);
        // new quickreload.properties name
        Files.traverseUpLookingForFileExcludingEndingDir(QR_DOT_PROPERTIES, Files.pwd(), Files.getHomeDir(), sideEffect);
    }

    /**
     * @return a list of quickreload.properties config files that are from the running path upwards
     */
    public List<File> getLocalQRConfigFiles()
    {
        final List<File> files = new ArrayList<>();
        // try and read as a configuration file
        Files.traverseUpLookingForFileExcludingEndingDir(QR_DOT_PROPERTIES, Files.pwd(), Files.getHomeDir(), files::add);
        return files;

    }

    private void applyQrConfigChanges()
    {
        qrConfiguration.getFlag(WEB_RESOURCE_BATCHING_KEY).ifPresent(batchingDesiredState -> {
            boolean batchingCurrentState = stateManager.isBatchingEnabled();
            if(batchingDesiredState != batchingCurrentState)
            {
                if(batchingDesiredState)
                {
                    stateManager.enableBatching();
                }
                else
                {
                    stateManager.disableBatching();
                }
            }
        });
    }

    @Override
    public void onShutdown()
    {
    }

    private void readSystemProperty()
    {
        String quickReloadDirs = StringUtils.defaultString(System.getProperty(QUICKRELOAD_TRACK));
        String[] dirs = StringUtils.split(quickReloadDirs, ",");
        for (String entry : dirs)
        {
            processDirectoryEntry(Optional.of(Files.pwd()), entry);
        }
    }


    private void touch(final File trackedDirList)
    {
        try
        {
            Files.mkdirs(trackedDirList.getParentFile());
            FileUtils.touch(trackedDirList);
            try (FileWriter out = new FileWriter(trackedDirList))
            {
                out.write("#\n"
                        + "# Quick Reload Config File\n"
                        + "# ------------------------------\n"
                        + "#\n"
                        + "# Add directories to this file, one on each line, and the quick reload support will monitor them\n"
                        + "# and automatically load them as soon as they are compiled and assembled.\n"
                        + "#\n"
                        + "# Directories can be relative to this configuration file and hence allow directories outside\n"
                        + "# your project to be monitored and reloaded."
                        + "#\n"
                        + "# You can use -Dquickreload.dirs=dir1,dir2,dir3 on the JVM command line to also specify directories\n"
                        + "# to monitor.\n"
                        + "#\n");
            }
        }
        catch (IOException ignored)
        {
        }
    }

    private enum ConfigFileMode
    {
        LOCAL, GLOBAL
    }

    private void readConfigFile(final File configFile, final ConfigFileMode configMode)
    {
        Optional<File> interpretRelativeToDir = Optional.of(configFile.getParentFile());
        List<String> fileEntries = readConfigFileEntries(configFile);

        // if a local .quickreload exists but is empty then take that as a sign they want an implicit entry to that directory
        if (configMode == ConfigFileMode.LOCAL && (configFile.exists() && configFile.isFile()) && fileEntries.size() == 0)
        {
            directoryTracker.add(configFile.getParentFile());
        }

        for (String entry : fileEntries)
        {
            processDirectoryEntry(interpretRelativeToDir, entry);
        }
    }

    private void processDirectoryEntry(final Optional<File> interpretRelativeToDir, final String entry)
    {
        ConfigEntry configEntry = parseEntry(entry);
        if (configEntry.isQrConfigEntry())
        {
            qrConfiguration.addConfiguration(configEntry.getQrConfigProperties());
        }
        else
        {
            String path = configEntry.path;
            Map<String, String> mapOfProperties = configEntry.getPathProperties();
            Optional<String> resourceEntry = Optional.ofNullable(mapOfProperties.get("resource"));
            if (resourceEntry.isPresent())
            {
                directoryTracker.addResourceEntry(path, interpretRelativeToDir);
            }
            else
            {
                //
                // a path can begin with ! and that means actively "remove" an entry if it exists
                if (path.startsWith("!"))
                {
                    path = StringUtils.substringAfter(path, "!").trim();
                    directoryTracker.removeFromTrackedAndBlacklistThisDir(path, interpretRelativeToDir, configEntry.getQrConfigProperties());
                }
                else
                {
                    directoryTracker.add(path, interpretRelativeToDir, mapOfProperties);
                }
            }

        }
    }

    public static class ConfigEntry
    {
        private final String path;
        private final Map<String, String> pathProperties;
        private final Map<String, String> qrConfigProperties;

        public ConfigEntry(final String path, final Map<String, String> pathProperties, final Map<String, String> qrConfigProperties)
        {
            this.path = path;
            this.pathProperties = pathProperties;
            this.qrConfigProperties = qrConfigProperties;
        }

        public boolean isQrConfigEntry()
        {
            return qrConfigProperties.size() > 0;
        }

        public String getPath()
        {
            return path;
        }

        public Map<String, String> getPathProperties()
        {
            return pathProperties;
        }

        public Map<String, String> getQrConfigProperties()
        {
            return qrConfigProperties;
        }
    }

    /**
     * Properties on paths can be in the form
     * <p>
     * prop1;prop2=4:/some/path
     * </p>
     *
     * Or we can have qr: config entries like
     *
     * qr:someconfig=true;andsomeotherconfig=true;
     */
    // VisibleForTesting
    ConfigEntry parseEntry(String entry)
    {
        entry = trim(entry);
        if (entry.matches("^qr\\s*:.*"))
        {
            // strip the qr: part
            entry = StringUtils.substringAfter(entry, ":");
            Map<String, String> qrProperties = parseProperties(entry);
            return new ConfigEntry(null, Collections.emptyMap(), qrProperties);
        }
        else
        {
            String path = trim(entry);
            String propertiesEntry = "";
            if (StringUtils.contains(entry, ':'))
            {
                path = trim(StringUtils.substringAfter(entry, ":"));
                propertiesEntry = StringUtils.substringBefore(entry, ":");
            }
            Map<String, String> properties = parseProperties(propertiesEntry);
            return new ConfigEntry(path, properties, Collections.emptyMap());
        }
    }

    private Map<String, String> parseProperties(final String propertiesEntry)
    {
        if (isNotBlank(propertiesEntry))
        {
            Map<String, String> properties = new HashMap<>();

            String[] values = StringUtils.split(propertiesEntry, ";");
            for (String value : values)
            {
                value = trim(value);
                if (StringUtils.startsWith(value,"="))
                {
                    // forget about =value meaning no key
                    continue;
                }
                String[] keyValues = StringUtils.split(value, "=");
                if (keyValues.length == 1 && isNotBlank(keyValues[0]))
                {
                    properties.put(trim(keyValues[0]), "true");
                }
                if (keyValues.length == 2 && isNotBlank(keyValues[0]))
                {
                    properties.put(trim(keyValues[0]), trim(keyValues[1]));
                }
            }
            return properties;
        }
        return Collections.emptyMap();
    }

    private List<String> readConfigFileEntries(final File configFile)
    {
        try
        {
            return new BasicConfigFileReader().readConfigFileEntries(configFile);
        }
        catch (FileNotFoundException ignored)
        {
        }
        catch (IOException e)
        {
            log.info(format("Unable to read file %s.  No plugins will be automatically tracked", configFile));
        }
        return Collections.emptyList();
    }

}
