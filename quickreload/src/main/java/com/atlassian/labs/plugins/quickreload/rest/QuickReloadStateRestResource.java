package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.StateManager;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static com.atlassian.labs.plugins.quickreload.rest.exception.RestApiExceptions.notFound;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 */
@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Path("/state") @jakarta.ws.rs.Path("/state")
public class QuickReloadStateRestResource {

    private final Links links;
    private final StateManager stateManager;

    @Inject @jakarta.inject.Inject
    public QuickReloadStateRestResource(Links links, StateManager stateManager) {
        this.links = links;
        this.stateManager = stateManager;
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public State state() {
        State state = new State(links.apiLinks(), enabled());

        if (enabled()) {
            return state;
        }

        throw notFound(state);
    }

    private boolean enabled() {return stateManager.isQuickReloadEnabled();}

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @PUT @jakarta.ws.rs.PUT
    public State enable() {
        if (!enabled()) {
            stateManager.enableQuickReload();
        }

        return new State(links.apiLinks(), enabled());
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @DELETE @jakarta.ws.rs.DELETE
    public State disable() {
        if (enabled()) {
            stateManager.disableQuickReload();
        }
        State state = new State(links.apiLinks(), enabled());
        throw notFound(state);
    }

    public static class State {
        public final boolean enabled;
        public final ApiLinks api;

        State(final ApiLinks api, final boolean enabled) {
            this.api = api;
            this.enabled = enabled;
        }
    }
}
