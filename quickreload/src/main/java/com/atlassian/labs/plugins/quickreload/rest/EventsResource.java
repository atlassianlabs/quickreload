package com.atlassian.labs.plugins.quickreload.rest;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.labs.plugins.quickreload.inspectors.EventInspector;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links;
import com.atlassian.labs.plugins.quickreload.rest.dto.Links.ApiLinks;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.time.Instant;
import java.util.Collection;
import java.util.Map;

import static com.atlassian.labs.plugins.quickreload.utils.ReflectionKit.asJsonMap;
import static java.time.Instant.ofEpochMilli;
import static java.time.ZoneId.systemDefault;
import static java.time.ZonedDateTime.ofInstant;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Produces(APPLICATION_JSON) @jakarta.ws.rs.Produces(APPLICATION_JSON)
@Path("/events") @jakarta.ws.rs.Path("/events")
public class EventsResource {

    private final EventInspector eventInspector;
    private final Links links;

    @Inject @jakarta.inject.Inject
    public EventsResource(
            final EventInspector eventInspector,
            final Links links) {
        this.eventInspector = requireNonNull(eventInspector, "eventInspector");
        this.links = requireNonNull(links, "links");
    }

    @UnrestrictedAccess @AnonymousAllowed // dual annotated for forward+back compat
    @XsrfProtectionExcluded
    @GET @jakarta.ws.rs.GET
    public Events getEventsPlural() {
        return new Events(links.apiLinks(), eventInspector.getCurrentEvents());
    }

    static class Event {
        public final String when;
        public final String eventClass;
        public final Map<String, String> event;

        Event(final EventInspector.Event event) {
            Instant instantEventOccurred = ofEpochMilli(event.getWhen());
            this.when = ofInstant(instantEventOccurred, systemDefault()).format(ISO_OFFSET_DATE_TIME);
            this.eventClass = event.getEvent().getClass().getName();
            this.event = asJsonMap(event.getEvent());
        }
    }

    static class Events {
        public final Collection<Event> events;
        public final ApiLinks api;

        public Events(final ApiLinks api, final Collection<EventInspector.Event> events) {
            this.api = api;
            this.events = events.stream().map(Event::new).collect(toList());
        }
    }
}
