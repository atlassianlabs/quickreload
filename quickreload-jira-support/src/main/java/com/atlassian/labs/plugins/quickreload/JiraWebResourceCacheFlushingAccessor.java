package com.atlassian.labs.plugins.quickreload;

import com.atlassian.jira.component.ComponentAccessor;
import org.slf4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.slf4j.LoggerFactory.getLogger;

public class JiraWebResourceCacheFlushingAccessor {
    private static final Logger log = getLogger(JiraWebResourceCacheFlushingAccessor.class);

    public static void resetStateCounter() {
        try {
            Class<?> uiSettingsStateManagerClazz = null;
            try {
                uiSettingsStateManagerClazz = Class.forName("com.atlassian.jira.config.properties.UiSettingsStateManager");
            } catch (ClassNotFoundException e) {
                log.warn("This doesn't look like Jira");
                return;
            }
            Object uiSettingsStateManagerBean = ComponentAccessor.getComponent(uiSettingsStateManagerClazz);
            Method invalidateStateHashMethod = uiSettingsStateManagerClazz.getMethod("invalidateStateHash");
            invalidateStateHashMethod.invoke(uiSettingsStateManagerBean);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.warn("Failed to call UiSettingsStateManager#invalidateStateHash() reflectively. This can be ignored for earlier versions of JIRA");
        }
    }
}
