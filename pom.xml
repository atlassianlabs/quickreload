<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>6.3.8</version>
    </parent>

    <groupId>com.atlassian.labs.plugins</groupId>
    <artifactId>quickreload-parent</artifactId>
    <version>6.1.2-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Atlassian Quick Reload Parent</name>

    <modules>
        <module>quickreload-jira-support</module>
        <module>quickreload</module>
        <module>test-plugin</module>
        <module>test-plugin-2</module>
        <module>not-test-plugin</module>
    </modules>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassianlabs/quickreload.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassianlabs/quickreload.git</developerConnection>
        <url>https://bitbucket.org/atlassianlabs/quickreload</url>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <amps.version>9.2.2</amps.version>
        <platform.version>5.3.48</platform.version>
        <osgi.core.version>6.0.0</osgi.core.version>
        <osgi.javaconfig.version>0.6.0</osgi.javaconfig.version>

        <!-- To be forward compatible on conditions, but also backwards compatible on Java 8 -->
        <webresource.version>7.0.0-m03</webresource.version>

        <atlassian.scanner.version>2.2.4</atlassian.scanner.version>
        <bamboo.version>9.2.12</bamboo.version>
        <bitbucket.version>8.9.11</bitbucket.version>
        <crowd.version>5.2.3</crowd.version>
        <confluence.version>7.19.20</confluence.version>
        <fecru.version>4.8.11</fecru.version>
        <jira.version>9.12.17</jira.version>
        <jira.data.version>${jira.version}</jira.data.version>
        <refapp.version>7.0.13</refapp.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- Local dependencies -->
            <dependency>
                <groupId>com.atlassian.labs.plugins</groupId>
                <artifactId>quickreload-jira-support</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- Platform dependencies -->
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>platform</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>third-party</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.atlassian.plugins</groupId>
                <artifactId>atlassian-plugins-webresource-api</artifactId>
                <version>${webresource.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.plugins</groupId>
                <artifactId>atlassian-plugins-webresource-spi</artifactId>
                <version>${webresource.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.plugins</groupId>
                <artifactId>atlassian-plugins-webresource</artifactId>
                <version>${webresource.version}</version>
            </dependency>

            <!-- Other dependencies -->
            <dependency>
                <groupId>com.atlassian.jira</groupId>
                <artifactId>jira-api</artifactId>
                <version>${jira.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.plugins</groupId>
                <artifactId>atlassian-plugins-osgi-javaconfig</artifactId>
                <version>${osgi.javaconfig.version}</version>
            </dependency>
            <dependency>
                <groupId>javax.inject</groupId>
                <artifactId>javax.inject</artifactId>
                <version>1</version>
            </dependency>
            <dependency>
                <groupId>jakarta.servlet</groupId>
                <artifactId>jakarta.servlet-api</artifactId>
                <version>5.0.0</version>
            </dependency>
            <dependency>
                <groupId>javax.ws.rs</groupId>
                <artifactId>jsr311-api</artifactId>
                <version>1.1.1</version>
            </dependency>
            <dependency>
                <groupId>org.apache.sling</groupId>
                <artifactId>org.apache.sling.commons.osgi</artifactId>
                <version>2.4.2</version>
            </dependency>
            <dependency>
                <groupId>org.jooq</groupId>
                <artifactId>joor-java-8</artifactId>
                <version>0.9.15</version>
            </dependency>
            <dependency>
                <groupId>org.osgi</groupId>
                <artifactId>osgi.core</artifactId>
                <version>${osgi.core.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.code.gson</groupId>
                <artifactId>gson</artifactId>
                <version>2.10.1</version>
            </dependency>
            <dependency>
                <groupId>xerces</groupId>
                <artifactId>xercesImpl</artifactId>
                <version>2.12.2</version>
            </dependency>
            <dependency>
                <groupId>commons-beanutils</groupId>
                <artifactId>commons-beanutils</artifactId>
                <version>1.9.4</version>
            </dependency>
            <dependency>
                <groupId>dom4j</groupId>
                <artifactId>dom4j</artifactId>
                <version>1.6.1-atlassian-4</version>
            </dependency>
            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>1.2.17-atlassian-18</version>
            </dependency>

            <dependency>
                <groupId>io.methvin</groupId>
                <artifactId>directory-watcher</artifactId>
                <version>0.18.0</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>jira-maven-plugin</artifactId>
                    <version>${amps.version}</version>
                </plugin>
                <plugin>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>amps-maven-plugin</artifactId>
                    <version>${amps.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <!-- Disable the "hammer" rule -->
                        <id>ban-milestones-and-release-candidates</id>
                        <phase>none</phase>
                    </execution>
                    <execution>
                        <!-- Disable the build environment rules (allows using AMPS snapshots) -->
                        <id>enforce-build-environment</id>
                        <phase>none</phase>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
