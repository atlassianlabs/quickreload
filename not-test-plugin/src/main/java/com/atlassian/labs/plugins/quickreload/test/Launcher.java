package com.atlassian.labs.plugins.quickreload.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * The starting point for the code
 */
@SuppressWarnings ("UnusedDeclaration")
@Service
public class Launcher implements InitializingBean, DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(Launcher.class);

    @Override
    public void afterPropertiesSet() throws Exception
    {
        log.error("\n\n"
                + "Quick Reload Not Test Plugin is coming into existence!!  That seems bad right???"
                + "\n\n");
    }

    @Override
    public void destroy() throws Exception
    {
        log.error("\n\n"
                + "Quick Reload Not Test Plugin is going out of existence!!  That seems bad right???"
                + "\n\n");
    }
}
