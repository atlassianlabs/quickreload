# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [6.1.1] - 2025-02-11

* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) FIx logging for RefApp 6.5.38
* [QR-30](https://ecosystem.atlassian.net/browse/QR-30) Fix security annotations on REST endpoints

## [6.1.0] - 2025-02-10

### Added
* [QR-26](https://ecosystem.atlassian.net/browse/QR-26) Ability to upload plugin archives via the REST API
* [QR-26](https://ecosystem.atlassian.net/browse/QR-26) Ability to install a whole directory of plugin archives
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Add UI for common REST API use cases
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Add bundle+plugin detail links to API REST response
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Add plugins+services API links to servlet page table
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Add GET method for tracking directories and installing plugins (allows ease of use from browser)
* [QR-28](https://ecosystem.atlassian.net/browse/QR-28) Looser matching on tracked directories (will recurse up to find pom.xml files)
* [QR-28](https://ecosystem.atlassian.net/browse/QR-28) Automatically add target/classes folders as resource folders for modern frontend
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Added REST API & UI for setting the resource dirs directly
* Add missing log enables for failing servlet and for directory tracking, 

### Changed
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Control panel UI is responsive to work on narrower screens
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Print full baseurl in the startup message to be clickable from the terminal & obvious to users

### Fixed
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) No longer reusing AUI's page panel styles since it's often nested (doubled up)
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Improved the content of the control panel page to make it easier to use
* [QR-27](https://ecosystem.atlassian.net/browse/QR-27) Removed the XSRF checks

## [6.0.1] - 2025-02-10

### Added
* [DCA11Y-1556](https://hello.jira.atlassian.cloud/browse/DCA11Y-1556) Flip context batching system property

### Changed

* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Delayed startup to improve performance (avoid filesystem contention)

### Fixed

* [QR-24](https://ecosystem.atlassian.net/browse/QR-24) Fix REST API not working in BB 8.19
* [QR-10](https://ecosystem.atlassian.net/browse/QR-10) Improve startup time
* [QR-10](https://ecosystem.atlassian.net/browse/QR-10) Allow for complete graceful shutdowns
* [QR-10](https://ecosystem.atlassian.net/browse/QR-10) Order watched files/dirs in startup message
* [QR-14](https://ecosystem.atlassian.net/browse/QR-14) Return bad request for plugin keys not available when checking if enabled
* [DCPL-2395](https://ecosystem.atlassian.net/browse/DCPL-2395) Fixed 500 always returned
* Fix batching toggle not loading if JS loaded after DOMContentLoaded
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Fix the logs for Bitbucket
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Fix the logs for Log4j based products (~Plat 7+)
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Fixed the log spacing to look better in Confluence with escaped tabs
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Fixed the log rendering in files (e.g. RefApp) to look better by removing jansi
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Delayed startup to log closer to product startup
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Improve startup performance by moving to a separate thread
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Improve startup performance by removing Spring package component scanning
* [QR-25](https://ecosystem.atlassian.net/browse/QR-25) Enable missing logs

### Changed
* [DCA11Y-1556](https://hello.jira.atlassian.cloud/browse/DCA11Y-1556) Only flip batching system properties for relevant product
* [DCA11Y-1556](https://hello.jira.atlassian.cloud/browse/DCA11Y-1556) Make the flag messages specifically mention batching
* Rely on AUI/flag and remove AUI/message and alert\() fallbacks
* Remove dependency on jQuery
* Toast if batching toggle fails to initialise

## [6.0.0] - 2025-02-07

### Breaking

* [DCPL-2395](https://ecosystem.atlassian.net/browse/DCPL-2395) Removed "self" link from API response

### Changed

* [DCPL-2395](https://ecosystem.atlassian.net/browse/DCPL-2395) Support jakarta jax-rs

### Fixed

* [DCPL-2395](https://ecosystem.atlassian.net/browse/DCPL-2395) API link in response for deleting a system property & for uninstalling a plugin

## [5.0.12] - 2025-02-04

### Changed

* [DCPL-2395](https://ecosystem.atlassian.net/browse/DCPL-2395) Support jakarta servlets

## [5.0.11] - 2025-01-31

### Changed

* [BORG-964](https://bulldog.internal.atlassian.com/browse/BORG-964) Speed up start-up times by refactoring the initial file scanning
* Removed duplicate curl example

## [5.0.10] - 2024-12-17

### Fixed
* [DCA11Y-1472](https://hello.jira.atlassian.cloud/browse/DCA11Y-1472) Disable CSS compression in AMPS

## [5.0.9] - 2024-11-07

### Changed

* [DCA11Y-1346](https://hello.jira.atlassian.cloud/browse/DCA11Y-1346) Remove LESS usage (convert to vanilla CSS)

## [5.0.8] - 2024-07-01

### Fixed
* [DCA11Y-1024](https://hello.jira.atlassian.cloud/browse/DCA11Y-1024) Fix case with both REST v1 and REST v2 around

## [5.0.7] - 2024-06-24

### Changed

* Upgraded `io.methvin:directory-watcher` to 0.18.0 from 0.16.1

## [5.0.6] - 2024-05-22

### Fixed

* [QR-22](https://ecosystem.atlassian.net/browse/QR-22) Fix the REST API and Servlet permissions in Platform 7
* [QR-22](https://ecosystem.atlassian.net/browse/QR-22) Fix the REST API always 500ing in Platform 7
* [QR-22](https://ecosystem.atlassian.net/browse/QR-22) Fix the REST API response not rendering in Platform 7 because of
  Jersey2
* [QR-22](https://ecosystem.atlassian.net/browse/QR-22) Remove usage of Joda in REST APIs causing 500s in Plat 7
* Fix returning valid Atlassian plugin keys for regular OSGi bundles

## [5.0.5] - 2024-05-16
* [DCA11Y-1001](https://hello.jira.atlassian.cloud/browse/DCA11Y-1001) Fix errors happening when another
  version of `underscore` is included on the page

## [5.0.4] - 2024-03-27

* [DCA11Y-926](https://hello.jira.atlassian.cloud/browse/DCA11Y-926) Fix
  `NullPointerException` on flushing the web resources after quick reloading
* [DCA11Y-869](https://hello.jira.atlassian.cloud/browse/DCA11Y-869) Make the
  dependency on `underscore` explicit and managed

## [5.0.3] - 2024-03-14

* [DCPL-1104](https://ecosystem.atlassian.net/browse/DCPL-1104) Make forward compatible with Platform 7 milestones
* Drop support for unsupported product versions
* Update Jira web-resource flusher

## [5.0.2] - 2023-03-03
* [BSP-4717](https://bulldog.internal.atlassian.com/browse/BSP-4717) Fixed events API not working on JDK 8
* [BSP-4716](https://bulldog.internal.atlassian.com/browse/BSP-4716) Fixed web-resource flushing for non-Jira platforms
* [BSP-4715](https://bulldog.internal.atlassian.com/browse/BSP-4715) Fixed working in FeCru

## [5.0.1] - 2023-02-06
* Up-merge of [4.0.1]
* [BSP-4355](https://bulldog.internal.atlassian.com/browse/BSP-4355) Fixed keyboard shortcut by properly defining web-resource dependencies

## [5.0.0] - 2022-01-17
* [BSP-4657](https://bulldog.internal.atlassian.com/browse/BSP-4657) Removes Achoo DB console. [It's already its own plugin](https://stash.atlassian.com/projects/CP/repos/achoo-database-console/)

## [4.0.1] - 2023-02-06
* [BSP-4355](https://bulldog.internal.atlassian.com/browse/BSP-4355) Fixed keyboard shortcut by properly defining web-resource dependencies

## [4.0.0] - 2022-11-18
* Adds support for mac M1 architecture
* Now requires JDK 8+

## [3.2.1]
## [3.2.0]
## [3.1.1]
## [3.1.0]
## [3.0.3]
## [3.0.2]
## [3.0.1]

## [3.0.0]

### Changes
* Adds support for Java 11
* Removes various dependencies to allow use in products which support platform 3 or platform 5

### Fixed
* Fixes DbConsole link placement on Refapp
* Restores import for `com.atlassian.sal.api.rdbms.TransactionalExecutorFactory`, but limits it to specific products
  where the DbConsole feature works (Confluence, Jira, Refapp)

## [2.0.1] - 2018-03-26

* No longer depends on a `com.atlassian.sal.api.rdbms.TransactionalExecutorFactory` being supplied by the host product

[5.0.9]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.6%0Dquickreload-parent-5.0.8
[5.0.8]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.6%0Dquickreload-parent-5.0.7
[5.0.7]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.6%0Dquickreload-parent-5.0.6
[5.0.6]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.6%0Dquickreload-parent-5.0.5
[5.0.5]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.5%0Dquickreload-parent-5.0.4
[5.0.4]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.4%0Dquickreload-parent-5.0.3
[5.0.3]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.3%0Dquickreload-parent-5.0.2
[5.0.2]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.2%0Dquickreload-parent-5.0.1
[5.0.1]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.1%0Dquickreload-parent-5.0.1
[5.0.0]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-5.0.0%0Dquickreload-parent-4.0.0
[4.0.1]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-4.0.1%0Dquickreload-parent-4.0.0
[4.0.0]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-4.0.0%0Dquickreload-parent-3.2.1
[3.2.1]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.2.1%0Dquickreload-parent-3.2.0
[3.2.0]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.2.0%0D3.1.x
[3.1.1]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.1.1%0Dquickreload-parent-3.1.0
[3.1.0]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.1.0%0D3.0.x
[3.0.3]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.0.3%0Dquickreload-parent-3.0.2
[3.0.2]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.0.2%0Dquickreload-parent-3.0.1
[3.0.1]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.0.1%0Dquickreload-parent-3.0.0
[3.0.0]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-3.0.0%0Dquickreload-parent-2.0.1
[2.0.1]: https://bitbucket.org/atlassianlabs/quickreload/branches/compare/quickreload-parent-2.0.1%0Dquickreload-parent-2.0.0
